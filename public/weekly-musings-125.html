<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 125</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 125</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time around, something that’s been cycling through the 8-bit processor in my skull for a while now. Something that many of us don’t consider, but should. My own danged self included.</p>
<p>With that out of the way, let’s get to this week’s musing</p>
<h2 id="on-digital-footprints">On Digital Footprints</h2>
<p>We leave a lot of ourselves online. Footprints leading to and from the various outposts on the web that we frequent or just pass through. Footprints that record those jaunts and strolls around sites and networks.</p>
<p>If any of us ever turn our minds to that, we tend to focus on the footprints left by our data online. Personal information. Purchase histories. Social media posts. Comments. Alla that kind of stuff. Little digital breadcrumbs that, individually, seem inconsequential. When taken together, those breadcrumbs form a loaf that tells people a lot about us, about what we’re doing online. The information that the platforms many of us embrace use and sell, often without our knowledge and in ways we don’t understand or agree with.</p>
<p>But there is other footprint that we don’t always consider when we venture online: the environmental footprint of our use of web-based tools and services.</p>
<p>I’m not just talking about energy used by, and the waste from, the mining of cryptocurrency (although that does have a <em>huge</em> impact). I’m talking about all of what we do online: hosting or visiting websites, taking advantage of mobile devices and apps, using social networks, making online purchases.</p>
<p>Thoughts about those footprints, about that environmental impact have been circulating through my synapses for a few years now. About how everything that we do on the web, everything that we use to access the web has an environmental cost. Individually, we make small impacts — an app here, a web page or a website there. It doesn’t seem like much. But multiply that by however many millions or billions of uses and hits there are a day, and all of it adds up.</p>
<p>It may seem harmless enough, dropping by Facebook or Instagram or Twitter or Amazon using a web browser or an app. You might think <em>How much energy does that use?</em> More than you imagine.</p>
<p>Let’s look at the situation using a <em>very</em> wide lens. An <a href="https://www.independent.co.uk/climate-change/news/global-warming-data-centres-to-consume-three-times-as-much-energy-in-next-decade-experts-warn-a6830086.html">article in The Independent</a> from 2016 noted:</p>
<blockquote>
<p>the 416.2 terawatt hours of electricity the world’s data centres used last year was significantly higher than the UK’s total consumption of about 300 terawatt hours.</p>
</blockquote>
<p>Think about that for a moment. Taken together, the collections of servers around the world that push bits to us, both for business and for our pleasure, suck up more energy than is used by a nation with a population of over 65 million. That was five years ago. You can bet that the situation is even worse in 2021.</p>
<p>What about at a personal level? Every request we make online — when our browser hits a website, when we click or tap a button, when use our favourite apps — has a cost. A cost in energy used. To power your devices. To power that internet service provide or cell tower. To power a data centre. To cool that data centre. The latter is probably the largest area in which a data centre consumes power — like people, computers tend to react badly when things get too hot.</p>
<p>It takes about 0.015 kWh to move and store a gigabyte of data. Barely a spark. Each month, each year, every one of us causes hundreds if not thousands of gigabytes of data to be pushed around the web. Let’s say, on average, we all chew up about 1,200 GB of data annually. That uses 18 kWh of energy, which produces up to 4.2 kilograms of CO<sup>2</sup>. That might not seem like a lot on its own — the average refrigerator uses 380-odd kWh of energy per year. But multiply that by the number of people worldwide using the internet and you can see the problem.</p>
<p>As Gerry McGovern notes in his book <em><a href="https://gerrymcgovern.com/books/world-wide-waste/">World Wide Waste</a></em>:</p>
<blockquote>
<p>As the gigabytes pile up it begins to register on the pollution monitor, though. When we get into zettabytes, there are real and substantial pollution consequences.</p>
<p>How much pollution does the transfer of 2,000 zettabytes of data cause?</p>
<p>8,400,000,000,000 kg. Eight trillion, four hundred billion kilos.</p>
</blockquote>
<p>As someone once said, that’s <em>a lot</em> of zeroes. But, as McGovern points out, much of that data being shoved around the internet is useless. It’s worthless. It adds little to what we’re seeing, what we’re reading, what we’re interacting with.</p>
<p>And it’s not just what other people are publishing to the web for all to see. It’s what we’re saving online, too. Online storage is cheap. Incredibly cheap. Pennies a megabyte. We’re saving photos, videos, images, documents, notes, archives. Sundry files that take up space on someone else’s computer — whether a social media site, a photo sharing service, an online file storage site.</p>
<p>A lot of that information is old. So old, it might not be useful or relevant any longer. You might not even remember <em>why</em> you saved those files. About a year ago, I did some culling of files I had stored online. Ones that I’d created as far back as the mid 1990s. Aside from the <em>very</em> limited nostalgia value, those files served no purpose. Some I couldn’t open any longer since the software I’d used to create them no longer exists.</p>
<p>Files like that, and I’m sure we all have more than a few gigabytes of them, just take up space and processing power. Which results in more energy being used. Which results in more pollution. And yet we continue to pile digital bric-a-brac into the shelf space that’s online storage.</p>
<p>Instead of adding more to that shelf space, maybe it’s time to decrease the number of our digital footprints. How? There are any number of ways to do that. Here are a few ideas:</p>
<p>If you have your own website, try to make it as small as possible. Maybe by following the ethos of the <a href="https://512kb.club/">512KB Club</a>, by <a href="https://sive.rs/polut">coding the site by hand</a> to limit the amount of cruft that accumulates on it, or by converting your site <a href="https://scottnesbitt.online/a-home-page-not-a-website">to a single page</a>.</p>
<p>Images on a web page suck up a lot of bandwidth. If you can, get rid of all images. If you can’t, try to use as few images as possible. Make them smaller and <a href="https://solar.lowtechmagazine.com/2018/09/how-to-build-a-lowtech-website.html">lower their resolution</a>.</p>
<p>Avoid, where possible, scripting languages like JavaScript or PHP. While both add features to your site, enabling visitors to more deeply interact with it, as yourself whether you really need to use them or not. Instead, use <a href="https://scottnesbitt.gumroad.com/l/learnhtml">HTML</a> and HTML alone.</p>
<p>Try to host your website (no matter its size) with a company that uses renewable energy. You might not eliminate the site’s carbon footprint, but you’ll noticeably reduce it.</p>
<p>Every time you go online, consider whether or not you need to use that app or visit that website — whether number of times a day that you normally do, or at all. Perhaps try to find an alternative to that site or app, one that doesn’t require you to call so much data to your device.</p>
<p>Think carefully about what you post and save online. Ask yourself these questions:</p>
<ul>
<li>Do I really need to share that photo or video?</li>
<li>Does it need to be that size or at that resolution? Or can I post a smaller version and use a lower resolution?</li>
<li>Is what I’m saving online essential? Or am I just saving that something because I think it will come in handy one day?</li>
</ul>
<p>Regardless, try to act not in your best interests but in the best interests of the wider web. And the wider world. There’s no reason that two must be separated by the wall of selfishness.</p>
<p>One person’s actions won’t decrease the environmental impact that the web has. But as more people adjust their habits, and decrease their digital footprints, the benefits of <a href="https://scottnesbitt.online/small-is-a-force-multiplier.html">force multiplication</a> come into play.</p>
<p>It doesn’t require radical shifts in behaviour. It doesn’t require you to <em>give up</em> or <em>sacrifice</em> something. All you need to do is make small tweaks and be more mindful about what you do online.</p>
<p>I’ll wrap up this musing with a thought from the book <em>World Wide Waste</em>, a thought that’s stuck with me since I first read it:</p>
<blockquote>
<p>Think about the weight of everything you do in digital.</p>
</blockquote>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
