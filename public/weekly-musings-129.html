<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 129</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 129</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This musing isn’t the one that I’d planned to send your way this week.</p>
<p>What I can only describe as a chain of suggestion, in the hypnotic sense, led me here. A mix of something a friend or two recently told me about and what I saw and read on visits to a couple of websites came together. Then, after some mixing with a large mental spoon, the idea for what you’re about to read shifted into my frontal lobe.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-the-typewriter">On the Typewriter</h2>
<p>Recently, I had a weird dream. OK, about 60% of my dreams fall into the <em>weird</em> bucket, but this one was weird <strong>and</strong> vivid.</p>
<p>That dream started innocently enough. I was at home, staring down the barrels of a pair of looming deadlines. As as I got ready to get to work, I found that one of my laptops, then the other refused to start. Worse, my wife and daughter weren’t home and they’d taken their laptops with them. And I could find neither my phone nor a tablet.</p>
<p>Panic, which is always a lot more amplified in a dream, gripped me. In my agitated dream state, I started rooting around in a closet in desperation. There I discovered a beige case. I hauled it out and wiped away the thin crusting of dust. Up went the metal clasp on the front and open flipped the lid. Inside lay a somewhat battered Royal portable typewriter.</p>
<p>I carried it over to the table, where I set said Royal down with a dull <em>thud</em>. Ancient muscle memory took over — I loaded a sheet of paper and started banging away at the keys. A familiar, but long unheard, <em>CLACK-CLACK-CLACK</em> echoed off the walls as words began to appear on the page …</p>
<p>I’m not sure how many of the fives of you who read these missives are my age or within my age range. That means I can’t be sure if you have anything like the relationship with the typewriter that I have. A typewriter was a constant fixture in my life from the time learned how to properly move my fingers across a keyboard until I was in my early 20s.</p>
<p>I learned to type just before the <a href="https://en.wikipedia.org/wiki/Home_computer">home computer</a> revolution kicked off, and a few years before I got my first computer (a Commodore <a href="https://en.wikipedia.org/wiki/Commodore_VIC-20">VIC-20</a> in case you’re wondering). I learned how to type on a manual typewriter — much like the Royal I used in my recent dream — then used a Smith Corona electric that came into my mother’s possession.</p>
<p>Even after I started using a computer regularly and wrote with a basic word processor called <a href="https://en.wikipedia.org/wiki/SpeedScript">SpeedScript</a>, I straddled the analog and digital worlds. Although a computer was within reach, I continued to use the typewriter well into the late 1980s. Mainly because I didn’t have a printer. To get anything printed, I’d save my work to a <a href="https://en.wikipedia.org/wiki/Datasette">cassette tape</a> (really!) and schlep that over to the house of a friend who had a Commodore computer (a <a href="https://en.wikipedia.org/wiki/Commodore_64">C64</a>, the lucky bastard!) and print it on his printer. That wasn’t something I could do all that regularly without putting a strain on our relationship.</p>
<p>From junior high school through to university, countless essays and papers and assignments were churned out on a typewriter. As were many of my early articles written (and more than a few rejected) for publication. The typewriter was an outlet for all of the words and ideas bubbling inside me. It was a way of getting those words and ideas, however good or bad, out of my system.</p>
<p>More to point, a typewriter enabled me to share ideas in written form. My handwriting is, and always has been, horrible. Borderline illegible, even by me. With a typewriter, I could put what was in my brain on paper in a way that others I chose to share with could actually <em>read</em>.</p>
<p>Nowadays, typewriters are objects of fascination in some circles. In those circles of fascination, typewriters have become collectible items, almost minor <em>objets d’art</em>. Typewriters can be something that’s pleasing to the eye, pleasing to the touch.</p>
<p>Some models, especially older manual typewriters, <em>are</em> beautiful pieces of engineering. They have compact, clean lines. There’s a certain aesthetic to them, a certain design ethos that’s lacking these days. There’s a decidedly <a href="https://en.wikipedia.org/wiki/Art_Deco">Art Deco</a> look to more than a few typewriters.</p>
<p>At their core, though, typewriters (no matter how aesthetically pleasing) were created, and used, as <em>tools</em>. They weren’t crafted as fine sculptures. They were mass produced items that helped people get work done.</p>
<p>While I don’t yearn to go back to the days when I used a typewriter, you can probably tell that I can see their appeal. Both for a generation that didn’t know them, and for the generation which used typewriters and seems to miss them. In the minds of some, typewriters can represent some positive aspects of the past that seem to be missing from modern times.</p>
<p>A typewriter demanded a bit more precision than your average word processor. You needed to set up tabs and margins by hand — there are no dialog boxes or click and drag controls to help you. You needed to position the paper correctly. And you needed to develop an instinctive feel for how much space to leave at the top when you turn the platen roller.</p>
<p>There was no spelling checker or automatic pagination to help you out, either. The former … well, that explains itself, doesn’t it? But with the latter you had to develop a sense for when a page was coming to an end, and when you needed to swap that page with a fresh one. That sense definitely took time, and more than a few sheets of paper, to become second nature.</p>
<p>You also needed to clearly plan what you were typing so you could do it as efficiently as possible. It took time and wasted paper if you decided part way through that you needed to shift paragraph three on page five to somewhere on page two. In that way, typewriters promoted slow, deliberate thought and deliberate planning.</p>
<p>That’s not to say that the typewriter was the perfect machine for writing or communicating. I definitely wasn’t. Unless you were using an electric with a <a href="https://en.wikipedia.org/wiki/IBM_Selectric_typewriter#Type_elements_and_fonts">typeball</a>, you risked jamming a couple of type arms if typed too quickly or if you struck two keys at the same time.</p>
<p>Typewriters also used reams of paper. Not just for what was completed, but for all those drafts and error-riddled pages that went into a waste paper basket. I don’t know about you, but I didn’t like killing trees back in the day any more than I do now.</p>
<p>Replacing <a href="https://en.wikipedia.org/wiki/Typewriter_ribbon">ribbons</a> was a pain. Worse, you never knew when a ribbon would come to the end of its life. On top of that, I have to wonder about the environmental impact those ribbons had.</p>
<p>If you made a mistake, you either needed to start a page over again or go over the error with headache-inducing correction fluid. As a budding young freelance writer back in the 1980s, I quickly discovered most editors prefer clean copy to something that’s marked up with pencil or pen.</p>
<p>Let’s not forget that even portable typewriters are bulky and take up space. They were great collectors of dust. You quickly learned that no matter how well built, a typewriter needed regular care — cleaning, oiling, and (depending on how much you used it) professional maintenance. Something that’s hard to come by these days.</p>
<p>While the typewriters that passed under my hands taught me a lot, I’m glad I don’t use them any longer. Even though I like to embrace constraints, typewriters have a few too many constraints when you compare them to a word processor or even the humblest text editor.</p>
<p>Typewriters were of a certain time. Not a better time, but a time that, in some ways, led us to where we are now.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
