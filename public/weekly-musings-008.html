<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 008</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 008</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Ever have one of those weeks where you have so many ideas at your fingertips that you have a hard time deciding which one to tackle first? That’s been the last seven days for me. It’s a great problem to have, to be honest. At least until that choice paralyzes you.</p>
<p>And that’s what happened to me this week. I started three different essays, but was unable to finish any of them. So, I dipped into my files and dug up an old essay that I believe still stand up in the few years since I wrote it. I hope you enjoy it. You’ll get a fresh essay next week. Promise!</p>
<h2 id="on-the-difference-between-movie-and-film">On The Difference Between Movie and Film</h2>
<p>British actor <a href="https://en.wikipedia.org/wiki/Tom_Baker">Tom Baker</a>, best know for his portrayal of the fourth incarnation of Doctor Who, once told an interviewer that watching a movie starts off as a communal activity. But, Baker added, once the lights go out and the curtain comes up, watching a movie suddenly becomes an intensely personal experience.</p>
<p>I can vouch for that. Growing up in Toronto, Canada, I embraced the personal experience of watching and discovering both movies and film. At 8 p.m. each Saturday night, when my parents were hunkered down watching <em>Hockey Night in Canada</em> in winter or whatever else the rest of the year, my gaze would be fixed on the 13-inch screen of the often-balky colour TV I inherited from my sister.</p>
<p>My fare on those Saturday evenings, much more interesting than a bunch of men chasing a puck with sticks, was a pair of classic films, shown back to back, on <em>Saturday Night at the Movies</em>. That hand-me-down TV, with one corner melted after an encounter with my sister’s curling iron, brought … well, I can’t recall how many of the best pieces of cinema ever made into my life.</p>
<p>There was more to <em>Saturday Night at the Movies</em> than that, though. The show’s presenter, the late <a href="https://en.wikipedia.org/wiki/Elwy_Yost">Elwy Yost</a>, would also provide commentary about the films. He’d interview people involved in all aspects of the production of those films — directors, producers, writers, actors, effects people. He managed to coax interesting facts and anecdotes about the films and the process or making films out of them.</p>
<p>Yost’s commentaries and interviews weren’t in the high-handed style of, say, <em>Cahiers du cinéma</em>. His commentaries were informed, yet accessible. They were authoritative yet personal. They shone with Yost’s deep love of both film and movies, and his deep respect for the folks who made them.</p>
<p>And that love of film and movies wore off on me. Yost taught me to love and appreciate both movies and film. His interviews and commentaries started me thinking critically about the differences between movies and film. And in some ways, watching movies and films influenced and informed my writing style.</p>
<p>We far too often use the words <em>movie</em> and <em>film</em> interchangeably. Maybe I’m being pedantic, but there <em>are</em> differences between the two. Most of the time, those differences are obvious. At other times, the differences are subtle.</p>
<p>A film can be a movie, but not every movie is a film.</p>
<p>Let me explain.</p>
<p>The biggest difference between movies and film lies in their <em>purpose</em>. A movie is designed to entertain. It provides an escape. A movie is a confection that provides a moment of enjoyment before fading.</p>
<p>A film can entertain, too. A film also has a deeper message, a deeper meaning. A film makes a statement. In some cases, a film holds up a mirror to our current reality to show us something that we might have ignored, didn’t want to see, or just weren’t able to see.</p>
<p>The Costa-Gavras film <em><a href="https://en.wikipedia.org/wiki/Missing_(1982_film)">Missing</a></em> is a good example of this. On the surface, <em>Missing</em> is the story of the search for an American journalist who disappeared during the 1973 coup in Chile. But it’s also a harsh indictment of the brutality of Pinochet’s regime and the U.S. government’s backing of that regime. You can interpret the film more widely as a denounciation of authoritarianism and those who knowingly back it.</p>
<p>A film can educate. It can enlighten. A film offers a different, unique, or unexpected point of view. A film probes deeper than a movie. It zooms in on the motivations, emotions, and personalities of the characters. It delves into the core situation of the plot, trying to illustrate and understand how that situation is molding the characters or how the characters are influencing the situation.</p>
<p>A film pushes the boundaries of technique and storytelling. A film’s structure might not always straightforward. It might not always be linear. While a film has a beginning, a middle, and an end those portions aren’t always where we expect them to be. Think of the oft-maligned adaptation of <em><a href="https://en.wikipedia.org/wiki/Cloud_Atlas_%28film%29">Cloud Atlas</a></em>. Its story bounces between characters and time periods, but if you pay attention you can see the common threads that bind the narratives and the characters.</p>
<p>The structure and pace of the best films is literary. The visual and spoken languages soar higher than those of a movie. There is an inherent level of complexity and chaos in a film, but if the film is done properly, the complexity and chaos come together in the end to create a coherent whole. That ending might not be a happy or uplifting one, though. Consider the conclusion of <em>Apocalypse Now</em>. Captain Willard succeeded in his mission to stop Colonel Kurtz, but at a high personal and moral cost. Willard came to understand and even empathize with Kurtz’s worldview. While Willard’s superiors were going to promote him for his actions, Willard stated that he <em>wasn’t even in their ***kin’ army anymore</em>, indicating a decisive psychic and moral break from the institutions and mindset he once embraced.</p>
<p>A film strives to be art. I realize that in some circles, <em>art</em> is a four-letter word. It’s a synonym for <em>pretentious</em>. And there’s no doubt that some of what passes for film can be pretentious. Most films, though, don’t quite reach the level of art. Ones that do are worth watching over and over again. To be honest, the ones that don’t quite succeed are often worth watching several times as well. Those films display a level of craft and discipline that put them well beyond most standard celluloid fare.</p>
<p>There are scenes in Terrence Malick’s <em>Days of Heaven</em> which are nothing short of visual poetry. Those scenes draw you in, they mesmerize you. Akira Kurosawa could say more with a static long shot, like the ones in his epic <em>Ran</em>, than most directors could or can with dynamic shots packed with visuals. Being able to do what Malick, Kurosawa, and filmmakers of their calibre can, is where the true art and true visual poetry of films lie.</p>
<p>Some movies try to be film. Few succeed. A small handful come close, but not close enough. A good example of that is <em><a href="https://en.wikipedia.org/wiki/Syriana">Syriana</a></em>. It’s a gripping story, told from several conflicting yet not dissimilar points of view. What stops <em>Syriana</em> from being a film is the almost forced earnestness of the main characters and the ending — it felt tacked on, as if the suits running the studio felt there needed to be an explicit happy ending for the character played by Matt Damon.</p>
<p>That’s not to say I’m against movies. I do believe, though, that most movies are forgettable. That many movies are either unwatchable or just not worth watching. Think of the summer and holiday fare that major movie studios put out. Think of all the straight-to-video productions that crop up each year.</p>
<p>There are times when I like, and even <em>need</em>, to watch a bit of light entertainment. I’m not always looking for a deeper message or stunning cinematography or a perfectly-crafted story. Sometimes, I just want a little mindless fun or an interesting tale told well. Sometimes, I’m more interested in a bit of distraction than enlightenment. That’s what a good (or sometimes just an OK) movie offers me. In my circle of friends, many of them more passionate about film than me, and we don’t see there being any shame in professing our love for movies like <em>Enter the Dragon</em> or <em>Repo Man</em> or Marvel’s superhero fare.</p>
<p>No matter how many movies flash before my eyes, I keep going back to film. A film can offer you so much, if you’re willing to open yourself to the possibilities.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
