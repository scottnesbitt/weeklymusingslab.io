<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 040</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 040</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>When I originally published this musing, it was an especially hectic time for me. The Day Job<sup>TM</sup> piled a lot on me, and then I logged I don’t know how many thousands of kilometres with flights between Auckland and Raleigh, NC.</p>
<p>Speaking of Raleigh, that’s where this musing started. As the ideas that would become this week’s letter were coalescing in my brain, I had breakfast with my friend <a href="http://www.semioticrobotic.net/">Bryan Berhenshausen</a>. Breakfast in Raleigh has become something of a tradition with us. It gives us a chance to catch up and share ideas in person rather than via email. During our conversation, Bryan offered some interesting and valuable insights into this topic. As is often the case, I’m indebted to Bryan for those insights.</p>
<p>Let’s get to this week’s musing.</p>
<h2 id="on-local-history">On (Local) History</h2>
<p>It’s interesting to view how some people perceive history. They think of it as something grand. They think of it as possessing scope. They think of history as focusing on major events, events that shape countries, civilizations, and the world as a whole. They see history as a set of big stories, sometimes interlocking stories, that define us.</p>
<p>History is more than that. For all the big stories, there are countless little ones. Stories of ordinary people. Stories of ordinary places. Local histories that we never learn about. If we do, we don’t learn about them in detail. Local histories that we never really discover unless we root them out.</p>
<p>Thoughts about local history have always nested deep inside my brain. It wasn’t until my annual trip to Raleigh, NC in late October, 2019 that those thoughts were pushed to the surface.</p>
<p>Let me explain.</p>
<p>When I arrive in Raleigh, it’s usually after about 20+ hours of travelling. It’s late on a Friday evening when I reach my hotel, and I’m under the influence of fatigue and the dull ache behind my eyebrows that’s jet lag. I want nothing more to unpack, jump into bed, and get six or so hours of sleep.</p>
<p>When Saturday morning rolls around, I follow a ritual. It starts with a French toast breakfast at my favourite cafe, followed by an amble around the three museums in the downtown area: the <a href="https://www.ncmuseumofhistory.org/">North Carolina History Museum</a>, the <a href="https://naturalsciences.org/">North Carolina Museum of Natural Sciences</a>, and the <a href="https://cityofraleighmuseum.org/">City of Raleigh Museum</a> (COR for short).</p>
<p>Of the three, I’m partial to the latter. COR appeals to my love of local history — it tells little stories of people and events that shaped Raleigh. More than that, COR is housed in a bit of history. It occupies the ground floor of the city’s first so-called skyscraper. In previous decades, the museum’s creaky, uneven but well-kept wooden floors were trod upon by countless residents of Raleigh in its capacity as one of the area’s largest hardware stores. I’m sure those floors and walls could tell stories. Well, they do …</p>
<p>The artifacts peppered throughout the space recount tales of a city. Its growth. Its challenges. Its triumphs and struggles. Tales you normally wouldn’t hear unless, as I mentioned a few paragraphs ago, you went looking for them.</p>
<p>I’m grateful for institutions like COR, the City of Toronto Archives, and others like them that attempt to keep local history alive and vital. But I also worry that history is in danger of slipping through our fingers.</p>
<p>Over plates of French toast on a Monday morning, I mentioned the nascent ideas that were forming the basis of these musings my friend Bryan Berhenshausen. With his typical acumen, Bryan noted that one reason we lose local history is because we’re now a very transient and mobile culture.</p>
<p>Compared to other times in human history, we’re a lot more mobile than previous generations. We can move from place to place not because we must but because we <em>want to</em>. Moving away from the places in which we were born, with which we’re familiar is easier and less fraught with peril than it was for our predecessors.</p>
<p>In doing that, however, we break a connection with our history. We leave behind the depth of what we knew. We arrive in a new, foreign place (even if it’s within our borders) and are essentially <em>tabula rasa</em>.</p>
<p>Bryan, for example, is a native of Pennsylvania who (when I wrote this musing) lived in North Carolina. I’m a Canadian transplanted to New Zealand. We live in those places, but we’re not quite <em>of</em> those places. We don’t have the deeper connection with North Carolina or New Zealand that those who were born in and grew up in those places do. As much of we love New Zealand and North Carolina, our roots aren’t quite as firmly in their soils as those of the folks who count themselves as natives.</p>
<p>We have to make an effort of uncover local history. We need to go out of our ways to learn about that history. Sadly, I don’t think all that many people do. They’re not encouraged to, and some just don’t see the point.</p>
<p>Local history can also slip away due to corporate influence. Especially when it comes to urban development. That act often reshapes urban centres, with little or no regard for the past. In the names of the twin gods <em>Progress</em> and <em>Modernisation</em>, corporate interests are willing to delete history.</p>
<p>They’re willing to edit long-standing local historical narratives to further their own aims and to create support for the narratives that they’re spinning around those aims. In their hands, local history goes from being a celebration of the past to becoming a justification for change and for doing away with the old. Or, at least, as much of the old as they can do away with.</p>
<p>A number of years ago, my friend Bryan wrote for his local newspaper in Pennsylvania. That newspaper was a repository of over 100 years of local history. When the paper was bought out, and eventually shuttered, by large media conglomerate that 100+ years of recorded history was put in jeopardy. Bryan’s not sure if the newspaper’s archives were destroyed or boxed up and stored somewhere. I don’t know which fate is worse.</p>
<p>When talking about what happened to his local paper, Bryan pointed out that any effort to preserve the paper’s archives needed to go far beyond merely putting the individual clippings in a book or hanging them on a wall. All that information needs <em>context</em>.</p>
<p>To get that context, you need a person or people with the right combination of skills and perspective. While an archivist can pull various documents and photographs and accounts together, there’s also a need someone with a deep, intimate knowledge of local history who can provide that context. You can never be certain that you’ll find someone like that. And you can’t be certain that the budget to hire the person to do the deed will exist. If it does, there’s always the unpredictability of that funding. It could be there one year, and dry up the next. Who will maintain a collection when that happens?</p>
<p>As inconsequential as some might make it out to be, local history <em>is</em> important. It’s a chronicle of where a neighbourhood, an area, a city came from. It’s a guidepost showing a way forward. It’s a link to people and to a past that we shouldn’t forget.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
