<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 187</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 187</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>As I note in the <a href="https://scottnesbitt.net/index.html#now">/now section</a> of my home page, I’ve been trying to reevaluate my relationship with the technology world and with technology in general. There are a few reasons for that, which I’m more than happy to discuss over a beverage of some sort.</p>
<p>This edition of the letter isn’t a discussion about, a paen to, or a defense of the titular technology. It’s more a use of that technology as the starting point for a musing about my dissatisfaction not just with technology in general, but also with pockets of the tech world. In some ways, it’s (once again) me being a cranky old man yelling at clouds.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-the-idea-of-chromebooks">On (the Idea of) Chromebooks</h2>
<p>In early September, 2022 a tech blogger published a post about why he uses devices running ChromeOS (the operating system that powers on Chromebooks and similar devices). That post had the dubious honour of making the front page of a certain technology news aggregation site. The comments on the post were, to be kind, interesting. More than a few of them questioned the blogger’s choice of using something running ChromeOS as his daily driver. Just as many pretty much stated that the blogger had made the <em>wrong</em> choice, that he had <em>better</em> options.</p>
<p>Once again, it was a group of strangers telling someone what’s best for them. I find that attitude more than slightly arrogant and more than a bit condescending.</p>
<p>To be honest, I also find that a tad amusing. At least as far as people spending even a modicum of their time and brain power thinking about this sort of thing. Rent free space in their heads and all of that …</p>
<p>Think about Chromebooks, if you will, for a moment. They’re derided as being just a laptop running a web browser. As <a href="https://www.zdnet.com/article/five-reasons-chromebooks-are-better-than-windows-laptops/">Steven Vaughan-Nichols pointed out</a>:</p>
<blockquote>
<p>People who claim that Chromebooks are just a Chrome browser and nothing more are clueless and haven’t actually used one.</p>
</blockquote>
<p>The browser is the device’s user interface (UI), but it also runs small apps. Regardless, for the average computer user, that UI is familiar. I can’t think of anyone who doesn’t use a web browser. These days, most people spend inordinate amount of time in one. The browser UI has become, for lack of better word, intuitive. Chromebooks leverage this and enable their users to do what need to do in an environment in which they’re comfortable.</p>
<p>Take my parents, for example. Over the years, they’ve badly hammered a number of desktop, and a couple of laptop, computers along with at least two tablets. Hammered them to the point to which those devices were no longer operational — either they won’t start or the operating system was pummeled with a variety of digital crap.</p>
<p>For my parents, a device — like a Chromebase or a Chromebox — running ChromeOS would be ideal. As tech journalist Jack Wallen wrote, devices running ChromeOS <a href="https://www.zdnet.com/article/five-reasons-chromebooks-are-the-perfect-laptop/">are essentially user proof</a>. On top of that, a ChromeOS device does what my parents, and people like them, need to do. And that’s nothing fancy or complex. The hardware’s fairly robust, as robust as (if not more so than) cheaper Windows laptops that some folks tout as alternatives to Chromebooks. I can’t think of any way in which my parents could gum up a Chromebase or Chromebox in the same way they’ve done that to the other devices they’ve owned over years.</p>
<p>Something I read while outlining this musing hit the nail on the proverbial head:</p>
<blockquote>
<p>[F]or most people, computers are tools, not a lifestyle.</p>
<p>— <a href="https://www.wired.com/story/modern-humans-learn-ancient-software-retrocomputing/">Paul Ford</a></p>
</blockquote>
<p>Some techies need a reminder that not everyone is a like them. Not everyone is a developer, a hardware hacker, or a gamer. Not everyone is doing intensive work with multimedia. Not everyone is interested in modding or tricking out their computers. Not everyone wants to tinker, to build their own desktops or fiddle with <a href="https://en.wikipedia.org/wiki/Single-board_computer">SBCs</a>. They don’t want to swap out hardware or install alternative operating systems. They just want to get things done in the fastest, simplest, most frictionless manner possible.</p>
<p>I’ll argue that the needs of the majority of computer users are fairly <em>simple</em>. Those needs are <em>basic</em>. <em>Boring</em>, even. There’s nothing wrong with that. For people in that situation, a device running ChromeOS or something like it is ideal.</p>
<p>But let’s leave Chromebooks and go back to the core idea of knowledgeable people actually not knowing. Sure, they might have an in-depth familiarity with hardware and programming languages. They might be able to administer databases and keep acres of servers running. But they’re often clueless about the needs of others, especially when those needs differ from their own. They’re often clueless about how people use technology, which differs from the way in which they use technology.</p>
<p>I don’t doubt that some of those folks have some valid points to make. Being denigrating and condescending isn’t the way to make those points, though. Tough love (or some warped interpretation of it) isn’t the best way to engage with people. Much of the time, those who get preachy or go into angry rant mode come across as arrogant jerks.</p>
<p>Their perceptions clouded by their use cases. By their biases. By their experiences. It’s all wrapped up in something that I call <em>the power user fallacy</em>. For whatever reason, there are swaths of people in some corners of technology world who fail to understand that ordinary mortals are only looking for what I call <em>technology for the masses, not the classes</em>.</p>
<p>Let’s go back to devices running ChromeOS for a moment. Those devices get seven or eight years worth of operating system updates — an amount that some people have deemed insufficient. For the average computer user, though, seven or eight years with a device is a lifetime. The average computer user doesn’t often think about upgrades. They use technology until it fails or they decide to get something new.</p>
<p>The same goes for upgrading. Or just about any update, physical or digital. I have an acquaintance who uses a 2012 vintage MacBook Air. It still works quite well, even though it’s slow to start up, the battery is broken, and the latest updates to macOS don’t install. She’ll be using that laptop until she no longer can. Her computer might be vulnerable to someone who can take advantage of openings in that laptop’s security, but she’s not alone in that. There are millions of people like my acquaintance. I’m not saying that the situation is a good thing (it’s not), but it <em>is</em> a reality.</p>
<p>The ordinary computer user wants something easy to use, that’s easy to set up. The ordinary computer user has limited uses for technology, and often doesn’t need anything heavily muscled or with the flexibility of a (digital) gymnast or the cardio of a (digital) triathlete. That could be a Chromebook, an inexpensive Windows laptop, a low-end MacBook Air, an older computer that’s been refurbished. Or it could be something else entirely.</p>
<p>It wouldn’t be so bad if some folks didn’t more or less state outright that anyone who chooses an option that doesn’t dovetail with theirs has made the <em>wrong</em> choice. That’s where arrogance and condescension mentioned earlier comes in — in essence, telling people what’s best for them, how should spend their money.</p>
<p>More to the point, why do some people feel the need to continually comment about it? At length, too.</p>
<p>This doesn’t just apply to computers. I’ve seen that kind of online pile on happen with all sorts of technology — tablets, phones, media players, TVs, software, services, and more. The points that I mentioned about Chromebooks earlier in this musing apply to those other technologies, too.</p>
<p>There’s no universal <em>best choice</em>. For anything.</p>
<p>What’s best for you might not be best for someone else. And that’s OK. We’re all different, with different needs and different use cases. We have different levels of abilities and different interests. Which is kind of what being an <em>individual</em> is all about. Someone might not want or need the open road flexibility of technology that can do everything and anything. Being limited, having constraints is the <em>right</em> path for some people. It’s their best option.</p>
<p>If only certain others would understand and accept that …</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
