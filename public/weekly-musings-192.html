<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 192</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 192</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time ’round, an edition of the letter that’s a something of a continuation of <a href="weekly-musings-018.html">Musing 018</a>. What you’re about to read is more a set of observations, thoughts, reminiscences, and opinions rather than something resembling a somewhat cohesive essay.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-libraries-again">On Libraries (Again)</h2>
<p>Over the past few months, I’ve had quite a bit of cause to regularly drop by my local public library. There are a few reasons for that, reasons with which I won’t bore you. But each visit to that small library a few minutes’ walk from my home reminded me of what wonderful places public libraries are. I’ve been reminded of how lucky we are to have institutions like that. I’ve been reminded how important it is to keep public libraries alive and to ensure that they thrive.</p>
<p>Libraries are, in the words of a good friend, one of the great egalitarian institutions in our society. They bring information, knowledge, and learning to <em>anyone</em>, regardless of their means. Regardless of who they are or where they come from. Regardless, at least to a point, of their abilities.</p>
<p>That said, libraries are more than just a collection of printed material. They’re centres of the communities in which they reside. They’re safe havens, if only for a few passing moments or hours. They’re a key that can unlock so many different doors.</p>
<p>As Issac Asimov wrote in 1971 to the patrons of a (then) new library in Troy, Michigan:</p>
<blockquote>
<p>Congratulations on the new library, because it isn’t just a library. It is a space ship that will take you to the farthest reaches of the Universe, a time machine that will take you to the far past and the far future, a teacher that knows more than any human being, a friend that will amuse you and console you — and most of all, a gateway, to a better and happier and more useful life.</p>
</blockquote>
<p>And because of that, and more, libraries deserve our patronage. They deserve to be protected.</p>
<hr />
<blockquote>
<p>In principle and reality, libraries are life-enhancing palaces of wonder.</p>
<p>— <a href="https://en.wikipedia.org/wiki/Gail_Honeyman">Gail Honeyman</a></p>
</blockquote>
<p>My level of annoyance, along with my gorge, rises considerably when someone or someones suggest (or outright state) that libraries <em>must</em> be run like a business. Or, worse, that libraries <em>must</em> compete against the likes of Amazon.</p>
<p>Libraries, at least the ones of the public variety, aren’t businesses. They’re not meant to be businesses. Libraries are a public resource — like parks, playgrounds, roads, footpaths. They exist for use by and for the betterment of <em>everyone</em> in our society. Not just people who can afford to access them.</p>
<p>While you can put a monetary value on a library building and what’s within its walls, you <em>can’t</em> put a value on what a library brings to a community. On what a library <em>means</em> to a community. You can’t put a price on the benefits that a library brings to a wide swath of people. You can’t quantify in dollar figures (or whatever your local currency is) the wonder that a library can imbue in a person, regardless of their age or their background.</p>
<p>Libraries are places of discovery. I can’t count the number of times that, when combing the shelves, I stumbled across a book that I didn’t expect or didn’t expect to like. Hard science fiction from the 1970s and early 1980s. Collections of essays by a variety of writers, living and dead. Classic literature and histories. As a teen, libraries afforded me my first encounters with magazines like <em>The Economist</em>, <em>The New York Review of Books</em>, <em>Times Literary Supplement</em>, <em>Foreign Affairs</em>, and so many more. I learned so much from that early immersion in those publications, and I continue to read them to this day. Often at, or through digital borrowing from, the library.</p>
<p>No business, no bookstore could offer me that. Without, I mean, requesting me to fork out money I might not have had.</p>
<hr />
<p>And, strangely enough, digital has given libraries a new relevance. It’s thrown libraries something of a lifeline.</p>
<p>During the COVID-19 pandemic and lockdowns, — in New Zealand and elsewhere, I’m sure — digital access to materials from libraries was a mental comfort. It was an escape. During those difficult weeks and months, more than a couple of people I know needed a distraction from everything that was going on in both the wider world and in their personal lives. The printed word was a source of that distraction, but those friends and acquaintances didn’t always have the budgets to order books online.</p>
<p>They also couldn’t walk over to their local to pick up a volume or three. Luckily, there were able to download ebooks, magazines, audiobooks, and the like which saved their sanity in small bits here and there. That said, there was a drawback: <em>a lot</em> of other people had the same idea. When, for example, I recommended the novel <em>Slow Horses</em> to my wife, she duly checked the Auckland Public Library’s digital holdings and discovered that she wouldn’t be able to download it for a near triple-digit number of weeks!</p>
<hr />
<p>A big part of the attraction of libraries is the buildings themselves. In <a href="https://www.theguardian.com/commentisfree/2022/dec/28/usa-public-libraries">an opinion piece</a> for <em>The Guardian</em>, Moira Donegan notes:</p>
<blockquote>
<p>[P]ublic libraries are unusually beautiful places, the kind of buildings that make you feel underdressed. In many American cities, the public library ranks among the most ornate and stately fixtures of downtown. They’re erected in early-20th century high style, like the Egyptian revival building at Los Angeles’ Riordan Central Library, or Boston’s neoclassical McKim building.</p>
</blockquote>
<p>The local library in the neighbourhood in which I grew up was a smaller stone and brick building erected in or around 1910. It didn’t inspire the awe in me that libraries in the U.S. inspired in Moira Donegan, but that place did have its own character. Walking through the doors, you entered a chamber that seemed more spacious than it was — probably due to the high ceilings and the lighting that, while not institutional or blinding, gave off enough illumination to both enable patrons to read and to project a sense of warmth. There were shelves of books everywhere, chairs and desks strategically placed. In the back was a large children’s section, with space for younger kids to spread out with their favourite volumes and for librarians to hold regular group readings.</p>
<p>That library was somewhere in which I felt at home. Somewhere I could comfortably and safely spend an hour or two after school or on an wintry or rainy Saturday morning. A place where I could forget my problems and everything happening outside. A place that let my imagination roam without reproach or ridicule.</p>
<p>Even though neighbourhood libraries of a more modern vintage lack the grandeur and charm of their older counterparts, those newer libraries are far from sterile boxes. They have a particular warmth, a particular energy to them. And that warmth and energy is there regardless of how many people happen to be in the building. It’s an intangible that makes every library — no matter how old or new, no matter how large or small — inviting.</p>
<p>As author and librarian Vicky Myron said:</p>
<blockquote>
<p>A great library doesn’t have to be big or beautiful. It doesn’t have to have the best facilities or the most efficient staff or the most users. A great library provides. It is enmeshed in the life of a community in a way that makes it indispensable. A great library is one nobody notices because it is always there, and always has what people need.</p>
</blockquote>
<hr />
<blockquote>
<p>The classroom was a jail of other people’s interests. The library was open, unending, free.</p>
<p>— <a href="https://en.wikipedia.org/wiki/Ta-Nehisi_Coates">Ta-Nehisi Coates</a></p>
</blockquote>
<p>Libraries are places of learning. Not just seemingly academic or esoteric or ephemeral subjects, but also practical skills. Thanks to how-to books, like Chilton Auto Repair Guides and various home repair books, friends and family were able to pick up new skills while saving themselves some money. Skills many of them still use to this day.</p>
<p>The mass of printed material that you can find in or get from libraries offers the most obvious route to learn something new. You have at your fingertips books, magazine, newspapers. From a range of places and publishers, tackling a variety of subjects. Texts that come from multiple points of view, from multiple minds, which encompass multiple experiences and fields of study.</p>
<p>By taking advantage of all of that, you can gain a degree of breadth and perspective. You can pick up a bit (or more than a bit) of knowledge and insight. You might even open the door that encourages you to explore a subject in more depth. You might stumble upon the path to your true vocation or towards an avocation. Best of all, you can do it at your own pace. You can build your own curriculum and carve out your own learning path, rather than following a path that was laid down by others. A path that might not be the right one for you to tread.</p>
<p>That potential for learning doesn’t stop with physical books, either. Via the Auckland library, for example, I get access to a number of courses on LinkedIn Learning. Those courses are more oriented towards developing career and work skills, and they’re not comprehensive by any means. But you <em>can</em> learn the basics of a number of professional skills and can discover whether you want to further pursue a subject.</p>
<p>Best of all, you don’t need to shell out on cent. Not a bad deal at all.</p>
<hr />
<p>Libraries are so much more than what they appear to be on the surface. They give us so much more than we realize.</p>
<p>A library isn’t merely a collection of knowledge and information gathered upon shelves between four walls and under a roof. A library is a place where knowledge and information demand to be touched, to be handled, to be used and absorbed. A library creates a connection between itself, the knowledge and information it houses and the people who visit and use it in the aim of enriching and enlightening them.</p>
<p>I’ll leave you with this thought from Ray Bradbury:</p>
<blockquote>
<p>Without libraries what have we? We have no past and no future.</p>
</blockquote>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
