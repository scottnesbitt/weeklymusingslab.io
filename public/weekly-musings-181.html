<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 181</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 181</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Over the last 168 hours or so, a couple of ideas for this edition of the letter have been pulling at me. I spent as much time as I could trying to tackle both, and what you’re about to read is the one that won the tug of war for my attention.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-relying-too-heavily-on-technology">On Relying (Too Heavily) on Technology</h2>
<blockquote>
<p>I can walk down the street, there’s no one there / Though the pavements are one huge crowd</p>
<p>— “<a href="https://en.wikipedia.org/wiki/I_Feel_Free">I Feel Free</a>” by <a href="https://en.wikipedia.org/wiki/Cream_(band)">Cream</a></p>
</blockquote>
<p>My family has a little monthly ritual. When the first Friday of a new month rolls ’round, we get a takeaway from a local restaurant. It’s a bit of a treat, and since we only do it once every 30 days or so, that treat doesn’t break the bank. It also gives us a chance to sample the variety of dishes offered by various restaurants in our area.</p>
<p>In August, 2022 it was my daughter’s turn to choose what to have. As she usually does, my daughter chose fish and chips. Luckily, there’s a good place for that in small mall a few minutes walk from where we live.</p>
<p>As I waited outside of that shop for our order, I noticed a slightly bewildered young man shuffling back and forth past the short row of other restaurants in that part of the mall. I figured, correctly as it turned out, he was an UberEats or Easi or Menulog or whatever driver on a pickup, looking for the fish and chips joint.</p>
<p>He wasn’t paying attention to his surroundings. Rather, his eyes were glued to the screen on his phone as he tried to find that one shop. The shop I happened to be standing outside of. The shop which had a large sign on its frontage and its name on one of the big windows facing the car park.</p>
<p>The poor fellow walked by me two or three times before I took pity on him. I said <em>Excuse me. Maybe you should look up</em>. He did. And then back at me. If looks could kill, that second glance would have put me 1.8 metres underground on the spot.</p>
<p>What happened that early evening in August isn’t uncommon. And I’m not only talking about it happening with pickup or delivery drivers. How often do we hear stories of people driving into streetcar tunnels or into small (or not so small) bodies of water? Of people who find themselves frustrated by a construction zone when map tells them there’s a clear footpath ahead of them? All of that happens while those people have their eyes focused on a GPS device or a map on their phone, rather than on the wider world (the <em>actual</em> world) outside of those screens.</p>
<p>A story usually like that elicits an amused chuckle or a head shake. We wonder why people didn’t see <em>very</em> prominent and obvious signs or pylons or anything like that. Why they didn’t use their so-called common sense. But stories like that also highlight a bigger problem.</p>
<p>That problem? Offloading simple, basic tasks to our devices. To apps. To online services. And expecting those devices and apps and services to do the heavy lifting for us. Not just wayfinding, but also remembering. Reminding. Managing. Maybe even thinking.</p>
<p>In some ways, the reality that more than a few people regularly experience has become virtual. What’s on the screen in front of them, what apps are telling them has, in some small way, become more real to their brains than the physical world.</p>
<p>Devices and services and apps <em>can</em> be boon. Especially if you’re in an unfamiliar place. Especially if you’re trying to juggle too much. The appeal of using those devices and apps and services is our ability to turn mundane tasks over to them so we can free up parts of our brains to focus on other, more important matters. Like what? I’m not entirely sure. Plans for the next great app or online business? Listening to an intriguing episode of your favourite podcast? Tracking the fluctuations in the price of cryptocurrency? What sauce to have with the midday meal?</p>
<p>I was being (slightly) facetious there. The danger, though, is relying too heavily on those devices and services. Giving control over to technology removes us, to varying degrees, from <em>the now</em>. From where we really should be putting our attention and focus. From engaging with the wider world.</p>
<p>Those devices, apps, and services aren’t infallible. They’re not as flexible or adaptable as we are. At least, as adaptable and flexible as we should be. How many times have you encountered someone (or been that someone) who, when confronted with a discrepancy between what’s on the screen and what’s literally in front of them, muttered <em>This can’t be right …</em> at the reality before them? Well, mon ami, if something is in front of your face then it usually <em>is</em> right.</p>
<p>We need to get back to relying more on our own built in sensors and CPUs. Our eyes. Our ears. Our senses of smell. Our brains. <a href="weekly-musings-178.html">Our adaptability</a>. We need to embrace the not-so-novel idea that the world which we take in with our senses, and process with our brains, is more accurate, more immediate than what we see on a screen.</p>
<p>I’m not entirely sure why some people seem to have come to believe the opposite. Perhaps they’ve been sucked in by hype that modern technology is infallible? That the people and the artificial intelligence behind various apps and services are smarter than we are? That those apps and services can constantly adapt to changes in our physical environment?</p>
<p>None of that is true. Especially not the last point. Believe it or not, change in the physical world can happen faster than technology can react to it. When I moved into the apartment I bought in 2020, and before could send photos, a friend wanted to see what the exterior of the building looked like. Predictably, he turned to Google Street View. Shortly after doing that, he sent an email asking why I’d moved into place that was still under construction. It seems the street view was at least a year behind and was only updated, as my friend found out, around 10 months later.</p>
<p>There should be no doubting the accuracy of what’s in front of your face, of what’s outside of your head, of what exists beyond the confines of a small screen held in your hand. No technology — regardless of the amount of digital sorcery blended in with it — is going to change that.</p>
<p>Technology is a <em>tool</em>. It’s an adjunct to our brains, to our bodies, and to our senses. It’s an extension of us. A supplement. Technology was never meant to be, never <em>should</em> be, a replacement for our brains and our senses. Relying too heavily on technology doesn’t only have the potential to be dangerous to us. It can also start us down the path to forgetting how to do certain tasks for ourselves. To forgetting to engage (whether actively or passively) with what’s around us. That way, it can make us just a little less human.</p>
<p>I’ll leave you with this though: always remember to take the time to look up from whatever screen your eyes are fixed on. You’ll be better off for it.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
