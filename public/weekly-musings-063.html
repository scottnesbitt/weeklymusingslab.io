<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 063</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 063</h1>
       </header>
	   <p>Welcome to this edition of Weekly Musings, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This week, it’s time for another letter about technology. As with my other musings on that subject, I’m not discussing the intricacies of any technology, but a more ephemeral aspect of it.</p>
<p>Before anyone starts reading between lines that aren’t there, understand that this musing isn’t an attack on or an indictment of free and open source software. Open source is the tech world in which I live. It’s the world that I know best and the one with which I have the most experience. What I discuss in the essay you’re about to read applies <em>any</em> technology. In fact, it applies to just about anything.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-open-source-and-the-power-user-fallacy">On Open Source and the Power User Fallacy</h2>
<p>It wasn’t that long ago that the free and open source (FOSS) world wasn’t a pleasant place to be in. If you were someone who lacked technical skills and posted for help in a forum, you were as liable to get belittled as you were to get help.</p>
<p>And woe betide you if you wrote or said something that didn’t mesh with the ideas or beliefs of some corner or the other of the FOSS world. On more than one occasion, I was on the receiving end of some nasty backlash. Yeah, fun times.</p>
<p>Thankfully, things have changed. For the most part, anyway. The free and open source software world is now a lot more open, accepting, giving, and tolerant. There are still pockets like the ones I just described, but they’re fewer and smaller now. But the attitude that you need deep technical skills to be involved in or use FOSS persists.</p>
<p>That was brought home to me in March of 2019. That’s when secure device maker <a href="https://puri.sm/">Purism</a> announced its <a href="https://librem.one/">Librem One</a> suite of secure, open source web apps. I heard more than a few people say <em>Why would you need something like that?</em> Some of those people expressed that sentiment in tones of anger and outrage. More than was warranted, in my opinion.</p>
<p>They all made the same argument: anyone can host and maintain the same, or similar, apps that Purism offers with its Librem One service. All you had to do, one person said, was hop over to a company called Digital Ocean, sign up for an account, create something called a <em>dropplet</em>, and install <a href="https://nextcloud.com/">Nextcloud</a> (an open source alternative to services like iCloud or Dropbox), or some other app.</p>
<p>The people I heard saying that? They were coders. They were system administrators.They were <a href="https://en.wikipedia.org/wiki/DevOps">DevOps</a> people. Folks with more than just a bit of technical <em>nous</em>. Folks who can do that sort of thing while blindfolded and eating breakfast. They were also people who had fallen for what I call <em>the power user fallacy</em>.</p>
<p>What’s the power user fallacy? That’s what I call the belief that if you can do something or something’s easy for you, then anyone can do it or it’s easy for anyone.The reality, though, is very different.</p>
<p>As Jules Winnfield said in the movie <em>Pulp Fiction</em>: “That sh*t ain’t the truth”. Not everyone has the skills to install and configure and maintain their own apps. Not everyone is interested in technology beyond being able to use it. Not everyone wants to learn how to do that.</p>
<p>They just want something that works out of the box, that they can easily connect to using a smartphone app or a web browser. Which explains the popularity of Google’s suite of applications, of Evernote, of Dropbox. Of smartphone apps. Of pre-packaged web applications that someone else runs and maintains.</p>
<p>I’m all for people taking control of their data. I use as many open source web applications that I can. Only two or three of the apps that I use aren’t FOSS, but those are run by small outfits I feel I can trust. But what about taking the DIY approach?</p>
<p>Let’s go back to Digital Ocean, shall we? The contention that it’s easy to set up a web application on Digital Ocean is true. In theory, at least.</p>
<p>In practice? Not so much. Take a look at <a href="https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-nextcloud-on-ubuntu-18-04">these instructions</a> for installing Nextcloud. Many people I know would get past the first few steps. They don’t have the basic knowledge or chops that system administrators, coders, or DevOps people take for granted. That’s not to say Digital Ocean’s guides are bad. They’re not. In fact, they’re some of the better technical documentation I’ve read. The key word is <em>technical</em>. That documentation is for a certain audience, one that’s not the ordinary computer user.</p>
<p>Even before they get to that step, the Digital Ocean website will quickly make many people want to quickly back away. Why? The terminology will scare them off. They won’t understand what a <em>virtual machine</em> or a <em>vCPU</em> is. They won’t know, or care, what <em>Kubernetes</em> is. And the slogan <em>Welcome to the developer cloud</em> … well, that’s not going to entice most computer users to stick around.</p>
<p>FOSS is for everyone. It’s not just for techies, tech hobbyists, hardware and code hackers. It’s for anyone who uses a computer. I have almost no technical skills, and yet I live my life in free and open source software. I have since around 1999.</p>
<p>But I don’t want to get my hands on one or more <a href="https://en.wikipedia.org/wiki/Raspberry_Pi">Raspberry Pis</a> and turn them into my servers. I don’t want to have to sweat and strain to get everything to work for me. I don’t want to have to worry about updating and maintaining my own infrastructure. And on my desktop, I have no interesting in compiling kernels, scripting, or building every application that I use from the code.</p>
<p>I know … in some FOSS circles, that’s blasphemy. That’s how it is.</p>
<p>I have no problems tossing some cash at people like Matt Baer of <a href="https://write.as">Write.as</a>, Mo Bitar of <a href="https://standardnotes.com/">Standard Notes</a>, or Nicolas Lœuillet of <a href="https://wallabag.it/">wallabag</a>. I have no problems turning to companies like <a href="https://system76.com/">System76</a>, <a href="https://zareason.com/">ZaReason</a>, or <a href="https://slimbook.es/en/">Slimbook</a> and paying a bit more for a laptop that comes with Linux pre-installed.</p>
<p>The money I send their way not only supports services and businesses that I respect, but also buys me time. Time to write, to read, to cook. Time to spend with my family and friends. Time to enjoy life.</p>
<p>If having a pre-packaged FOSS suite like Librem One or <a href="https://framasoft.org/en/">Framasoft</a> or <a href="https://apps.disroot.org/">Disroot</a> brings free and open source software into the lives or more people, then I’m all for those pre-packaged suites. Paying for them isn’t evil, either. The people behind them need to keep the servers running, and that’s never cheap.</p>
<p>The next time you’re about to tell someone that some technical task is easy, take a moment to consider the power user fallacy. Are you falling into its trap? And does the person you’re talking to have the same skills and knowledge you do? Are they interested in acquiring them? If they aren’t, then either hold your tongue or point them to an alternative.</p>
<p>Doing that will help spread FOSS further and more widely than assuming everyone has an inner techie they want to embrace.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
