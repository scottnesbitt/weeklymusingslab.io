<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 027</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 027</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>When I look back at some of what I’ve written over the years, I’m never embarrassed. That’s not to say that I think everything I’ve written is great, or even pretty good. But I realize that writing isn’t just an act of putting words and ideas on a page. It’s a matter of <em>growth</em>. It’s a matter of <em>improvement</em>.</p>
<p>Now, if I was writing in the same way I did 10 or 20 years ago, then I’d be embarrassed. I’m not the writer I was. I’m not the person I was. I like to think I’ve gotten better at both.</p>
<p>Sometimes, though, something I’ve written holds up after the passage of several years. Like this week’s musing. I stumbled across it in my archives, and felt the urge to share it with you. I hope you enjoy it.</p>
<h2 id="on-stargazing-with-the-ancients">On Stargazing with the Ancients</h2>
<p>Cities hold many secrets. I’m not talking about the dark ones, hidden in basements and cellars and backyards. I mean the little oases and interesting places that even the locals don’t always know about.</p>
<p>To be honest, many of the major attractions around the world hold little or no interest for me. But there are a countless experiences even just slightly off the beaten path that are more than worthwhile. They give me more of the flavour of the place. Many of those sights seem ordinary. But you can find something new in the ordinary. The ordinary can be beautiful. It can be interesting.</p>
<p>Back in 2006, I spent the end of June and a good part of July in China. My base was Beijing. I could have explored that city for a year and still not have uncovered everything interesting within its confines. To be honest, I try to avoid most larger attractions. The ones that draw masses of tourists. I don’t like crowds at the best of times. A crush of people who aimlessly roam around like cattle doesn’t make the situation any better.</p>
<p>Anyway, there’s much more to see in most cities than the well-known sights.</p>
<p>One of my wanders through Beijing on a morning in early July brought me to <a href="https://en.wikipedia.org/wiki/Jianguomen">Jianguomen</a> subway station. With my then seven-year-old daughter in tow, I departed the station by Exit A and turned left. For no particular reason, I looked up. There I saw a 500-year-old monument to science, and one of the lesser known but most interesting sights in the city: the ancient observatory. Centuries ago, Chinese astronomers attempted to map and understand the sky from this location using quaintly primitive and yet ingenious instruments.</p>
<p>This was a place far removed from the large, domed observatories and huge telescopes peering into the night that we associate with astronomy. The ancient observatory tells an older and different story.</p>
<p>To get to the observatory from the station, My daughter and I wound our way through a couple of narrow streets typical of that part of the city, then stepped through an old gate. A man in threadbare coveralls popped out of a room on my right. I think he was the caretaker and, from the glance I got into the room, I think he also lived on the site. I handed him 40 <em>renminbi</em> (20 for myself and 20 for my daughter) and then made my way to the main courtyard. which was a handful of steps away.</p>
<p>The main courtyard was an area measuring a couple of hundred square metres. Various large astronomical instruments fashioned out of brass were strategically placed throughout. The instruments were replicas — the originals were moved to the <a href="https://en.wikipedia.org/wiki/Purple_Mountain_(Nanjing)">Purple Mountain</a> area of Nanjing during the Sino-Japanese war. Even though instruments were replicas, they were solid and the detailing was beautiful.</p>
<p>I was particularly fascinated by the ornate celestial globe which sat in the centre of the courtyard. The globe allowed China’s ancient astronomers to more easily plot the position of celestial bodies in relation to the earth. The four dragons holding it aloft just added to my fascination with the globe.</p>
<p><img src="images/vByTUUD.jpg" alt="Photo of the celestial globe in the observatory’s courtyard" /><br />
</p>
<p>The instruments found throughout grounds of the observatory were definitely of a different age. An age when science and aesthetics were tightly interconnected. Without exception, the instruments at the ancient observatory combined function with form. They did a job, but they were also works of art. I spent long minutes sitting on one of the benches in the courtyard admiring those instruments, showing my respect to the craftsmen who lovingly made them centuries ago.</p>
<p>Surrounding the courtyard were a set of rooms. Those rooms acted as an ersatz combination of museum and astronomy exhibition. Everything was in Chinese (which I didn’t, and still don’t, read or speak). I was able to deduce that the information on the interior walls charted the history of Chinese astronomy. I can’t be sure how much depth the exhibits went into, but I got the impression that they were a good primer on ancient stargazing.</p>
<p>As interesting as the courtyard was, I really wanted to get a closer look at what I saw from the entrance of the subway station. For that, I needed to go up.</p>
<p><img src="images/IonXhnJ.jpg" alt="Looking up at the top of the observatory" /><br />
</p>
<p>One hundred steps took me and my daughter to the top of the observatory. As in the courtyard, the observatory only contains brass replicas of the actual instruments used 500+ years ago by Chinese astronomers. But if you let your imagination take you back to that time, you can see how this location would have been a perfect spot to observe the night sky. The area immediately surrounding the observatory platform wouldn’t have been blocked by any trees or buildings. On a clear night, an astronomer could easily get a 360 degree panorama of the heavens.</p>
<p><img src="images/Lir5pBZ.jpg" alt="Contrasting old and new" /><br />
</p>
<p>I tried to imagine what a Chinese astronomer in the 16<sup>th</sup> century would have seen. In those days, there was no air pollution blanketing the city, making the skies hazy and wavy. There were no electric lights drowning out all but the brightest stars. The sky would have been clear and beautiful, populated by heavenly bodies of all sizes and formations. They were there to inspire awe in their beauty. The stars were there for intrepid scholars to try to understand.</p>
<p>Strangely enough, the real attraction of the observatory isn’t the set of replica instruments or the viewing platform. Instead, it’s the garden just behind the platform. As with the rest of the grounds, the garden was peppered with various astronomical instruments — mostly instruments for measuring the altitude and attitude of bodies in the sky. What made the garden so attractive was the <em>quiet</em>. I was just a few metres from the hustle and bustle, the din and the roar of the centre of Beijing. But I could barely hear the traffic. It was an irregular, distant din rather than a constant, loud drone.</p>
<p><img src="images/l72pb5S.jpg" alt="The garden at Beijing’s ancient observatory" /><br />
</p>
<p>My daughter and I were the only people in the garden on the day we visited the observatory. It’s not that the garden was difficult to find. But on that day, the main entrance to the garden was being repaired which might have put off other visitors.</p>
<p>The lack of visitors was a bit of a surprise. For the two hours or so we were at the observatory, the only other people I saw there were the caretaker and two or three other visitors (who didn’t seem to be experiencing the awe that infected me). Which was a shame, really. Beijing’s ancient observatory is one of the city’s little treasures.</p>
<p>You don’t need to be interested in astronomy or history to appreciate and enjoy the place. The astronomical instruments, even though they’re only replicas, are works of finely-crafted art. The site is a little oasis in one of the busiest, most crowded cities in the world. That alone is worth the comparatively modest entry fee.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
