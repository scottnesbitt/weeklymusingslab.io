<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 124</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 124</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time ’round, something a bit different. I hope you enjoy it.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-the-phone-booth">On the Phone Booth</h2>
<p>There’s a lonely, almost anachronistic object that stands just around the corner from my apartment building. It’s an upright rectangle, just over two metres tall, encased in glass. I’m not sure how many times over the last nine or so months I walked past that object and barely noticed it. And when I <em>did</em> notice it, I don’t ever recall seeing someone inside it. I don’t recall seeing anyone using the smaller, but still bulky, blue box inside that rectangle.</p>
<p>Why would they? Chances are someone who needed to make a phone call would do so with the little device they carry with them at practically all times.</p>
<p>Yes, that rectangle, adorned with red graphics and white lettering, is the sole phone booth in my neighbourhood. The only one, to be honest, that I’ve seen for kilometres around. And one of the few that I’ve seen in the city in recent years.</p>
<p>I’m not even sure if the phone inside the booth works. If anyone <em>does</em> use that booth, they never step inside. They just use the wifi hotspot the booth offers (assuming they have a mobile plan with by Spark Telecom, the firm that owns the booth). In fact, like me, I’m sure most people don’t notice that phone booth. It’s so out of place that it’s practically invisible.</p>
<p>How times have changed.</p>
<p>I’m not sure what it’s like in the rest of the world, but in New Zealand the number of phone booths on city streets seems to have dropped way. Only a few dot the urban landscape — a reminder of a not-so-distant time, a reminder of a time <em>before</em> just about everyone carried their phones in their pockets or bags. A time when you made a phone call in public by stepping through an hinged door, picking up a heavy receiver, shoving a coin or two into a slot, and turning a dial with your index finger or punching metal buttons with your forefinger.</p>
<p>It was in recent memory that phone booths were a ubiquitous, almost integral, part of the urban landscape. They were a part of the <a href="weekly-musings-046.html">information inscribed</a> into a city’s fabric. They were part of daily life, whether you used them frequently or at all.</p>
<p>The phone booth represented the democratized point-to-point and person-to-person voice communication. There was a time when few people had the means, or even a good reason, to install a phone line in their homes. But as phone booths spread far and wide, those people could make calls when they needed to. They could call who they needed to, at any hour.</p>
<p>And phone booths were everywhere — on street corners, outside of convenience stores and libraries and post offices, at petrol stations, in restaurants and diners, in the lobbies of office buildings, in bus and train stations. In just about any place in a city in which large numbers congregated.</p>
<p>Not only were they everywhere, but phone booths took many shapes. Some were the traditional upright glass rectangles with folding doors. The interiors of phone booths in posher locales were surrounded in wood panelling with comfortable seats. Others weren’t booths at all, but one in a row of small cubbies literally bolted on to a wall.</p>
<p>In many ways, phone booths were the original mobile phones. They enabled professionals and working people to stay in touch with the office when they needed to. You had reporters calling the <a href="https://en.wikipedia.org/wiki/Rewrite_man">rewrite desk</a> to file a story. You had sales and delivery people calling office to check in or to get next assignment.</p>
<p>It wasn’t just people on the job who used them. You could pop into a booth to ring home, reassuring family that you were safe or that you’d be home late. By slipping out to a phone booth, you could talk to your friends or boyfriend or girlfriend away of the prying, critical earshot of parents and siblings. You could connect with family in another part of town, or even another town. You could order from a local shop. All for the cost of a coin or two.</p>
<p>You didn’t need to worry about signal strength or whether your mobile’s battery had enough charge to complete a call. You didn’t need to concern yourself with roaming charges. No matter where you were, there was probably a phone booth waiting to swallow your coins and for you to dial a number.</p>
<p>Overseas, phone booths have traditionally been a lifeline for travellers. They’ve been a quick, easy, and familiar way to call home or to call for help. When travelling around Japan in early 1990s, every couple of weeks I’d buy $20 worth of phone cards, shove them into a slot in a public phone capable of making international calls (and there were quite a few of those in Japan!). From there, I’d dial home to check in, to get some news, and to maintain a connection with the people I needed to maintain a connection with.</p>
<p>Admittedly, those cards didn’t last all that long (international dialling before VOIP definitely wasn’t cheap), but those cards and those phone booths did their jobs. And while considerably more expensive, making a long-distance call from a phone booth was faster and more immediate than sending a letter by air mail.</p>
<p>Earlier in this musing, I mentioned that phone booths were part of the information lattice that overlaid a city. That went well beyond communication. Phone booths were miniature information hubs, thanks to the two thick books — one containing business numbers, the other personal — that hung in most booths. Those books packed more than alphabetical lists of names and strings of associated numbers. They also held quite a lot of useful information. Some of that was at the front of the book, including what to do or who to call in an emergency. There were also listings for certain businesses, listings that you can compare to the promoted ads which pop up when you use search engines.</p>
<p>You could also use the information in those phone books to figure out where you were. If, say, you were looking for a business in a part of the city you weren’t familiar with and forgot its address, you could look that business up. By comparing its address to your location, you could see how close (or not) you were to it. And, if push came to shove, you could call <a href="https://en.wikipedia.org/wiki/Directory_assistance">directory assistance</a> to get a helping hand. Again, kind of like using a search engine but one powered by people.</p>
<p>Something that we forget is that phone booths also offered a form or two of protection that you could never get from a mobile phone or smartphone. On more than one occasion, and in more than one country, I found myself darting into a phone booth to escape a sudden rain shower when there was no other shelter around. A self defense instructor I knew in Canada taught students how to barricade selves in phone booth, by wedging themselves into the small space and using their legs to hold the door closed, if they were being attacked. Try to do either with your phone.</p>
<p>As I mentioned a few paragraphs back, you could use a phone booth to have conversations away from the prying ears of family and friends. Phone booths also offered a degree of privacy when you were out in public. When you shut the door, you were in your own little cone of silence. Well, maybe not silence but phone booths did block you off from the rest of the world, at least for a moment. You didn’t need to worry about others hearing one side of your conversation — or, as happens these days, both sides of a conversation when callers are talking (usually loudly) while on speaker phone.</p>
<p>Not bad for an obsolete technology …</p>
<p>Sooner than I expect, there will come a time when even fewer phone booths take up a patch of urban landscape. There will, sooner than I expect, also be a generation of people who will see a phone booth and wonder what it is, what it’s for. And there’s a good chance they won’t believe anyone who explains phone booths to them.</p>
<p>Another piece of history is fading away. I’m not sure if that’s a good or bad thing, but I <em>do</em> believe that it’s important we don’t forget this technology of the recent past. That we don’t forget its place in shaping how we communicate.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
