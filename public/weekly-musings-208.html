<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 208</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 208</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Technology and I aren’t as tight as we used to be. There are more than a handful or reasons for that, some of which I outline in what you’re about to read. This edition of the letter is less a flowing argument and more of a long rant. There’s quite a bit I need to get off my chest about this subject, and there will be more in the future.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-criticizing-tech">On Criticizing Tech</h2>
<p>A few days before putting fingers to keyboard to type out this musing, I glanced at the list of books that I planned to read and reread 2023. What struck me was that a large portion of the books on that list are about technology. Specifically, <em>criticisms</em> of technology.</p>
<p>I’m not sure how that came about, but it wasn’t a deliberate course that I charted when I cobbled the list together late in the previous year. Perhaps it was an unconscious reflection of my shifting relationship with technology and the industry that spawns it. Perhaps it was a reflection of my thoughts and opinions of feelings about technology and the companies behind it.</p>
<p>What I’ve read so far in 2023 didn’t merely reinforce those ideas and opinions and feelings. What was on those pages opened my eyes to new ideas and opinions as well. The books I’ve read expanded my thinking around tech and what it’s good for, where it falls flat, and areas in which more than a handful of companies need to be held accountable for their arrogance and hubris.</p>
<p>My criticism of tech comes not as a knee jerk reaction. It comes from more than a few years of observation, from reading, from taking the time to think, and from being employed in technology sector for a majority of my working life. In some ways, my criticism seems like I’m biting the hand that (literally) feeds me. It’s not.</p>
<p>Over the years when I’ve voiced my criticisms, whether in writing or orally, I’ve been called a <em>hater</em> or <em>jealous</em> by various cheerleaders who worship at the altar of technology, of technology firms, and of the people who founded and who run those firms. People who worship to the point at which they stop thinking and reflexively spout epithets instead of pausing to consider the arguments of others.</p>
<p>But I believe that criticizing tech is important. I believe it’s necessary.</p>
<p>Technology companies, for the most part, aren’t (despite the self-serving stories that their founders and PR departments spin) bastions of great ideas and innovation and creativity. They’re not, for the most part, on missions to improve the world or to help humanity to progress.</p>
<p>The tech lords in Silicon Valley and beyond don’t know better than us. They don’t know what’s best for us, regardless of what they and their followers say (and say often <em>and</em> loudly). The technology that they create isn’t always for the benefit of you or me.</p>
<p>Those firms are in it for a buck. More than a buck, in fact. As many bucks as they can rake in. And not many care about how they earn that money. They’ll permit toxic ideas to propagate and spread because that draws in more eyeballs. They enable lies and disinformation to fan out because the result is more <em>engagement</em> which drops more cash into their tills. They’ll continue to help keep certain groups on the margins and in fear because that’s what sells.</p>
<p>Tech isn’t always making the world a better place. Instead, it’s regularly causing more damage rather than closing rifts and wounds. It’s driving us apart instead of pulling us together. In more than a few cases, tech is amplifying the worst of us with algorithms that wrap us in bubbles of ideology, of group think, of misinformation, and of lies. Bubbles that are difficult to burst and to escape.</p>
<p>What about the promise of innovation? Most tech these days is hardly that. Often, what we get are solutions looking for problems. The people behind that tech believe it’s useful, believe it’s fresh, believe that since it appeals to them it’ll appeal to a wider audience. More to the point, they believe what they’ve created is a potential platinum mine. So they bootstrap or (more likely) turn to venture capital for funding to get those ideas off the ground.</p>
<p>As Adrian Daub points out in <em>What Tech Calls Thinking</em>:</p>
<blockquote>
<p>There is a tendency in Silicon Valley to want to be revolutionary without, you know, revolutionizing anything.</p>
</blockquote>
<p>Instead of something truly innovative, we often wind up with a variation on an existing theme. Or worse, a gadget that’s locked down — like those much-hyped juicers from the mid-2010s, which only worked with bags of raw material from the maker of the juicers.</p>
<p>Related to that is what’s essentially the homogenization of tech. We get multiple apps and tools in the same space or niche, all competing for that precious slice of the market, for that bit of so-called <em>mindshare</em>. But there’s little to differentiate between them, either visually or functionally — perhaps to make it easier for customers to jump ship when a newer, shinier, far more hip tool hits the market?</p>
<p>That technology does more or less do the same thing. Often, it all looks alike (with a few variations). Often, their creators bolt on features and <em>functionality</em> (whatever that means) which they hope will make their app or tool stand out. Which will increase the appeal of that app or tool at expense of its rivals. And that cycle keeps repeating itself until a seemingly newer, shinier, sexier app or tool enters the market.</p>
<p>For all the talk about decentralization in the tech world, there’s a lot of centralization happening. I’m talking about the dependency on the so-called cloud. In so many cases, you can’t use a lot of new technology unless 1) you’re connected to the internet, and 2) have a direct line to someone else’s computer. The dangers of that are obvious.</p>
<p>First, you’re locked into the ecosystem maintained by the company behind that technology. You can exchange data between the apps and services of that company. That might not always be easy, but it can be done. Try moving away, and the story can be <em>very</em> different. You might be able to export your data, but will it be in a format that you can easily use elsewhere? Not always (although that seems to be changing slowly).</p>
<p>Second, you’re leaving yourself open to outages. Which do happen, even in these days of fault tolerance and high availability and all that sort of thing. It’s not just the company’s infrastructure being compromised for a short period, but also the chance that the business on the other end of your internet connection might pull the plug, radically change its business model, or go out of business. That leaves you, and what you do with their wares, at the mercy of those companies. Too much control, control that they shouldn’t have, is in the hands and on the servers of those firms.</p>
<p>Third, there’s your data. You don’t know how it’s being used. You don’t know who can see it. You don’t know whether or not your data is being sold, to whom, and how the buyers are using the data. All you can be sure about is that none of that is in your best interests.</p>
<p>Tied up in your data being collected en masse is one of the great sins being perpetrated by technology firms: <em>surveillance</em>, in many forms. That surveillance is almost constant and it’s done in the most innocuous ways. From your phone and apps calling home to tracking cookies dropped into your web browser’s cache to tracking pixels in emails, there’s always someone looking over your shoulder.</p>
<p>In <em>The Age of Surveillance Capitalism</em>, Shoshanna Zuboff notes that:</p>
<blockquote>
<p>[E]ven the most innocent-seeming applications such as weather, flashlights, ride sharing, and dating apps are “infested” with dozens of tracking programs that rely on increasingly bizarre, aggressive, and illegible tactics to collect massive amounts of data.</p>
</blockquote>
<p>Technology companies can follow you wherever you go, whether in the online or physical worlds. They know where you’ve been. They know what you’ve done, they’ve looked at what you’ve bought or considered buying. Those firms know when you’re sleeping (and what your heart rate is when you’re asleep), they know when you’re awake. Thanks to all that surveillance and tracking, those companies know when you’ve been bad or good, and reinforce that behaviour.</p>
<p>But why? What benefit does that have? For you and to me, none at all — surveillance works <em>against us</em>. For technology companies, it’s more data about you, about your behaviour that they can use and sell. That they can profit from, while your privacy (or expectation of privacy) is shattered like glass. Your rights to privacy and to anonymity are battered about like a <a href="https://www.thebigad.com/2021/07/31/1970s-american-tourister-luggage-gorilla-commercial/">gorilla trashing a piece of luggage</a>.</p>
<p>That’s not to say that there isn’t technology out there that’s worth supporting. That’s not to say there are technology companies and projects actually trying to benefit the world rather than merely pad some bottom line. We just don’t hear about a lot of them because they’re not sexy enough to be worth a journalist’s time. We don’t hear much about them because they’re not netting record funding. We don’t hear much about them because their <em>story</em> isn’t deemed worthy of column inches. We don’t hear much about them because the folks behind those technologies aren’t proclaiming their greatness and genius at the top of their lungs to anyone and everyone.</p>
<p>While there are bright spots out there, the technology industry has <em>a lot</em> to answer for. That industry isn’t, and shouldn’t be, immune to the glare of criticism. Especially when it’s criticism that’s well deserved. Any criticism of tech firms is valid if what they’re doing is antithetical to our interests, to our needs, to our safety. We have every right to level criticism in their direction if that criticism is valid, and not just sour grapes or the result of feeling disappointed or aggrieved.</p>
<p>We have every right to criticize the technology world if the actions that those who inhabit that world take are unethical. If those actions breach our privacy. If those actions violate our basic rights and put us at risk. If those actions erode our personal or collective dignity.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
