<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 019</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 019</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>In this week’s letter, I ponder something that hits very close to home for me. No, make that it hits right at home. You’ll understand once you start reading.</p>
<p>So, let’s get to it, shall we?</p>
<h2 id="on-autism-and-raising-a-child-with-autism">On Autism, and Raising a Child with Autism</h2>
<p>For what seems like the thousandth time, my wife turned to me and said <em>Some people have strange ideas about autism</em>. And, for what seems like the thousandth time, I had to agree with her.</p>
<p>I blame most peoples’ perception of autism on the 1988 movie <em><a href="https://en.wikipedia.org/wiki/Rain_Man">Rain Man</a></em>. In that movie, Dustin Hoffman played the character Raymond Babbitt, an autistic man who’d been institutionalized since childhood and who, while he knew nothing of ordinary life, could perform remarkable feats of observation, calculation, and memory.</p>
<p>Hoffman’s portrayal of Babbitt was powerful and nuanced and sympathetic. Unfortunately, that portrayal also cemented a very narrow view of autism and people with autism in the minds of the wider public.</p>
<p>Not all people with autism are like Dustin Hoffman’s character in <em>Rain Man</em>. Autism is considered a <em>spectrum</em> (hence, the affliction being dubbed <em>Autism Spectrum Disorder</em>). It affects different people in different ways.</p>
<p>Often, there’s a common thread of symptoms and characteristics linking all people with autism, but rarely are two cases the same. One person with autism may not be able to speak or interact with others, while another may be <em>very</em> high functioning like the renowned animal scientist <a href="https://en.wikipedia.org/wiki/Temple_Grandin">Temple Grandin</a>.</p>
<p>So, what’s it like for someone with autism? Again, it varies from person to person. In his book <em><a href="https://en.wikipedia.org/wiki/The_Reason_I_Jump">The Reason I Jump</a></em>, Naoki Higashida summed autism up very succinctly and eloquently:</p>
<blockquote>
<p>For people with autism, living itself is a battle.</p>
</blockquote>
<p>My daughter was diagnosed with autism before her third birthday. She describes autism as hearing butterflies making <em>shoosh-shoosh</em> noises in her head, so she needs to chant or sing to block those noises out. She also notices and remembers the wrong thing. On the first day of school in 2017, for example, she recalls that her English teacher wore a white shirt and brown pants but she doesn’t recall anything the teacher said.</p>
<p>Often, people with autism (especially younger ones) have trouble dealing with visual and aural stimulation. Sights and sounds we can filter out can quickly overwhelm them. Imagine the worst case of sensory overload you’ve experienced. That, for many people with autism, is a typical day.</p>
<p>Autism affects parents and family, too. You find yourself, to paraphrase something I read, mourning the loss of the child or grandchild or sibling you thought you’d have. You experience waves frustration and pain and anger with having to deal with the government, with schools, with the medical establishment. You regularly hit walls of frustration and of pain trying to ensure that your child has as normal a life as s/he can.</p>
<p>There’s no break or time off, either. Being the parent of a child with autism can be a 24/7/365 job. You’re constantly taking care of them because they either can’t do everything for themselves or they’re not aware of the various dangers outside the home. Even high-functioning people with autism have meltdowns where and when you least expect those meltdowns to happen, and you need to be there to calm them.</p>
<p>Worse, parents of children with autism have to put up with the stares — varying from pitying to accusing — when out in public. Sorry, folks, none of us can help that we didn’t dodge a certain genetic bullet like you did. And your stares don’t have magical properties …</p>
<p>Having a child with autism can wear you out physically, emotionally, and mentally. Not just by having to continually deal with your child’s affliction, but also due to the slings and arrows (often silent) of strangers. Strangers who have no inkling of what your child is going through, what you’re going through.</p>
<p>That all sounds bleak, doesn’t it? For some people, it is. But there is something that can be done. That something? <em>Therapy</em>. Specifically, a therapy called <a href="https://en.wikipedia.org/wiki/Applied_behavior_analysis">ABA</a> (short for Applied Behavioural Analysis). At its core, ABA teaches kids with autism new skills and new habits using a system of repetition and reward. It’s very intense, and can be draining for both the therapist and the person receiving the therapy.</p>
<p>ABA, however, can be <em>very</em> effective.</p>
<p>When my daughter was diagnosed with autism, my wife and I were told our daughter would never be able to talk or read, to go to school or take care of herself. Thanks to a lot of effort from various therapists, my daughter can speak and write (although her expressive language is weak), read, and she attends a regular high school in New Zealand. She’s learned to play the cello and several other instruments and even taught herself how to read music. My daughter can also perform many simple, day-to-day tasks various people said she’d never be able to. With a lot of hard work, she even finished high school in early 2019.</p>
<p>While ABA works, it’s also <strong>very</strong> expensive — costing $20,000 (USD) or more each year. Usually, more. That puts it out of the reach of many families. When our daughter began receiving ABA, my wife and I went into quite a bit of debt funding that therapy even though I’ve had a series of well-paying corporate jobs over the years. I don’t begrudge one cent of what we spent or of the debt we took on, though, because it helped my daughter immensely. The results of all that therapy also proved the doctors and certain educators wrong, which itself is priceless.</p>
<p>My daughter may never live an unassisted life, but her quality of life is far greater than it would have been thanks to the therapy she did receive.</p>
<p>It would have been nice to have received some support from the government, though. My tax dollars at work on all that. It’s not how events played out. I’ve lived in two countries where governments don’t fund ABA or won’t fund for it everyone. The previous government of New Zealand, for example, refused to set aside funding because that government (despite mountains of evidence to the contrary) was unsure that ABA would work <em>in the New Zealand context</em>.</p>
<p>Think about that for a moment. Like you, I don’t understand what <em>in the New Zealand context</em> means. Are Kiwis with autism different from those in, say, Japan or the UK? Do the needs of autistic people in New Zealand differ that greatly from those of their counterparts elsewhere in the world? I see it merely being an excuse not to spend the money helping people in need.</p>
<p>The hope that ABA provides is often overshadowed by government penny pinching. Too many people, who could lead productive lives, are being ignored or abandoned. Others are being denied a better life and to be less a burden on the taxpayer by sheer ignorance and inaction.</p>
<p>That’s short-term thinking, which politicians and policymakers seem to be very adept at. As I mentioned earlier, ABA costs $20,000 (USD) or more per year. But research by the California Bureau of Insurance research estimates it can cost $2 million (USD) or more for the state to care for a person with autism over the span of their adult lives. In New Zealand, for example, an autistic man named Ashley Peacock has been institutionalized and his care costs $1 million (NZD) each year. ABA is definitely cheap by comparison.</p>
<p>Not every person with autism will grow up to be a <a href="https://www.simonandschuster.com/books/Carlys-Voice/Arthur-Fleischmann/9781439194157">Carly Fleischmann</a> or a Temple Grandin. That said, most people with autism don’t need to be shunted out of sight, out of mind into institutions or wholly supported by the state, either. Many people with autism have potential, potential that remains untapped. Isn’t it better in the long term, for them and for society, to help them realize that potential than to stagnate because a short-sighted bureaucrat doesn’t want to make an investment in a child’s future?</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
