<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 081</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 081</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Over the last several weeks, I’ve been reading quite a bit about topics related to this week’s musing. Much of that reading dovetails with my thoughts and ideas, which resulted in what you’re about to read.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-what-to-keep-and-what-to-delete">On What to Keep (and What to Delete)</h2>
<p>Whether we intend to or not, we collect a lot of digital cruft. Piles of bookmarks. Notes. Articles stored in read-it-later apps. Unread feeds in our RSS readers. And more.</p>
<p>We become digital pack rats. We figure that we’ll get a chance to look at all those things we’ve saved when we have time (whenever that is). The problem is that most of what you save and collect <em>won’t</em> be useful in the future. Most of it has a short shelf life anyway, a shelf life that’s ended long ago.</p>
<p>Often, we wind up with all that digital weight because we spent time creating or finding it. We can’t bear to part with it because it’s <em>ours</em>. We hold on to it because we <em>can</em>. While every bit and byte you’ve accumulated might be of minor interest to a digital archaeologist in the future (and that’s doubtful!), it has little or no use in 2020. Or beyond.</p>
<p>Let me put it into perspective with an observation Gerry McGovern makes in his book <em>World Wide Waste</em>:</p>
<blockquote>
<p>80% of all digital data is never accessed or used again after it is stored, according to a 2018 report by the Active Archive Alliance.</p>
</blockquote>
<p>Instead of letting that backlog build up, you should delete it. Mercilessly.</p>
<p>When doing that, you need to be ruthless. You need to be merciless. You need to be brutal. You need to use a chainsaw and not a scalpel to cut away everything you don’t need.</p>
<p>Why? This has nothing to do with me (or you) going full Marie Kondo. This has nothing to do with whether or not something brings you joy. This has <em>everything</em> to do with being kinder to the environment and removing friction (even if it’s just a little friction) from your life.</p>
<p>Storage has become cheap in recent times. Shockingly cheap. Because of that, people shovel tens of gigabytes of files into the so-called <em>cloud</em> via services like Dropbox, Nextcloud, iCloud, and others. Those online spaces become like their attics — corners of their houses in which they pile box after box of old … well, whatever it is they pile up there.</p>
<p>The cloud isn’t a magical land where data floats around like ephemeral pixie dust. No. The cloud is a bunch of physical computers sitting in physical facilities dotted across the globe. The files you save to the cloud take up space on those computers. Those computers use electricity, and a lot of it. They pump out heat. They often run on polluting, non-renewable sources of power. <a href="https://www.datacenterknowledge.com/industry-perspectives/data-center-dilemma-our-data-destroying-environment">According to some models</a>, “data center energy usage could engulf over 10% of the global electricity supply by 2030”. Imagine the amount of energy used, and the amount of carbon emitted, to store your files online.</p>
<p>Then there’s the impact on your time.</p>
<p>Even if you’ve organized your files and whatnot, you’ll still spend an inordinate amount of time finding what you want to find under the digital strata you’ve accumulated over the years. Don’t get me wrong: I’m not one of those productivity hackers who believes that your life would be infinitely richer if you shave a second off the multitude of little tasks you perform during the day. That said, is your time best served clawing through a virtual pile of possessions to find that one shiny object?</p>
<p>Earlier, I wrote that you need to use a chainsaw and not a scalpel to cut away all the digital cruft you don’t need. If you’re having trouble deciding of what to delete, here are a few things to keep in mind and a few questions to ask yourself:</p>
<p><strong>Get out of the contingency mindset</strong>. What makes many of us save as much digital stuff as we can? Often, it’s the idea that we might <em>need</em> that document or snippet or note or link <em>one day</em>. That’s a state of mind that I call <em>the contingency mindset</em> — you’re thinking about every eventuality, every outcome, every contingency.</p>
<p>You need to drop that mindset. Why? Except in rare cases, that <em>one day</em> you’re waiting for never comes. So why waste time and mental energy on planning for that day?</p>
<p><strong>What have you forgotten aboutOn Teaching (and Learning) Skills, Not Software?</strong> There’s a lot buried in our digital archives (even if you don’t consider what you have as archives in the strictest sense). Files you haven’t opened in months or even years. Bookmarks on top of bookmarks. Notes you haven’t glanced at. As you continually add more and more to your digital pile, you forget about what’s in the layers below. Worse, you forget about <em>why</em> you saved it in the first place.</p>
<p>Until a few years ago, I coached a handful of writers. One question that came out of all of their mouths and keyboards was “How do I deal with all the ideas I have?” My advice was to delete any idea that you haven’t been able to get to within six or eight weeks of that idea springing into your brain. There’s a good chance that you’ll never get to that idea. Ever.</p>
<p>Why not apply that rule to what you’ve been hoarding digitally?</p>
<p><strong>Can you open and view that information?</strong> A while back, I found a handful of files on a backup drive. Those files had an extension that I didn’t immediately recognize. After a couple of minutes of puzzling over those files, I remember that I created them using a shareware word processor back in the mid 1990s. A word processor that no longer exists. Worse, there’s no easy way to peek into those files to see what’s in them.</p>
<p>We all have something like that lying around. It could be a spreadsheet, a document, an email backup, an archive, or an image in a format used by software that’s no more. If you can find a version of that software, it probably won’t run on newer computers and newer operating systems. You can try to find an application to open or convert those files, but that isn’t worth the effort.</p>
<p><strong>How much of that is still current and relevant?</strong> And how much of it has any <em>actual</em> value rather than sentimental value? Little, if any, I’d say. Do you really care about that dessert you bought in that small shop in <a href="https://en.wikipedia.org/wiki/Ura-Harajuku">Ura-Harajuku</a> back in 2017, the one you carefully photographed before scarfing it down?</p>
<p>While purging files from my backup hard drive recently, I found a couple of folders filled with text, HTML, and word processor files. Most of that was articles and essays I wrote early in my career. Articles which I banged out while I was trying to establish that career and while I was trying to find my voice as a writer. Articles that appeared in publications that went under long ago. Articles on topics that are no longer of passing or even historical interest today. I knew I couldn’t rework or try to sell those pieces again. Trying to do so would have been a waste of time. So into the digital dustbin all of that went.</p>
<p><strong>Will you ever really need any it?</strong> I know a couple of people who proudly boast that they have <em>every</em> personal email they’ve sent and received since the early 1990. Thousands, tens of thousands of messages, replies, attachments.</p>
<p>So what?</p>
<p>They’ve never looked at those emails in the last 20-odd years. They probably won’t in the next 20 years, or longer. Anyway, the contents of most of those emails is out of date. Beyond nostalgia, there’s no reason to keep them.</p>
<p>Saying that you keep something because you <em>might</em>, just might need it one day is silly. If you think that, then things will just keep piling up. You’ll never keep up. Don’t waste your time and energy trying. There are better things to do.</p>
<p>Now, if you’ll excuse me, I have a date with my <a href="https://standardnotes.com">note taking tool</a> to do some purging.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
