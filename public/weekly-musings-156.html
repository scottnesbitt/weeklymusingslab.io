<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 156</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 156</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time ’round, an idea that popped into my head for seemingly no reason. I was halfway through another musing, but (as regularly happens) the idea for what you’re about to read dropkicked that other musing out of the ring and took control. So, here it is.</p>
<p>A quick announcement: there will be no edition of the letter next week. A few reasons for that, but nothing scary or sinister. I just need some space to complete a task or four, which won’t leave much (if any) time to tap out a new musing. We’ll be back to our regularly scheduled schedule on May 11, 2022.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-the-to-do-list">On the To-Do List</h2>
<p>The to-do list. The task list. Whatever you want to call it, a to-do list is (in many ways) the centerpiece of most systems for productivity.</p>
<p>The to-do list is also many things to many people. It can be useful, but it can also be a crutch. It can be a source of exhilaration and a source of stress and frustration. It can be a guide or a millstone around your neck.</p>
<p>More often than they should, people have a list of tasks that can go on for pages (whether digital or analog). Looking at a list like that can be daunting. It can be demoralizing. It can block you from starting to do the work that you need to do.</p>
<p>But does it have to be that way? I think not. But before we get to that, let’s look at what’s wrong with the to-do list.</p>
<h3 id="the-tyranny-of-the-to-do-list">The Tyranny of the To-Do List</h3>
<p>Over the years I’ve seen friends and co-workers and even people I’ve consulted with and coached start using to-do lists with the best intentions. But they quickly descend into a scary spiral. Their to-do lists morphed into a paper or digital tyrant controlling them.</p>
<p>The to-do list became <em>the</em> focus of their work and their lives. Their lists became too long, too unmanageable. Their lists became, to a degree, a repository for not only their tasks but also for all of their aspirations. They put <em>everything</em> into their to-do lists — what they needed to do, what they <em>thought</em> they should do, and what they wanted to do somewhere down the line.</p>
<p>By doing that, you end up with a bunch of tasks that you never get around to. As a to-do list gets longer, it becomes more frustrating. It becomes more intimidating. You see your list growing. You get an increasingly nagging sense that you’re constantly falling behind. That you’re not productive. That everything is slipping away from you.</p>
<p>More than a handful of enterprising folks have watched that happen, too. And they’ve used the spirit of their enterprise to build and market tool and apps which they claim will help weak-willed mortals corral and control any to-do list.</p>
<h3 id="apps-and-tools-everywhere">Apps and Tools, Everywhere</h3>
<p>Ah, the myriad of tools and apps that purport to make managing your tasks easier. That claim to help boost your productivity. So many of them, and so many that keep cropping up at regular intervals, whether online or on the desktop.</p>
<p>I know or know of more than a couple of productivity hackers who get hung up on to-do list apps. They demand <em>everything</em> from them — a mobile version (whether it’s an app or a website that looks good in a mobile browser); integration with their calendars and email accounts; reminders at certain intervals; powered by AI; a telepathic link to the user. That and so much more. Woe betide any app or tool that comes up lacking …</p>
<p>Way back in 2012, I read what had to be one of the dumbest reviews of a mobile app that I’ve read … well, ever. It wasn’t a professional review; it was left by a user who tried out a to-do list app in the Google Play Store. The reviewer gave the app one star out of five, with the comment <em>Too simple to the point of being useless</em>.</p>
<h3 id="what-should-a-to-do-list-be">What Should a To-Do List Be?</h3>
<p>Call me old school, but I don’t believe that to-do lists (whether digital or paper) are meant to have many, if any, bells and whistles. Bells and whistles that just add a layer or three of complexity to something that should be straightforward. To-do lists should be too simple to the point of being <em>useful</em>.</p>
<p>So what do I believe to-do lists should be? They’re not scheduling tools. They’re not project management apps. They’re not detailed plans. Instead, a to-do list is a <em>reminder</em> of what you need to complete. A to-do list should be short and to point. And it should be <em>very</em> focused.</p>
<p>Although he was <a href="http://atulgawande.com/book/the-checklist-manifesto/">writing about checklists</a>, what Atul Gawande explains also applies to to-do lists:</p>
<blockquote>
<p>They remind us of the minimum necessary steps and make them explicit. They not only offer the possibility of verification but also instill a kind of discipline of higher performance.</p>
</blockquote>
<p>A to-do list isn’t a dumping ground for tasks. It should be less a long list of jobs that you need to slog through and more, as I mentioned just above, of a <em>reminder</em> of what you need to complete on a given day. Your to-do list for that given day should contain, at most, three or four tasks which you can <em>definitely</em> complete (or significantly chip away at) over the space of the day.</p>
<p>Each task on the list doesn’t need to have the detail of an essay or of a long project plan. The description of each task should be a short sentence or even a sentence fragment. If you need to, add a <em>short</em> note to your tasks to give them some additional context.</p>
<p>That said, a to-do list should be <em>fluid</em>. If you don’t finish a task or two, don’t let that stress you out. Instead, shift the tasks that you couldn’t tackle to the next day.</p>
<h3 id="final-thoughts">Final Thoughts</h3>
<p>A to-do list shouldn’t be a source of stress. It should be a tool that helps you focus. A tool that helps you direct your energy and brain power towards what you need to get done at a specific moment in time.</p>
<p>Your to-do list should be a guide, a prompt. Don’t let it grow out of control. And don’t let it oppress you. You control your to-do list, not the other way around.</p>
<p>But to-do lists are only tools, nothing more. As <a href="https://macwright.com/2015/09/10/todo.html">Tom MacWright</a> so aptly pointed out:</p>
<blockquote>
<p>To-do lists do not make you productive.</p>
</blockquote>
<p>Doing the work, with a to-do list as your guide, makes you productive.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
