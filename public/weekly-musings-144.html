<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 144</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 144</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>I don’t have much, if anything, pithy or even marginally insightful with which to kick off this edition of the letter. I think the subject speaks for itself, literally and figuratively.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-radio">On Radio</h2>
<p>When you’re growing up, magic seems to be everywhere. For me, some of that magic came from a box. Actually, a series of boxes. Boxes of different sizes, of slightly different shapes, made from different materials. Boxes packed with wires and gizmos and other electronic bits that I never fully understood. Boxes that grabbed sounds and voices and music out of the air, and piped those sounds through a sometimes tinny speaker.</p>
<p>Magic, indeed. In the form of radio. Magic, at least, to my (then) young ears and mind. And I’m not just talking about the shortwave transmissions and the joyfully mysterious <a href="weekly-musings-067.html">numbers stations</a> that came to grab my attention, but also the AM and FM broadcasting of the day.</p>
<p>In a lot of ways, the radio of my youth was similar to radio of today. In a lot of ways, it was <em>very</em> different. I came of age during the tail end of the adventurous days of radio. While the focus was on playing hits and what was popular at the time, there also a bit more diversity with the programs on the airwaves rather than being completely balkanized according to one type of music or another. One station in Toronto, for example, was known for playing mostly hard rock. That station also gave extensive air time older rock music, to jazz and to the blues.</p>
<p>When I was growing up, talk radio really wasn’t a thing. Sure, you had talk shows and opinion segments on the news, but rarely was it as divisive as what started appearing on airwaves in late 80s and early 90s. A trend which continues today. No, radio at that time was mainly a mix of music and news. Depending on the station, you got more of one than the other.</p>
<p>Aside from Canada’s public broadcaster, the <a href="https://en.wikipedia.org/wiki/CBC.ca">CBC</a>, most radio stations leaned heavily towards the musical side of the fence. News was sprinkled into the mix to break things up at prescribed intervals. And while there was an element of predictability around what issued forth from a radio in those days, you couldn’t <em>always</em> be sure what you’d hear. You couldn’t always predict what song (whether newer or older) would suddenly pop into the rotation.</p>
<p>There were stations, usually cogs in a large corporate machine, that were precisely, almost algorithmically, programmed by dedicated professionals who seemed to closely study to what the listening public wanted (or what they <em>believed</em> the listening public wanted). Stations that, like many today, played only the latest releases. The popular songs that were inching their ways up the various <a href="https://en.wikipedia.org/wiki/List_of_record_charts">charts</a>. But other, smaller, scrappier stations and networks bucked that trend with a variety of programming that constantly surprised and entertained.</p>
<p>Radio wasn’t, and isn’t, only music. There was, and is, more on offer — news, interviews, drama, comedy, opinion, and talk. Radio provides a quick way to get the latest information, often in smaller and more easily digested chunks. It offers listeners a starting point, where they can get the gist of the issues of the day and then, if they so choose, delve deeper with an evening newspaper or a magazine.</p>
<p>With drama and comedy coming through only your ears, you’re forced to use your mind a bit more than, say, while watching something on TV. You have to visualize the scene — where the actors were pretending to be, what was physically around them, the general atmosphere. You have to try to imagine the expressions on their faces, how they’re sitting or standing, all from the tone of their voices and the background music. Truly a <em>theatre of the mind</em>.</p>
<p>Listening to radio news also offered (at least, offered me) a number of lessons in writing. Lessons in how to condense information to its key elements. In how to use audio and what my radio reporting instructors in journalism school called <em>actualities</em> — ambient sounds that brought a bit of extra life to story. To this day, I use a radio news report as my model for good, tight writing — especially when crafting documentation at The Day Job<sup>TM</sup>. Writing that’s tight, that contains only the main points, that uses plain language and reads in the way in which you’d hear it.</p>
<p>But it’s not just what came out of the various radios that I listened to over decades that grabbed and held my fascination. There were also the radios themselves. At least ones manufactured until late 1950s and early 1960s.</p>
<p>Early radio sets were large. They were bulky. Inside, there was a landscape of wires and vacuum tubes and other bits and bobs that made those radio do what they did. On the outside, though, those radio sets blended in with their environment. Didn’t look out of place in a living room or sitting room — solid wood cases, of different shades of brown, some about the height of a small child. Some utilitarian and some ornate. Others, while still bulky, could be set on a table. Some of their shapes had a more pleasing curve, like an oval with bottom half lopped off.</p>
<p>In my weaker moments, I consider those radios something akin to works of art. They were definitely the product of skilled craftspeople.</p>
<p>The glowing dials and knobs for tuning and adjusting the volume encouraged you to touch, to interact with a radio. Listening to those sets could be as much a tactile experience as an aural one.</p>
<p>Later, there was a raft of radios fashioned out of a then-wondrous material called <a href="https://en.wikipedia.org/wiki/Bakelite">Bakelite</a>. Radios that were generally smaller than many of their contemporaries, with a starker, more minimal appearance. Despite their appearance, Bakelite radios weren’t cold, sterile devices. Rather, they radiated a futuristic look and feel. Eventually, though, radio sets became very generic, very homogeneous — much like modern websites created with various frameworks and content management systems. There’s nothing wrong with those radios, but they lack a certain style. They lack a certain personality and warmth.</p>
<p>These days, terrestrial radio really doesn’t grab or hold my attention. Neither do modern radios. As I mentioned a few sentences ago, both are a bit too homogeneous, too generic for my taste. The music being broadcast, for most part, doesn’t appeal to me. News and comment doesn’t hold my attention in way it did. One exception to that is more than a bit of the programming from <a href="https://en.wikipedia.org/wiki/Radio_New_Zealand">Radio New Zealand</a>, which reminds me of CBC I remember from the 80s and 90s.</p>
<p>But is the landscape of radio today really as bleak as I seem to think it is? Is radio, as some people have hinted, doomed to fade away? The answer to both questions is <em>No</em>.</p>
<p>Radio’s saviour, in some ways, is the internet. Internet radio stations offer more variety in way of music — everything from <a href="https://somafm.com">Soma FM</a>’s mix of musical genres to the mix of stations available via Radio Caprice. That variety holds my interest and pops up more than a few surprises. Far more than terrestrial radio does.</p>
<p>And let’s not forget <a href="https://en.wikipedia.org/wiki/Campus_radio">campus radio</a>, which dares (and <em>always</em> has dared) to take chances with its programming. Which gives attention to local and lesser-known musical acts. Which, whether deliberately or not, spots trends and tried to shape the taste of its listeners. Which radiates a love of music, regardless of its genre.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
