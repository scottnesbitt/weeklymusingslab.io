<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 147</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 147</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time ’round, a topic that relates to what I pondered in more than a couple of previous editions. And it’s a topic that’s been, in a number of forms, on my mind quite a bit over the last several weeks.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-using-one-tool-for-everything">On Using One Tool For Everything</h2>
<p>In late 2012, productivity blogger Mike Vardy did an experiment that he dubbed <em>Everything in Evernote</em>. The name of that experiment says it all: it was Vardy’s attempt to do everything that he needed to do using the popular note taking tool Evernote.</p>
<p>As I recall (and it was a few years back, so forgive me if the details are hazy), that experiment predictably ended with the conclusion that you <em>can</em> do everything in Evernote. It just wasn’t as simple as it seemed.</p>
<p>In summing up his experiment, Vardy wrote (and, again, I’m piecing this together from memory):</p>
<blockquote>
<p>The problem isn’t with Evernote, though. The problem is trying to use Evernote for so much more than what it really is for.</p>
</blockquote>
<p>I can understand why someone would want to do, or try to do, everything with one digital tool. Who wants to be hopping between several apps? Who doesn’t want everything stored, accessible, and ready to work with all in one place?</p>
<p>With one tool, you don’t need to worry about jumping between similar applications to keep your work in sync. You don’t need to worry about the cognitive overhead of trying to maintain and keep track of information in several places.</p>
<p>With one tool, you have a single source of truth. There’s no duplication. There’s no overlap. There’s a smaller chance that something will slip through the cracks.</p>
<p>Often, though, you learn the lesson that Mike Vardy learned: that one tool, while great at what it’s intended for, isn’t really designed to be everything or to do everything. It takes a lot of shoehorning, a lot of smashing a square peg into a circular opening to get things to work the way you want them to.</p>
<p>Just because you can use a tool to do something it’s not meant for doesn’t mean that 1) it will help you do a job more efficiently, or 2) that you should feel obliged to push the tool’s envelope. Using one tool for everything is often like trying to use a coin as a screwdriver. It <em>can</em> get job done in some cases, but doing that job is difficult and messy and slow and sometimes ugly. Often, it takes more time and energy to do anything. The results aren’t always commensurate with the effort that you put in.</p>
<p>Most of the time, one tool <em>isn’t</em> the best option. And it’s definitely no substitute for an actual, fit-for-purpose tool.</p>
<p>In the quest to find that one perfect too, it’s too easy to indulge in a bit (or more than a bit) of <a href="weekly-musings-118.html">tool fetishism</a>. You can find yourself spending so much time looking for and playing with apps and services that you don’t actually get much done.</p>
<p>If you <em>do</em> find something that holds on to your interest, you can find yourself devoting an inordinate amount of time to tweaking, configuring, twiddling and twerning to try to make it yours. Let’s face it: with some tools, to get what you need (or think you need) from them, you must go all in on them. You need to learn their intricacies and quirks, their various ins and outs. You need to be willing to roll up your sleeves and spend a lot of time with the tool.</p>
<p>And that’s time which you’re not spending on doing deeper work. You can argue that the up-front investment of time and learning will be worth it in the longer run — the more you know, the more productive you’ll become. But when will that magical time arrive? And will you actually be more productive when it does?</p>
<p>Does the time that you spend with the tool offer any benefit? It can. More often than not, however, you’ll need to implement a lot of hacks and install a lot of plugins and the like. You’ll need to adapt your workflow to the tool, not the other way around.</p>
<p>You’ll also need to maintain all of that, which (in some cases) can break if you sneeze to hard. In physics, there’s a concept knows as <a href="http://en.wikipedia.org/wiki/Second_law_of_thermodynamics">the Second Law of Thermodynamics</a>. Essentially, the law states that complex systems break down over time. To which I add that people expend a lot of energy and effort maintaining complex systems as those systems are breaking down.</p>
<p>Then there’s the other end of the spectrum, where you have multiple specialized tools for what you need to do. In that place, too, you can spiral out of control with several apps and services and whatnot to do a job. Having all of that adds to your cognitive overhead. It can be difficult, if not impossible, to stitch all of those pieces together. And it’s easy to have several tools that do same or similar things in your toolkit.</p>
<p>I see that a lot with people using productivity applications and so-called <em>tools for thought</em>. They’ll use two or three note-taking applications, a pair of <a href="weekly-musings-108.html">outliners</a>, a text editor with multiple plugins and add-ons, and maybe even <a href="weekly-musings-117.html">a wiki</a> of some sort.</p>
<p>In that scenario, it’s easy enough to forget what you put and where you put it. You spend a lot of time trying to track things down. As mentioned a moment or three ago, it can be difficult to get all those tools to interact or interoperate with each other.</p>
<p>A better solution might be to have three or four focused applications that cover your major needs. For example, I use a dedicated Markdown editor to do most of my writing. I also use a text editor to … well, edit text that isn’t formatted with Markdown. On top of that, I work with a dedicated note taking application and an outliner (the former via apps on my desktop and phone, and the latter on the web).</p>
<p>Even that might be a bit much for what I need to do. There is a bit of overlap with all those applications, and the digital minimalist in me is yelling at me to do something about it. Like what? I could, for example, just use a text editor to write and to edit whatever text files I need to edit. I could also use that editor as my note taking tool and share it across my laptops using Nextcloud, and with my phone using the <a href="https://plaintextproject.online/articles/2022/02/15/notes.html#going-mobile">Nextcloud Notes app</a>. That’s something I’m thinking a bit about now, as I wait for a new laptop to arrive and while I’m trying to decide what I’ll install on that computer.</p>
<p>Rarely, if ever, is there one tool to rule them all. Or one tool to help you do everything you need to do. That said, your choice of tools is just that. <em>Your choice</em>.</p>
<p>Using one tool for majority of what need to do might work for you. But if you feel constrained or frustrated when working with that one tool, it might be time to look for something (or maybe a couple of <em>somethings</em>) else.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
