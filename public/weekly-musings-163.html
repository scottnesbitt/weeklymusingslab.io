<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 163</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 163</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Recently, I’ve received a complaint or three about my recent shift away from musings focusing on technology. While it’s still within range of my scanners, technology isn’t what’s gripping my attention most strongly right now. Instead, simpler, more human and seemingly mundane matters are. Matters that I find fascinating and, at moment, more relatable than technology.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-cooking">On Cooking</h2>
<p>It’s hard to believe that nine years have flowed by since Soylent (the food substitute, not the food rations derived from people made famous in <a href="http://en.wikipedia.org/wiki/Soylent_Green">that movie</a>) came on the scene. First developed by a coder named Rob Rhinehart, Soylent came about because Rheinhart <em>found himself exhausted by his constant need to prepare and consume food the traditional way.</em></p>
<p>The original Soylent was a powder that you mixed with water (or some other liquid). That mixing created what can best be described as sludge in a glass. It looked unappealing, to say the least, but quickly gained a following.</p>
<p>Back in 2013 (well, nowadays, too), I couldn’t understand why someone would want to fork out quite a bit of cash to mix and consume something that has the look and consistency of industrial waste. That would be money better spent at a local market, for actual food that you can actually enjoy. But I guess, for some, convenience trumps enjoyment. Or the taste of food.</p>
<p>In Soylent’s early days, I came across an incisive comment about Soylent and food substitutes in general (sorry, I don’t have the link): <em>are people these days so lacking in time and in basic skills that they can’t cook a decent meal for themselves?</em></p>
<p>It’s a sad commentary on our times, and about productivity obsessed people who can’t or won’t cook. Who prefer to fill up on chemical concoction. Why? Cooking is a simple act, but one that can enrich your life. One that can bring you joy. One that offers a useful skill.</p>
<p>Don’t get me wrong: this musing isn’t a paen to those popular cooking and cooking competition shows that seem to fill the airwaves. It has nothing to do with being a foodie. It’s more about cooking as a way to take better care of yourself. It’s more about leisure (which I’ll write about in a future edition of the letter). It’s more about being social.</p>
<p>Cooking isn’t difficult, at least not as difficult as I’ve have heard more than a couple of people make it out to be. I put the blame for some of that on those cooking shows I mentioned a paragraph ago. The impression that you get from many of those shows is that to cook well, a meal needs to be complex. It needs to be a complex mix of ingredients. It needs to be a set of flavours, balanced on a razor’s edge. It needs to an art rather than a craft.</p>
<p>Cooking <em>can</em> become difficult and time consuming if you decide to create big, complex meal. But there’s nothing wrong with simple. There’s nothing wrong with sticking to the basics. There’s no reason you can’t, in 30 to 45 minutes, whip up a nice meal using the number of ingredients that you can count on one hand (with a finger or two to spare).</p>
<p>Cooking for yourself isn’t difficult. It’s not exhausting. You don’t need pile of utensils or expensive equipment. You don’t need knife and plating skills that would make a Michelin-starred chef envious. You don’t need to spend two hours shopping and another two doing prep work.</p>
<p>Preparing a good meal involves simple ingredients. Nothing special or taxing. Nothing overly expensive or rare. Take the recipe for a chickpea-spinach-tomato stew that a friend shared with me, for example. Those three ingredients, plus an onion and a couple of spices, make a delicious, healthy, and filling meal. And you can pull it all together in less than an hour. Which I do once a week.</p>
<p>To get started, all you need is a few recipes — which you can find online or at a local supermarket or a library. Once you get used to preparing them, you can think about changing those recipes to suit own tastes or to mix and match them with other recipes.</p>
<p>And, no, simple meals <em>aren’t</em> boring. In fact, some of the best meals I’ve eaten have only contained three ingredients. No exotic toppings. No foods whose names I couldn’t pronounce. Nothing molded into fancy shapes. No dry ice or wafting liquid nitrogen. Just food picked up from the local supermarket and carefully prepared.</p>
<p>Of course, there’s the time argument. As in <em>I don’t have time to cook</em>. Which was one of the reasons that the sludge known as Soylent was created. Experience is speaking here: it doesn’t take long to cook good meal. Under an hour in many cases.</p>
<p>If you can’t find time to cook, what does that say about your life? What does that say about you and your priorities? Are you really so busy that can’t take a bit of time to step away from what you’re doing? Or have you jumped on productivity assembly line, or been sucked into whirlpool of what I call <em>idle hands myth</em> (as in <em>the Devil makes work for idle hands</em>)? Yes, folks, there <em>is</em> a difference between being productive and being busy. But I believe that your life is out of balance if you can’t spare hour to cook and eat a decent meal. Even a few time a week.</p>
<p>By no means am I a great cook. I wouldn’t even rate myself a food one. But I enjoy preparing food. It puts me into another state. It allows my mind to drift away from work and the stresses of life, if only for a little while. For me, cooking is <em>leisure</em> in its truest sense: it’s the time and space I need to step back from everything except the current moment.</p>
<p>As architect Lisa Heschong wrote in <em>Thermal Delight in Architecture</em>:</p>
<blockquote>
<p>[E]ating is a basic physiological necessity, [though] no one would overlook the fact that it plays a profound role in the cultural life of a people.</p>
</blockquote>
<p>There’s definitely a social element to cooking. In the preparation, in the division of labour — my wife chops the onions, because when I do my eyes look like they’ve been sprayed with tear gas. Even if one person is cooking, having others around to chat with (or to jokingly critique what you’re doing) makes cooking all that more fun.</p>
<p>It’s not just when you’re preparing a meal with family or friends, either. The social element also comes with consuming what you’ve crafted. Remember earlier in this musing, when I mentioned that some of the best meals I’ve had were simple? Many of those simple meals were shared with close friends. Perhaps it was the company — people with whom I laughed and argued and just talked — that made those meals memorable. The food, delicious as it was, might have just been a bridge.</p>
<p>I won’t say that cooking encapsulates everything good about life. Why? Because I don’t actually believe that. Being able to cook, though, and enjoying not just results but the <em>process</em> of cooking can be relaxing. It can be rewarding. It can be fulfilling.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
