<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 190</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 190</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time ’round, a slightly longer edition of the letter. An edition with some thoughts about technology and its sustainability. At least, what I believe that sustainability entails.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-the-10-year-device">On the 10-Year Device</h2>
<p>Towards the end of 2021, the laptop that had been my workhorse for seven years or so was showing the signs of a steady, inexorable decline. It was time to put that computer out to pasture and to <a href="https://opensourcemusings.com/getting-to-know-the-starlite.html">buy a new one</a>. Which I duly did just after Christmas that year.</p>
<p>In the interval between clicking the button to submit payment and receiving the emailed confirmation of the order, I was stung by a pang of guilt. A pang of guilt that kept coming back while I waited for that laptop to arrive.</p>
<p>The thought that kept entering my head, in a few forms, was <em>Maybe I should have bought an older laptop and refurbished it</em>. While I have 10 thumbs when it comes to most tasks of the technical sort, I am (just) competent enough to swap out a computer’s memory and hard drive with new ones. And I have more than a little experience (if you want to call it that) installing Linux on older computers.</p>
<p>Then there was the fate of older hardware. My previous laptop found a new home, but for how long? Would it be recycled? Would it be sent overseas to be broken up and, in the process, contribute to the damage to the health of the people breaking it up and to the environment of their land? Or would the bits and pieces of that laptop wind up in landfill? Those questions still haunt me.</p>
<p>On top of that, the manufacture of new devices takes a toll on the environment. As journalist and sustainability advocate <a href="https://solar.lowtechmagazine.com/2020/12/how-and-why-i-stopped-buying-new-laptops.html">Kris de Decker points out</a>:</p>
<blockquote>
<p>[T]he production of laptops requires a yearly energy consumption of 480 to 868 petajoules, which corresponds to between one quarter and almost half of all solar PV energy produced worldwide in 2018 (2,023 petajoules). The making of a laptop also involves a high material consumption, which includes a wide variety of minerals that may be considered scarce due to different types of constraints: economic, social, geochemical, and geopolitical.</p>
</blockquote>
<p>Once again, despite my best intentions, I found myself being part of the problem rather than the solution. And the guilt I felt after buying my new laptop got me thinking a bit more about the concept of the <em>10-year device</em>.</p>
<p>While that concept has been knocking around in my head for quite some times, I first encountered a variation of it being articulated (and well) in <a href="https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-and-the-10-year-smartphone-3799">a blog post</a> published by the UBports project, which develops an alternative mobile operating system called Ubuntu Touch. The concept, as it says on the tin, revolves around phones, tablets, laptops, desktops, and the like lasting and remaining useful for 10 years. Or longer. The idea of the 10-year device isn’t (only) about resurrecting older hardware, but is also about baking longevity into the design and manufacture of newer devices.</p>
<p>For me, the 10-year device symbolizes the idea that you should only buy new hardware if you have a compelling reason to do so. You should should upgrade when you want to or have no choice but to, rather than being compelled by a manufacturer to replace something — be it a computer or a mobile device — because it doesn’t run the latest version of an operating system or the latest apps. Or because it isn’t <em>new</em>.</p>
<p>The concept of the 10-year device is wrapped up in being able to <em>easily</em> swap out components that have reached the end of their lives. Not just low-hanging fruit like memory or storage, but <em>everything</em>: screens, processors, power supplies, and more. All without the need for specialized tools and without requiring the steady, deft touch of a surgeon or a watchmaker. The concept of the 10-year device points to there being a way to <em>easily</em> install an alternative operating system — like LineageOS or Ubuntu Touch on phones, or Linux on a computer — without going through a dozen plus steps and jumping through various tricky technical hoops.</p>
<p>And it’s usually the software and the operating system that slows a device down, influencing us to replace it. As writer and open source advocate <a href="https://opensource.com/article/22/4/how-linux-saves-earth">David Both notes</a>:</p>
<blockquote>
<p>Computers don’t slow down because they are old. Computers with Windows installed produce less legitimate work as they grow older because of the massive amount of malware, spyware, adware, and scareware they accumulate over time. Computer users have come to believe that this is normal, and they resign themselves to life with all of this junk dragging down the performance of their computers.</p>
</blockquote>
<p>With a 10-year device, older hardware should be sufficient if the operating system and software isn’t putting an undue amount of strain on that hardware. Remember the friend I mentioned in <a href="weekly-musings-187.html">Musing 187</a>? The one with the 2012-vintage MacBook Air? That computer takes a couple of minutes to start up since the version of macOS it’s running really isn’t meant for a device that old. Once logged in, my friend can do whatever she needs to do with little trouble. But what if the version of macOS <em>was</em> meant for a 10+ year old laptop? My friend could have the 10-year device without, as some people whine, <em>losing anything</em> or <em>giving something up</em>.</p>
<p>There are a few smaller firms that make it easier to keep their wares going for longer. For example, laptop maker <a href="https://frame.work">Framework</a>, ethical phone manufacturer <a href="https://www.fairphone.com/en/">Fairphone</a>, and <a href="https://starlabs.systems">Star Labs</a>. All sell replacement components and share instructions about how to replace old parts with newer ones. The only drawback is that these are smaller companies, ones which can’t take advantage of <a href="https://en.wikipedia.org/wiki/Economies_of_scale">economies of scale</a>. Which means their products are more expensive than those of the tech giants. Then again, if you amortize the cost over a decade or longer, the cost is justified in my mind.</p>
<p>A couple or three people I know are enthusiastic users of devices running chromeOS. When I mentioned the idea of the 10-year device, and the outline for this musing, to them, they pointed out that Chromebooks and the like come close to being that fabled 10-year device. That’s true, to a point. A device running chromeOS does receive updates for seven or eight years from its release date, I believe. But aside from the Chromebook put out by Framework (yes, them again!), you can’t upgrade the hardware or replace components in many devices running chromeOS.</p>
<p>As you can expect, there are more than a few obstacles to the 10-year device becoming a reality. The first of those is manufacturers themselves. Not companies like Framework, Fairphone, or Star Labs, but the major players in the market. The companies whose wares most folks buy. Those companies aren’t in the business of supporting their older models indefinitely. To compete, to hold the attention of fickle consumers and reviewers and investors, those firms need to regularly release devices that are <em>new</em>, that are <em>fresh</em>, that are <em>innovative</em>. There’s no room for last year’s models.</p>
<p>The economics don’t support keeping older hardware alive. At least, not as things stand. Perhaps that points to the need for new economic and business models that support <em>true</em> sustainability, that encourage longevity. What might those models be? I’ll leave it to people with actual expertise in business and economics to work out the details and implement those models if they see fit to do so.</p>
<p>Another obstacle is the power of marketing. A fearsome, relentless machine that creates and fuels a <em>covet-buy-upgrade-repeat</em> cycle among consumers. Consumers who are relentlessly targeted with imagery and messaging stating that what they have (no matter how useful it still might be) is inadequate. That unless they buy the newest whatsit they’ll be <em>left behind</em>. That they’ll be missing out if they don’t own the latest and greatest. Not all of us fall into the <em>covet-buy-upgrade-repeat cycle</em>. But enough people do, and that helps the cycle continue.</p>
<p>And, I won’t lie: there can definitely be problems with the 10-year device. One of those problems is easy access to spare components. This is especially true for batteries. Many people don’t know where to look, or to scrounge, for spares. They don’t know what to look for, what will work, and for how long those spares will work. It’s not worth their time and effort to search.</p>
<p>What happens to the old components that you replace? And this includes what you pull out of a FairPhone or a Framework laptops. Do those bits and pieces get recycled or do they become yet more electronic pollution? I still don’t know …</p>
<p>Going back to our lords and masters, the manufacturers: some make it <em>incredibly</em> difficult (of not impossible) to swap out components — for example, by soldering RAM or an SSD to a computer’s motherboard or not allowing you to replace a battery in a smartphone. It’s built-in obsolescence taken to an unacceptable, unsustainable extreme.</p>
<p>If those obstacles and problems vanish, or are minimized, I believe the 10-year device could become a reality. Not, as I pointed out earlier in this musing, just when resuscitating older hardware but with the new devices that manufacturers put out. Maybe by supporting the not-so-new, large manufacturers of the devices upon which we rely could create a new economic model. One that’s more sustainable. One that inches closer to being truly green. One which recognizes that most of us can get by quite comfortably using what we have for longer, rather than blithely tossing it aside for when a new device shows its face.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
