<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 126</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 126</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Sometimes, things just hit you. All at once. That was me this last week. Three ideas grabbed my attention in an iron grip, and wouldn’t let go. Being a masochist, I decided I could work on all of them. Was I wrong. I made quite a bit of headway on each idea, but I couldn’t finish any of them. Maybe that was the universe trying to tell me something …</p>
<p>So, I scrambled to come up with something else. It’s based on an idea I had a few years ago, but kind of dovetails with those three other ideas I mentioned a moment ago.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-ad-blocking">On Ad Blocking</h2>
<p>Imagine walking down the street and suddenly having an advertising flyer or placard shoved in front of your face. Imagine that flyer or placard staying with you as you wend your way through pedestrian traffic. Imagine that flyer or placard only moving out of your line of sight when you’ve swatted it away or bought what it’s advertising.</p>
<p>That’s how I felt when using my wife’s laptop recently. If you’re wondering, I was using it because I needed to check something and her 2012-vintage MacBook Air just happened to be turned on. So, I fired up my wife’s web browser of choice. Which happened to be Google Chrome, which I haven’t used in years. And did I get a shock.</p>
<p>On my laptops, I use Firefox along with the <a href="https://ublockorigin.com/">uBlock Origin</a> and <a href="https://privacybadger.org/">Privacy Badger</a> extensions. Those extensions help automatically shoo away ads and anything along those lines. Although my wife did install the Chrome edition of the uBlock extension, I had to click a toolbar button to get the extension to do its thing on every site I visited. Every. Single. Site.</p>
<p>I can only imagine what it’s like for people who don’t use ad blockers …</p>
<p>Ah, online ads. They’re annoying. They’re intrusive. They’re distracting. And they make the experience of visiting certain sites a lot less pleasant. Worse, we run into those online ads several times a day. Several times a site.</p>
<p>The proliferation of ads is shocking. It seems to have gotten worse in the last few years. Firefox and the extensions I mentioned have pretty much insulated me from most ads online. But, as I discovered with my adventure using my wife’s laptop, ads can fill anywhere up to half a page. At the top of a page, in the body of what you’re reading, or in a column (which can be one-third of the page or wider).</p>
<p>With more and more people turning to ad blocking software to swat away annoying ads, many online publishers are taking a hit in the wallet. A few years ago, one German online publisher claimed that it lost €9 million (about one-fifth of its revenue) in a single year. I’m sure there were others moaning about the same thing.</p>
<p>Of course, to combat ad blocking software publishers and advertisers are coming up with clever and downright underhanded ways to get ads before our eyes. They use scripts to pop up an ad when you’ve scrolled part way down a page. They drop overlays over a page at timed intervals. Ads that are embedded on page. Worse, some sites don’t let you view what’s on them unless you either whitelist those sites or disable your ad blocker. No thanks, folks.</p>
<p>Worse, advertisers spend a lot of time and more than a few dollars (or whatever their currency of choice is) to try to circumvent ad blocking software. It’s a constant cat and mouse game between two sets of developers, a steadily escalating arms race between the two camps. And the real losers are you and me.</p>
<p>A web chock full of ads wasn’t the web that Tim Berners-Lee envisioned over 30 years ago. It wasn’t the web that I expected or wished for. I’m sure the same goes for you too.</p>
<p>Ad blocking software has become an integral app in the web browsing toolkits of millions. Myself included. More out of necessity than choice. I make no bones about blocking ads on most sites. Ads throw a wrench into my reading. Ads that are garish. Ads that are jolting and distracting. Ads slow pages down. Ads that are rarely relevant to my interests and needs. No, I don’t want to burn fat, meet that special someone, or make $821.61 each day working from home.</p>
<p>Notice I wrote <em>most sites</em> in the previous paragraph ago. I turn blocking off on a small handful of sites that 1) have unobtrusive ads, and 2) remind me politely that the revenue from those ads keeps the lights on. Sites like that, in a small way, show me respect. They give me a choice. They let me <em>opt in</em>, rather than forcing me to opt out by creating a whitelist or by lowering my shields. Sites like that are usually small and struggling. If what they’re publishing interests me, if I keep coming back to them, then I believe they deserve a chance to survive and thrive. I hope I can help them do that.</p>
<p>Sadly, sites like that are few and far between. Most other sites send wave after wave of ads at us. Which, in turn, forces us to brandish ad blocking software to turn back that tide. Ad revenues for those sites decline, and those sites and media companies whine and stamp their feet about it. It’s the cliché vicious circle in action.</p>
<h3 id="going-beyond-ads">Going Beyond Ads</h3>
<p>The battle, though, is going far deeper than mere advertisements.</p>
<p>In <a href="http://www.mondaynote.com/2015/08/03/what-the-ad-blocker-debate-reveals/">a 2015 essay</a> at <em>Monday Note</em> (an essay that’s still relevant six years into the future), Jean-Louis Gassée pointed out that:</p>
<blockquote>
<p>Web publishers insert gratuitous chunks of code that let advertisers vend their wares and track our every move, code that causes pages to stutter, juggle, and reload for no discernible reason. Even after the page has settled into seeming quiescence, it may keep loading unseen content in the background for minutes on end.</p>
</blockquote>
<p>In that essay, Gassée brings up two key points that are the crux of the ad- and content-blocking wars: <strong>trust</strong> and <strong>speed</strong>.</p>
<p>I can’t think of many, if any, people who want web publishers monitoring what they’re doing. I can’t think of anyone who doesn’t want web pages to load quickly, especially on mobile devices. All the cruft publishers throw at us slows down pages, whether on the desktop or on mobile devices, with no benefit to us.</p>
<p>Ads and those <em>chunks of code</em> Gassée mentioned don’t enhance our experiences on a website. They don’t improve what those site publish. They don’t make what we’re seeing more convenient or easier to read. They don’t make the web a better place. Those ads, those trackers slow us down. They tax our patience. They increase our frustration with the very technology we’re trying to use to make our lives a bit easier.</p>
<p>When we learn that sites are tracking us and deliberately slowing us down we either turn to blocking software or abandon those sites. The publishers themselves are to blame for that. They put making money by any means necessary before their readers. Then they cry <em>foul</em> when their efforts are blocked or when readers dump them.</p>
<p>Don’t get me wrong. I have nothing against a company making money. What troubles me is <em>the ways</em> in which many firms choose to make that money. Ways that are, to put it bluntly, incredibly underhanded and more than a little sleazy.</p>
<h3 id="is-there-a-solution">Is There a Solution?</h3>
<p>I don’t think there is a definitive one. Entrepreneur and journalist Frederic Filloux offered a few fixes:</p>
<ul>
<li>Sites can try to use underhanded tricks to beat ad blockers.</li>
<li>People can register with a valid email address to selectively get ads, but sites won’t force feed ads to visitors.</li>
<li>Visitors can subscribe to the site to eliminate the ads. Some amount of money or the other will probably need to change hands to make up for the lost ad revenue, though.</li>
</ul>
<p>Those solutions might work for large media companies. The rest of us will continue to turn to ad blocking software and, in a way, help escalate a perverse kind of arms race on the web. As ad blocking software gets better, publishers will try to find ways around it. The battle will never end.</p>
<p>The ball is in the court of the web publishers. It always has been. They can speed up their sites by removing all the cruft they throw at us. That would be a step in the right direction.</p>
<p>Rebuilding the trust they shattered with their shenanigans, on the other hand, will take a long time. If that’s even at all possible.</p>
<p>Maybe there is a solution. Maybe that solution is as simple as all sites treating visitors with a little more respect. Maybe it’s time to find new sources of revenue online. Maybe it’s as simple as not constantly forcing ads at our eyeballs.</p>
<p>That would make for a pleasant change.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
