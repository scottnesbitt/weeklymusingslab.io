<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 034</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 034</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>One of my projects outside of this letter is an ebook that I’m working on in fits and starts. I don’t have a title for the book yet, but its working name is Project Crimson and it will be a collection of slightly (sometimes more than slightly) contrarian essays on technology and our relationship with it<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a>.</p>
<p>This week, I’m sharing an essay-in-progress from that book. The essay is still a bit rough. It’s still a bit unfinished. But I hope you enjoy it.</p>
<h2 id="on-what-you-dont-own">On What You Don’t Own</h2>
<p>In the not-so-distant past that was April, 2017 someone going by the name R Martin ran into a spot of bother. Martin had bought an internet-connected garage door opener called the Garadget from Amazon.com. As much of the tech we use these days is wont to do, the smartphone app controlling the device suddenly decided to stop working. On a Saturday evening.</p>
<p>So, like many people in this age of entitlement and easy outrage, Martin left a nasty complaint on the Garadget message board.</p>
<p>Unfortunately, SoftComplex, the company behind Garadget, didn’t jump when Martin told them to. It was a Saturday evening after all … After undergoing the indignity of being ignored for a few minutes, Martin let his outrage be his guide. He fired back with a nasty one-star rating on Amazon.</p>
<p>What happened next wasn’t the reaction Martin had hoped for. Instead of rushing to Martin’s aid and begging his forgiveness, SoftComplex vindictively bricked Martin’s Garadget unit. The company’s head Denis Grisak told Martin that his “unit will be denied server connection,” rendering it useless.</p>
<p>Martin was being an overly-entitled, impatient, whiny jerk — the type of person the internet seems to be overpopulated with. SoftComplex’s reaction, though, was pretty jerky in its own right. Is the company’s leadership that thin skinned? Couldn’t SoftComplex have taken a higher road? We’ll never know.</p>
<p>The incident between R Martin and SoftComplex teaches us a powerful lesson. No company, no matter how big or small, should have the kind of power, that kind of control over something you paid for. Something that you own. At least, that you <em>think</em> you own.</p>
<p>More and more companies have that kind of sway over their wares, though. We’ve placed too much of the power in the hands of manufacturers and we’re at their mercy. That’s especially true with so-called Internet of Things (IoT) devices.</p>
<p>That’s definitely not a good thing. We shouldn’t trade convenience for freedom. Yet more and more people are blindly, happily giving away their freedom in exchange for that dubious convenience.</p>
<p>We don’t own things any long. At best, we’re leasing what should be ours.</p>
<p>As <a href="https://www.eff.org/deeplinks/2016/04/nest-reminds-customers-ownership-isnt-what-it-used-be">the Electronic Frontier Foundation pointed out</a>:</p>
<blockquote>
<p>It used to be that when you bought an appliance, you owned it, and you could take it apart, repair it, and plug in whatever accessories you wanted without the manufacturer’s knowledge or permission.</p>
</blockquote>
<p>Admittedly, the appeal of IoT is easy to see: you get a small, compact device that takes care of trivial, boring tasks for you. Most IoT devices are simple. They offload the grunt work on to a server somewhere in that nebulous, amorphous blob of ones and zeros called <strong>the cloud</strong>. All of the heavy lifting is done elsewhere, and your device is more or less a receptacle for the results of all that heavy lifting.</p>
<p>And that’s where a large part of the problem lies. You don’t control the devices you buy. You use them, but at the pleasure and discretion of the firms that make them. You’re at the mercy of not only internet connections and the reliability of someone else’s computers. As R Martin found out, you can say the wrong thing and in the wrong tone, and the company will cut you off. No questions asked. No appeal. No apologies.</p>
<p>Anything can set those companies off, too. You could write a bad review or an indignant forum post. Your payment for monthly service might not go through because your credit card expired. You might offend the religious or political beliefs of someone at an IoT company. You might refuse to move to a more expensive tier or upgrade your device when the company tells you to. You could spell the company’s name incorrectly in an email. No matter how big or small the transgression or slight, it’s enough for someone to press the <strong>Off</strong> button.</p>
<p>It’s not just the threat of a firm cutting you off when you make what’s in their eyes a mis-step. A firm could suddenly decide to axe a particular device or line of devices. A while back, a company called Nest, owned by Google’s parent Alphabet, announced that it was killing its Revolv Smart Home devices. The heart of the Revolv devices, which acted as a central location that let you control all of your connected gadgets, was a service that resided on Nest’s servers. Nest told the world that the service would go dead and when. Imagine all of the people who foolishly relied on Smart Home devices and how they had to scramble to find an alternative or to decouple from Revolv’s service.</p>
<p>You’re dead in the water if the services backing your smart devices go down for any length of time, too. When the services go down, your devices can’t phone home. Remember that while the devices sit in your home, most or all of their functions reside elsewhere. They reside out of your reach, out of your control. You wind up with a futuristic wall ornament or paperweight when those services go bye-bye even for a short period.</p>
<p>That should never happen. It doesn’t <em>need</em> to happen. Before consigning your devices to the digital glue factory, their creators should give you some options. For example, in early 2016 streaming music service Rdio closed its doors. One company that went down with Rdio was Aether, the maker of a device called the Aether Cone. The Aether Cone was a so-called <em>smart speaker</em> that worked with, <strong>and only with</strong>, Rdio. As Aether <a href="https://github.com/AetherThings/AetherCone/wiki">notes</a>:</p>
<blockquote>
<p>Aether’s servers have shut down, eliminating most functionality of the Cone, including voice commands, Rdio streaming music and song recommendation.</p>
</blockquote>
<p>In one final act, Aether updated the firmware (the software that controls the device) to sever its link to Aether’s services. That update let owners of the Aether Cone to continue to use it as a generic wireless speaker, or with the popular Spotify music streaming service.</p>
<p>Pushing out that firmware update was a small gesture, and a gesture Aether could have gotten away without making. But Aether did right by its customers. It was a gesture, I’m sure, that some of the owners of the Aether Cone appreciated.</p>
<p>I don’t think it’s too much of a stretch of the imagination to say that what Aether did was a fairly uncommon occurrence. You can’t trust all IoT device makers to do the right thing and update the firmware of those devices so you can still use their basic functions if the company evaporates.</p>
<p>The situations I’ve been describing go beyond a faceless corporation callously pulling the rug out from under you. It goes to the deeper, more fundamental question of <em>ownership</em>. Call me old fashioned, but when money changes hands and you walk out of a store (even a virtual store) with goods, <strong>those goods belong to you</strong>.</p>
<p>You should be able do with them what you like. That could mean opening them up, messing around with the innards, replacing the software, adding components. You should be able to repair them without taking or sending the devices to a limited number of repair depots. And you shouldn’t need <a href="https://en.wikipedia.org/wiki/Pentalobe_screw">a specialized tool</a> to do that.</p>
<p>When the plug is pulled, when the service backing what should be <strong>your property</strong> goes silent, you wind up with something that’s less than what you bought. It’s often difficult, if not impossible, to replace the software guts with something you can control. It’s often hard, if not impossible, to do the same with physical components.</p>
<p>Connected devices and the Internet of Things aren’t really worth it in the end. You wind up relying on services and infrastructure and code that 1) is out of your hands, and 2) that is in the hands of firms which can be fickle.</p>
<p>Not everyone see this that way I do. I don’t expect them too, either. If you feel need to embrace the Internet of Things, try to find devices that are based on and which use open standards and open source firmware. At the very least, find devices that aren’t locked down. Devices that enable you to change the firmware (or anything else) if you so desire.</p>
<p>Another option is to set up a <a href="https://en.wikipedia.org/wiki/Platform_cooperative">platform co-operative</a>. The idea of a platform co-operative is to put the ownership of a company or service in the hands of the members who invest in that company or service. That way, there aren’t any nasty surprises. The members of the co-operative decide what happens with a service and a device. And if the co-operative bases its services on open source software and open hardware, members will have more choice and will be able to better weather the services disappearing if the co-operative doesn’t work out.</p>
<p>There aren’t any IoT platform co-operatives out there now, but there definitely is an opportunity there. It’ll take time and money to build them up, but I think that could be the way forward with IoT. And more.</p>
<p>While IoT might seem like the current wave of the future, there are just too many problems with it. Call me a Luddite, but as I mentioned a few paragraphs ago, I advocate sticking with dumb devices and dumb fixtures and dumb appliances. They might not have the same cachet or geek factor as their smart counterparts, but their behaviour is predictable. You don’t need to spend outrageous sums on replacements. If you have some basic home repair skills, you can make the change yourself quickly and relatively cheaply.</p>
<p>More to the point, you know that those devices are under <strong>your</strong> control and actually belong to you.</p>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p>Project Crimson, sadly, collapsed under the combined weight of my hefty expectations and my doubts in my ability as a writer.<a href="#fnref1" class="footnote-back">↩</a></p></li>
</ol>
</section>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
