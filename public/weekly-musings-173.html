<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 173</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 173</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Don’t you just love it when something you’re reading lights the fuse of an idea in your mind? That happened to me recently, when my eyes focused on a section of book called <em>Essayism</em> by novelist Brian Dillon. It’s a short volume, but one densely packed with ideas and opinions about the titular form of writing, including including some passages that helped expand upon what you’re about to read.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-lists">On Lists</h2>
<p>Sometime in 1978, a thick trade paperback appeared on a coffee table at home. I think one of my parents picked it up at a chain bookstore — might have been at a Coles or a WHSmith at a local mall. The book was titled <em>The Book of Lists</em>. And I quickly became fascinated with it.</p>
<p>Looking back, I’m not entirely sure <em>why</em> the book fascinated me. I mean, was just a book filled with various lists of varying lengths. Lists about movies, crimes, sports, and many, many other subjects. Including a few subjects that an 11 year old probably shouldn’t have known about …</p>
<p>Maybe what fascinated me was how the authors came up with topics of those lists, the sheer number of those lists, and how they gathered all the information to craft those lists. Remember that book came out in days when internet wasn’t on computers, when the World Wide Web wasn’t even a glimmer of a thought in Tim Berners-Lee’s mind. The authors couldn’t turn to one or more search engines to find information or help them on their way.</p>
<p>Nope. The authors, and their staff, needed to do research and legwork and interviews the old fashioned way — using various books and archives, doing interviews, and the like. Whether I realized at time or not, <em>The Book of Lists</em> spurred me to start thinking about lists in different ways.</p>
<p>Lists are part of all of our lives. Fairly sizable parts, too. Not just to-do lists (which I’ll look at in a moment), but other seemingly innocuous lists. Like the shopping list.</p>
<p>The idea behind a shopping list is to keep us focused, to ensure we stay within the budget, to ensure we buy only the food that we need. A shopping list is also a chronicle of our diets and, in a small way, our states of mind. Changes to our shopping lists could show a renewed interest in taking better care of ourselves. Or a sudden and continual inclusion of more processed and junk foods could indicate some underlying problems we’re experiencing in our personal or professional lives.</p>
<p>In novel <em>Canticle for Lebowitz</em>, one of relics found in fallout shelter centuries after nuclear war was the title character’s list of items to pick up on his way home. It became an endless source of fascination for monks studying those relics. And that’s not surprising. Personal lists, like shopping lists, are a fuel for historians, archaeologists, and anthropologists. Not just because they’re often among most common artifacts to survive the centuries but because they’re a window into the lives of ordinary people at a particular moment in time. And, by extension, a chronicle the day-to-day cultural lives of a society.</p>
<p>Lists also become, in no small way, repositories for our goals, our aspirations, our dreams. You’re probably familiar with the idea of the bucket list — a list of all of the things that you want to do or experience before you die or before you reach a certain age. Outside of bucket lists, a personal list can be an enumeration of the places that you want to visit when you travel somewhere or what you want to learn in the next year or two years or longer.</p>
<p>Lists are also powerful tools in writing — whether that’s non fiction or literature. If structured properly, lists can have a certain rhythm to them. The literary term for that is <em>parataxis</em> — one thing in a sequence, followed by another, followed by another, until the end. No wonder lists are so widely used in technical communication …</p>
<p>The beauty of a list is that it can appear anywhere in writing, no matter what form that writing takes. As Brian Dillon notes in <em>Essayism</em>:</p>
<blockquote>
<p>When the list appears suddenly in a novel, it feels as if a verbal <a href="https://en.wikipedia.org/wiki/Midden">midden</a> has been dumped on the page.</p>
</blockquote>
<p>Those lists often not just break up the text, but follow a slightly different cadence than the collections of words around them. They jolt us out of a narrative flow. The lists enumerate, but also elaborate. They describe, define, elucidate.</p>
<p>All that’s well and good, but lists can also have dark side. While lists can help give us structure and focus, they can also constrain or restrict us. By becoming beholden to lists, we become so focused on one task or opportunity that we might not be able to see another more interesting and promising one. You might, for example, make a list of all the places that you want to see while abroad. Which is great, but that list and the fact that you’ll stick to it too closely narrows the possibility of discovering something out of way.</p>
<p>It’s easier to make a list than to do some actual work. Making a list, especially an effective one, takes time and effort. It’s an early step on the path to completing a project. There are those who believe that once they have a list, they’ll have a map of everything that they’ll need to do. It doesn’t work that way. Let’s go back to Brian Dillon, who notes that a list is:</p>
<blockquote>
<p>really a complex undertaking. You are bound to forget something, and you will be tempted to give up or let the thing tail off.</p>
</blockquote>
<p>Lists can bring focus, but they can also promote a <em>contingency mindset</em>. I know, for example, more than a few people who spend an inordinate amount of time crafting lengthy lists of what they need (or <em>think</em> they need) to take when they travel. With those lists, they try to anticipate and address every possible situation they could encounter. They quickly get frustrated while packing all of those items, not realizing that they don’t need half or even two thirds of it all. Yet the lesson doesn’t sink in. They do the same the next time they need to get ready to travel.</p>
<p>Lists can also express anxiety, insecurity, a lack of certainty, a sense of overwhelm. Take the to-do list. Like shopping lists, they’re meant to focus our attention and effort. But far too often, those lists grow to uncontrollable proportions. The to-do list becomes, to a degree, a repository for not only what we <em>need</em> to do but also everything that we <em>want</em> to do. Instead of helping us, to-do lists can morph into a hindrance. They take the shape of an insurmountable barrier. To-do lists become a source of stress, of anxiety, and of fear. We become too paralyzed to do anything.</p>
<hr />
<p>A list is more than an enumeration of items. It’s more than something scribbled in a notebook or on a scrap of paper or in some app, to be used as a reminder or a guide. A list can be aspirational. A list can be intentional. A list can be existential. A list can be container of hopes and dreams. It can also provide us with a path for living and for doing.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
