<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 179</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 179</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>One does occasionally get email. A couple or three of those missives are often from complete strangers who’ve read something I’ve published online. Sometimes they offer praise. Sometimes they offer constructive (or not-so-constructive) criticism. Sometimes they jump on me with both feet about an idea or opinion I’ve presented, one which doesn’t mesh with their ideas and opinions. A bit of the latter prompted what you’re about to read.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-doing-nothing">On Doing Nothing</h2>
<p>For the last I don’t know how many years, we seem to have been living in a time in which the cult of productivity is thriving. Driven by what I believe to be a misguided sense of what <em>productivity</em> actually is.</p>
<p>Even though working from home, given rise to by the COVID-19 pandemic, has prompted many of us to reevaluate our relationships with work, there are still more than few folks out there with their shoulders perpetually to wheel. Who embrace the <em>always be grinding</em> ethos. Who fill their waking hours with work and more work, with task after task.</p>
<p>In more than a few cases, they’re trying to find meaning from work, an identity in their work, and fulfillment from that work. People who do all that seem to want to be able to simultaneously brag and whine about how many hours they put in yesterday. Are in pursuit of gaining brownie points for striking yet another item off of their overfull to-do lists.</p>
<p>To be honest, that’s not a great way to work. And making work an integral and central part of your existence is definitely not a great way to live. It’s the road to burnout, to disillusion, to loneliness.</p>
<p>A lot of that’s wrapped up in the (frankly misguided) desire to get more done. Yes, experience is speaking. There was a stretch of several years during which I felt that I needed to get more done. More writing. More work around the house. Learning more. All of that sort of thing. Task upon task, job upon job. A grind that seemed to never end.</p>
<p>As you can imagine, I was unhappy and frustrated with the amount that I <em>wasn’t</em> doing. But as the years passed, I realized that most of what I <em>thought</em> I wanted or needed to do didn’t matter in the grander scheme of my life. A majority of those tasks and projects and goals weren’t important. They weren’t worthwhile. They were just me filling what I assumed to be dead time. But they weren’t fulfilling or enriching me, either personally or professionally.</p>
<p>That’s when I decided to shift gears. To spend time doing nothing. I had the temerity to every so often lie around. To be a bit of a sloth. To do unproductive tasks. Or to do nothing at all.</p>
<p>If you spend any amount of time, no matter how brief, doing nothing then you’re taking the risk of being branded. And not in a good way. You’re branded as being <strong>lazy</strong>. I was, by more than a couple of people and from more than a couple of circles.</p>
<p>I’m constantly surprised at how many people blanch at the mere thought of spending a portion of their personal time doing nothing. I often talk about <em>the idle hands syndrome]</em> (as in <em>The Devil makes work for …</em>), and how far too many people are afflicted with it. Unless they’re spending all or a majority of their waking hours undertaking a constant stream of activities or tasks or projects, they feel like they’re wasting time.</p>
<p>They’re not.</p>
<p>Remember that gear shift I mentioned a few paragraphs ago? These days, I spend more than a couple of Saturdays (and parts of many Sundays) each month doing nothing. Well, nothing of consequence. Sometimes, I’m forced to because I’m physically hobbled by the results of spending a few decades bashing my body around in creative ways. Sometimes, those days are gray and rainy. Sometimes I’m mentally or physically tired. All of which sap my motivation to do things. To learn things. Instead, I sit back. I spend time with my wife and daughter. I read articles that I save to <a href="https://opensource.com/article/18/7/wallabag">wallabag</a>. I do some cleaning. I watch highlights of auto racing or a movie. I listen to music.</p>
<p>No running around doing various errands. No filling my every moment with little jobs. No spreading my energies thinly on tasks that turn out to be unimportant. And guess what?</p>
<p>I never feel like a sloth.</p>
<p>I’m never anxious about getting things done.</p>
<p>I don’t miss out on anything.</p>
<p>In fact, when I do nothing I feel more relaxed than I usually do. My thinking become clearer and I get a boost of mental and physical energy.</p>
<h3 id="being-idle-isnt-a-waste">Being Idle Isn’t a Waste</h3>
<p>Doing task after task, as if you’re on an assembly line, usually is. You don’t need to fill every waking moment with activities or actions.</p>
<p>The human body and brain are wonderful things that are capable of doing a lot. But the body and brain need time to rest, to refresh, to process, and to reflect. All of that gives you more and better focus when you get back to work.</p>
<p>But just as important is the fact that taking time to do nothing lets you appreciate what you have and what you’ve done. Not just appreciate it, but also enjoy it.</p>
<p>Taking time to do nothing lets you enjoy the company of family and friends.</p>
<p>Taking time to do nothing lets you take in the wonders, no matter how small or slight, of the world around you.</p>
<p>And that’s more worthwhile than filling those moments with work or projects or tasks that can wait for another day.</p>
<h3 id="how-to-do-nothing">How to Do Nothing</h3>
<p>For many people, that isn’t easy. At one time, doing nothing would have been tough for me too. But here’s what you can do.</p>
<p>Start off slowly. Take a half hour or a hour a few times a week to do nothing. Ease yourself in. Then, take longer breaks until you’re comfortable with going a whole day without doing much.</p>
<p>Learn to <em>let go</em>. Of stress. Of guilt. Of the misguided desire to do something that you think is productive.</p>
<p>Next, learn to tell yourself that you deserved a break. It takes a little while to actually believe it, though.</p>
<p>Finally, remember that whatever you think you’re missing probably isn’t going to ruin your life, your relationships, or sanity. It’s not going to set your career or ambitions back. It’s not going to vault the competition, real or imagined, far ahead of you.</p>
<p>Doing nothing sounds simple, but it isn’t. It takes time and a bit of practice and some discipline to comfortably do nothing. You’ll have to consciously stop yourself from doing those little jobs or from reaching for your phone or tablet. But the results are well worth the effort.</p>
<p>And, as writer Warren Ellis pointed out:</p>
<blockquote>
<p>Giving in for one night and saying the hell with it, I’ll start again tomorrow, is fine, and you should never worry about doing it. The world won’t end because you say the hell with it and get comfortable for one damn night. And if it does? Well, s**t, were you guarding the single button that was going to save the world? No, you weren’t.</p>
</blockquote>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
