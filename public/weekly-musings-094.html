<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 094</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 094</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>It’s good to have you back. Thanks for joining me.</p>
<p>This time ’round, thoughts about a topic inspired by something I heard at The Day Job<sup>TM</sup> recently. But also thoughts that, as usual, have been bouncing around in my brain for a while.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-simple-software">On Simple Software</h2>
<p>Maybe I’ve been digging in the enterprise software trenches a bit too long. Maybe I’m just getting old and cranky or jaded. Maybe it’s a bit of all of the above. But lately, I’ve been lamenting today’s lack of simple software.</p>
<p>By <em>simple</em>, I mean software that does just what it needs to do. Not software that tries to be everything to everyone and comes up short in all areas. Software that’s focused, but also easy to use.</p>
<p>That’s not to say that <em>all</em> software is overly complex and overloaded with features. Quite a bit of it isn’t, though much of that simple software occupies some very niche spaces.</p>
<p>Despite so much software being just <em>too much</em>, developers and (what’s to me) a shockingly large number of users put up with the status quo. They cling to the idea that more features and functions in software means that software is <em>better</em>. That by pushing a one-size-fits-all approach, we wind up with not something that’s a well-tailored solution but one that fits like a tablecloth. A big, tent-like tablecloth.</p>
<p>There’s an expectation that people who use software will take the time and effort to find what they need among a myriad of menus and shortcuts. That doesn’t take into account the frustration that people feel when they can’t find what they’re looking for, especially in software that they don’t use very often.</p>
<p>Much modern software adds too much <a href="https://www.lambdatest.com/blog/what-is-cognitive-overhead-in-design-and-how-to-reduce-it/">cognitive overhead</a>. Its complexity adds too many layers to tasks that should be simple.</p>
<p>That complexity explains the popularity of, for example, Google’s suite of productivity tools. Especially amongst the average person. Google’s tools are stripped down. They’re basic. They do <em>just enough</em>. And I know a couple or seven people, who work in various fields, who regularly deride Google’s tools for their simplicity. Who themselves are frustrated because those tools don’t have the features that they get in desktop software. Those folks need to remember that they’re not the target audience for tools like Google’s productivity suite. That audience is people who, say, want to dash off a quick letter to their MP or to keep track of their monthly expenses. Tools like Google’s are more than enough to do that.</p>
<p>That brings up another harsh truth: not everyone wants to bend the software that they use to their will. Not everyone wants or needs to learn all the intricacies of an application. Not everyone wants to become a so-called <em>pro</em> or <a href="weekly-musings-063.html">power user</a>. They just want a simple tool to quickly get a job done and to move on.</p>
<p>As you may or may not know, I write for a living. Which means using a word processor for my work. Back in the late 1990s, my favourite word processor wasn’t Microsoft Word. It wasn’t <a href="https://en.wikipedia.org/wiki/WordPerfect">WordPerfect</a>. It wasn’t <a href="https://en.wikipedia.org/wiki/IBM_Lotus_Word_Pro">Ami Pro</a> or even <a href="https://en.wikipedia.org/wiki/StarOffice">StarOffice</a>. It was <a href="http://yeahwrite.com/">Yeah Write</a>. Really!</p>
<p>Why? Yeah Write made writing document shockingly easy. You chose the type of document that you wanted to create — a letter, a memo, a fax, an email — and then you filled in various fields that made up the document. So, if you were writing a letter you could insert your contact’s information from Yeah Write’s address book, select a salutation, type the body of the letter, and choose a closing. It was that simple.</p>
<p>I’m proud to say that I influenced a couple of dozen people to buy and use Yeah Write. That said, the software never put a dent or even a small scratch on the word processor market. It was a bit too niche and didn’t have the cachet of the big guns of that part of the software world.</p>
<p>Sadly, not enough people are clamouring for simple software these days. And if they are, the big development shops aren’t listening to them because … well, they don’t have to. Some of those shops did at one time. They offered <em>lite</em> or <em>express</em> versions of their wares. Software with a limited set of features and functions for the home user. Some of that was given away for free. Some was priced under $50.</p>
<p>In the end, a lot of that software disappeared. Some of it couldn’t compete with the offerings of the bigger players. Some of it <em>was</em> offered by the bigger players, but cannibalized users and sales of their more feature-packed (and more expensive) software. So, it had to (in marketing speak) be sunsetted. Which brought us to where we are today.</p>
<p>I still believe, though, that there’s a place in the world for simple software. Software for everyday use by ordinary people not specialists. One way to do that is to take a different approach to software. And to do it in a way that won’t tear a chunk out of the sales of larger, more complex software.</p>
<p>How? By offering what I call a <em>graded interface</em>. A graded interface follows the principles of a <a href="https://en.wikipedia.org/wiki/Graded_reader">graded reader</a>: it guides you through the software gradually, starting you off with simple functions and, as you need them, progressing to more advanced, complex ones.</p>
<p>Think that’s too difficult to bake into software? I’ve been told that more than once. Guess what? It <em>is</em> possible. The best example I’ve seen of a graded interface came with New Deal Office and Breadbox Ensemble. Those were attempts in the late 1990s and early 2000s to revive a desktop environment called <a href="https://en.wikipedia.org/wiki/GEOS_(16-bit_operating_system)">GEOS</a>. The applications had four levels of complexity that you could move between. You started off with very basic functions. If you needed more, you could go into the settings and move up a level. At level four, you unlocked everything.</p>
<p>As with Yeah Write, I introduced several people I knew to both New Deal Office and Breadbox Ensemble. Those folks were people who had older computers that would grind to a halt trying to run the latest (at the time) version of Windows, but who wanted and needed to get things done. New Deal Office and Breadbox Ensemble gave them that, and as I recall only about half of them switched the interfaces of the applications that they used higher than the second level of complexity.</p>
<p>I have the feeling that we won’t be seeing anything like that again any time soon. I hope I’m wrong, though. I really believe that the world needs more simple software. Software, to paraphrase <a href="https://en.wikipedia.org/wiki/Jack_Tramiel">Jack Tramiel</a>, that’s built for the masses and not the classes.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
