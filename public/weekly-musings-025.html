<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 025</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 025</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>After writing <a href="weekly-musings-024.html">last week’s letter</a>, the idea underlying that essay continued to poke at my brain. The results of that poking (aside from a mild headache) was me digging up an older essay on that subject, which I think complements last week’s musing.</p>
<p>I hope you enjoy it.</p>
<h2 id="on-calm-technology">On Calm Technology</h2>
<p>We’re slaves to our technology. We’re beholden to it. And that technology likes to remind us of that fact.</p>
<p>Our devices beep. They ping. They buzz. They ring. They vibrate, They flash. All of which let us know that we’ve got an incoming call or text message, that someone’s responded to a tweet or posted something to Facebook, that there’s an email or three waiting for our attention.</p>
<p>And we duly jump. Our attention is ripped away from what we’re doing or <em>should</em> be doing. Our attention is ripped away from the people we should be focusing on. Our attention is torn from the moment. That attention moves to a screen attached to the device that spawned the alert.</p>
<p>That shattering of our attention makes us less productive, makes us less focused, makes us less mindful of the <em>now</em>. It erodes our cognitive functions and punches holes in our concentration. We’re like lab animals responding to stimuli to get a quick, easy reward.</p>
<p>Imagine, however, a way to move those notifications and alerts out of your direct view and into the peripheries of your attention. Of changing those notifications from obtrusive popups and shrill alarms to a subtle sound, a vibration, a dim light, some text. Or, a combination of any of the above.</p>
<p>That’s the promise of <a href="https://en.wikipedia.org/wiki/Calm_technology">calm technology</a>. With calm technology, we’ll be able to retain more than just a shard of our shattered attention and to let us keep our focus aimed where it should be aimed.</p>
<p>The idea of calm technology was born at the <a href="https://en.wikipedia.org/wiki/PARC_(company)">Xerox Palo Alto Research Centre</a> (PARC for short). PARC is a research and development centre where, over the last 40 years, many of the technologies we take for granted nowadays originated.</p>
<p>In the 1990s, two researchers at PARC — John Seely Brown and the late Mark Weiser — recognized that technology would soon take a more prominent place in our lives. They also foresaw that technology could be quite intrusive. They expounded upon this idea in an article titled “<a href="https://www.karlstechnology.com/blog/designing-calm-technology/">Designing Calm Technology</a>”, which stated that calm technology is:</p>
<blockquote>
<p>that which informs but doesn’t demand our focus or attention.</p>
</blockquote>
<p>At its core, calm technology is wrapped up in both how we interact with technology and how technology interacts with us. As Amber Case writes in her book <em><a href="http://shop.oreilly.com/product/0636920039747.do">Calm Technology: Principles and Patterns for Non-Intrusive Design</a></em>, if we expand the number of devices we use but:</p>
<blockquote>
<p>maintain our current standards of communication, we’d soon find ourselves — our very world — buried under an indistinguishable pile of dialog boxes, pop-up boxes, push notifications, and alarms.</p>
</blockquote>
<p>We’re at that point now. When it comes to technology, much of our attention is spread across multiple devices like computers, tablets, smartphones, and smartwatches. Each of those devices is vying for our attention. Each of those devices is tossing notifications and alerts in our faces. They’re cleaving our attention down the middle, leading us by the nose <em>away</em> from what we need or should be doing.</p>
<p>The key driver of calm technology, in my opinion, is <em>attention</em>. To quote Amber Case again:</p>
<blockquote>
<p>Although the number of alerts vying for our attention has increased, the amount of attention we have stays the same.</p>
</blockquote>
<p>In theory, those notifications should adapt to that amount of attention while melding into your personal flow. Rather than jolt your attention with a loud buzzer or a bright and flashing light, a calm notification draws your attention away with a subtle cue.</p>
<p>Calm technology lies at the intersection between the devices and services that we use and a field called <em><a href="https://en.wikipedia.org/wiki/User_experience">user experience</a></em> (UX for short). Calm technology is as much about the underlying technology as it is a framework for how we interact with that technology.</p>
<p>Devices and notifications have woven themselves into the fabric of our lives. Woven themselves deeply into that fabric. They need to disappear into our environment when they’re not needed. There shouldn’t be a constant feed of notifications and alerts either directly to your sense or to the peripheries of your senses. Instead, you should only get the notifications and alerts that you <strong>need</strong>. As Case writes:</p>
<blockquote>
<p>ideally, technology should allow use to shift very briefly, get the information that we need, and then shift back.</p>
</blockquote>
<p>A good, though rudimentary, example of the application of calm technology (and the shifting Case talked about) is the Pebble Time smartwatch. I bought one in November, 2015 and used it until Pebble went away. Mainly, I used the watch to get notifications of interactions on Twitter and Mastodon, of incoming phone calls and text messages, and of emails I’ve received.</p>
<p>I could have gotten more notifications, but I didn’t see the point. In fact, I pared back on the number of notifications that were pumped to my watch. What I did receive, however, gave me an at-a-glance indication of what needed my attention momentarily. To be entirely honest, though, very little of that needed my immediate attention. Not even for a brief moment.</p>
<p>In pondering calm technology, I’ve realized that there’s one problem it <em>doesn’t</em> solve: the problem of the pervasiveness of technology in our lives. While calm technology pushes notifications and, by extension, technology to the peripheries, it doesn’t completely offer a respite from technology.</p>
<p>In that respect, calm technology fails. It lessens the presence of technology in our lives, but doesn’t eliminate it completely. Signs and signals are still there, although we’re not incessantly bombarded with those signs and signals. Calm technology gives us some control, it gives us something of a break from technology.</p>
<p>On the other hand, calm technology offers few options to completely opt out. If you want to get away from notifications, from the signs and signals, you either need to turn them all off or turn off your devices. For many people, neither is a viable (or, at least, acceptable) option.</p>
<p>Many implementations of calm technology are predicated on us installing apps on our smartphones and tablets. Those apps are the conduits for alerts and notifications our devices send us. I find that to be rather presumptuous. What if you don’t want to clog up your mobile devices with another app? If you do wind up installing the app, what happens when your device isn’t within reach or earshot? In that case, you need to be in line of sight of your phone or tablet. Alerts will need to be easy to decipher and understand if they’re to remain calm.</p>
<p>Calm technology is definitely an interesting concept. But it’s one that doesn’t do quite enough about creating a stronger division between ourselves and the technology we use. That could, however, change in the future. Just for that, I’m looking forward to see how calm technology matures in the coming years.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
