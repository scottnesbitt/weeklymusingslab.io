<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 207</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 207</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time ’round, an idea that’s been floating around in my brain for quite a while. In fact, I didn’t realize that ideas was lodged in the old gray matter. The human subconscious is a wonderful thing, ain’t it? And all it took to crystalize that idea was some innocuous speculation prompted by someone I know. Funny how that works.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-software-minimalism">On Software Minimalism</h2>
<p>It all started, as things like this sometimes do, with a question.</p>
<p>A little while before I started tapping out this edition of the letter, a friend asked which MacBook I’d buy if I was in the market for one. I’m not and probably never will be. I’ve used Macs at points in past and while they’re solid computers, I’m just not all that impressed with them.</p>
<p>Pressed by said friend, my reply was that I’d get a slightly older MacBook Air — maybe of pre-2020 vintage. They were more than a bit surprised, both at my choice of the Air and that I’d buy an older one. That surprise doubled when, after being asked about what software I’d run on that laptop, I said a note taking tool like <a href="https://auernotes.com/">Auer Notes</a> or <a href="https://notational.net/">Notational Velocity</a>, a text editor, a web browser, Pandoc, and maybe couple of other small tools.</p>
<p>After a moment, they said <em>You’re really going full digital minimalist with that, aren’t you?</em> I retorted that I was, in fact, going full <em>software minimalist</em>.</p>
<p>So what’s the difference? And why is software minimalism so important to me?</p>
<p>Let’s look at some (admittedly loose) definitions. Digital minimalism about being deliberate and mindful about how we use our devices. About what apps and services use, or don’t use as the case may be. Digital minimalism is about applying that deliberation and mindfulness to eliminate distraction and to regain focus, to our break addiction to (or just preoccupation with) the online world.</p>
<p>Software minimalism, at least as I see it, is similar to digital minimalism but diverges somewhat from the latter concept. One of the main differences is that software minimalism is wrapped up in the idea of <a href="https://47nil.com/lightweight.html">being <em>light</em></a>. Software minimalism takes its cue from the minimalism practiced in the physical world. It’s more about minimalism in <em>quantity</em> of software.</p>
<p>The central idea of software minimalism is to only have the software and apps that you <em>need</em> on your computer or device. By that, I mean software and apps that you use <em>every day</em>. Not software and apps that you use once in blue moon or which might need on that far away and fabled <em>some day</em>.</p>
<p>Software minimalism involves <a href="https://scottnesbitt.online/eliminate-whats-not-essential.html">eliminating what’s not essential</a>. It involves whittling what you have down to one application for a task (or set of tasks). Do you need a trio of browsers, a brace of text editors, a handful of note taking apps, or other sets of applications that have a lot of overlap? Probably not.</p>
<p>Most of us can strip down the amount of software that we have installed on our devices. The results of doing that can be beneficial, believe it or not. You no longer have a dock or task bar or desktop filled with icons. You no longer run into friction and head scratching as you try to figure out what file is where. Software minimalism prompts you to focus on your workflows and pare them down to their essentials, rather than trying to maintain all the moving parts of a more complex workflow.</p>
<p>Take this example from my own life: I do all of my work in <a href="https://plaintextproject.online">plain text</a>. Which means spending my time in a text editor. In the past, I would also use a dedicated <a href="https://scottnesbitt.gumroad.com/l/learnmarkdown">Markdown</a> editor to write blog posts, articles, essays, book chapters, and the like. One day, I paused to ponder what that dedicated Markdown editor offered me that my (albeit simple) text editor didn’t.</p>
<p>It turns out that I never took advantage of the formatting toolbar in that Markdown editor. Being able to preview a file was nice, as was the ability to export my work in various other formats. But I rarely used those features. So, I promptly got rid of the Markdown editor and never looked back. I’m not any less productive after ditching it.</p>
<p>What about when you need a piece of software that you only use occasionally? In those situations, I feel fortunate to be Linux user: I have so much solid software at my fingertips, which I can install at will and don’t have to worry about licensing fees. Let me give you another example: a tool to manipulate pages in a PDF file. There are several available for the Linux desktop, and I need to use one perhaps once or twice a year. When I need that application, I install it, fiddle with a file or two, then uninstall the software. Talk about out of sight, out of mind.</p>
<p>I’ve been told that it doesn’t matter how much software someone has installed on their devices. These days, everyone has hectares of hard drive or storage space, so they can pile in as much as they want to. But why should you? Just because you <em>can</em> doesn’t mean you <em>should</em>. Do you really need to fill up all of that real estate with software that you rarely, if ever, use? There’s little or no advantage to doing that. More isn’t necessarily better.</p>
<p>When I’ve mentioned ideas around software minimalism (even before started using term), some people immediately turned around and proclaimed <em>But I can’t do without …</em>. My reply was: <em>How do you know that if you haven’t tried?</em>. Don’t know if I got through to them, but if nothing else, I gave them pause for thought.</p>
<p>In my own embrace of software minimalism, I’ve focused not just on stripping back the number of applications that I use but also on using slimmer, lighter, quieter software. Which led me to embrace workflows that have fewer (digital) moving parts, and which allow me to focus on what I’m doing rather than jumping between software and services to try get something done. In turn, that’s diverted me from the temptation to constantly look around for the next tool or app that will turn me into a productivity deity, that will revolutionize my workflow, that will make my processes more seamless and efficient. I can do that by subtracting, not constantly adding or hacking.</p>
<p>I’m satisfied with the software that I’m using because it does what I need it to do. With a minimum of fuss, with a minimum of overhead. My software might not do 38 different jobs and have every feature under the sun, but so what? I’d only use it for 2 or 3 of those jobs, and would only take advantage of a <em>very</em> small subset of its features.</p>
<p>By embracing software minimalism, I’ve lost nothing. But I <em>have</em> gained by not using more and/or heavier tools.</p>
<hr />
<p>Software minimalism isn’t for everyone. But I think for the so-called <em>average computer user</em> (and even for someone not-so-average), it can be a way to eliminate digital clutter. It can be a way to gain a bit control. It can be a way to wipe away more than a bit of friction.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
