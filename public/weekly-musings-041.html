<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 041</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 041</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This week’s essay is taken from a short collection of pieces on Japan. I’ve reworked the essay a bit, but the angle is still the same: a short chronicle of something I encountered during a three-month trip to Japan back in the early 1990s. Time and more than a few blows to the head haven’t dimmed that memory. I hope you enjoy it.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-whats-under-wraps">On What’s Under Wraps</h2>
<p>One week into my sojourn to Japan back in the early 1990s, and I found myself without something to read.</p>
<p>The three paperback I brought with me, which were meant to last a month, were read on the flight from Toronto and during those first few jet-lagged late nights and early mornings on the ground in <a href="https://en.wikipedia.org/wiki/Amagasaki">Amagasaki City</a>. I had, to paraphrase Ernest Hemingway, burned through those three books like a drunkard burns through his patrimony.</p>
<p>While I was reading <em>The Japan Times</em>, the <em>Daily Yomiuri</em>, and the <em>Asahi Shimbun</em> (Japan’s three major English-language daily newspapers) each morning, I needed something more. I needed to go beyond news and fact and opinion. I needed more depth, more breadth.</p>
<p>What I needed was the physical and intellectual heft of a book. Desperately.</p>
<p>You might be wondering why I needed a few somethings to read on that trip. After all, I was in Japan. A fascinating country with so much to see, so much to do. When I travel, I need some time at the end of a busy day of exploring. Time to unwind. To reflect. To process. For me, reading a book has always been a great way to do that.</p>
<p>On top of that, I was staying with a friend during the first month or so of my trip. A friend with a somewhat active romantic life, which required me to vanish from his small apartment overnight. I did so cheerfully, and having a book handy was a good way to while away the wee hours in one of the all-night cafes I wound up frequenting in Kobe and Osaka.</p>
<p>Early one afternoon shortly after I ran out of books, my ambles around Osaka deposited me outside the entrance of <a href="https://www.kinokuniya.co.jp/">Kinokuniya Books</a> at <a href="https://en.wikipedia.org/wiki/Umeda_Station">Umeda Station</a>. With a bit of trepidation, I walked through the entrance. I wasn’t entirely sure what I’d find. An attendant by the door greeted me with a smile and a cheery <em>Irrashaimase!</em> (<em>Welcome!</em>).</p>
<p>As I walked around, I marvelled at the books — the number of them, the covers of which were so different from books I was used to. As I wandered, I was somehow magnetically pulled into Kinokuniya’s English-language books section. In the larger scheme of the store, that section was small. It was also, however, comforting.</p>
<p>After thumbing through a mix of classics, recent bestsellers, and books aimed at people studying Japanese, I greedily snatched up three volumes. Those were <em>Essays</em> by Michel de Montaigne, <em>Go Tell It On The Mountain</em> by James Baldwin, and <em>Japanese Reading Program with Basic 997 Words</em>. The first two offered the depth and breadth I was looking for in my reading. The latter was an attempt to bolster my <strong>very</strong> limited Japanese vocabulary.</p>
<p>At the cashier’s desk, the cashier rang up the books and in a bit of a daze I handed over several 1,000 yen bills. I don’t remember how much those books cost, just that the English ones were bloody expensive! I do remember, though, forgoing a couple or three meals to be able to afford those three volumes.</p>
<p>The cashier passed the books to a colleague, who I thought would slip them into a bag which he’d then pass to me. But, in the corner of my eye, I noticed something curious happening.</p>
<p>I turned my head right and watched as the colleague placed the first of the three books on a dun-coloured sheet of paper. With a series of dexterous folds and a smoothing swipe of his hand, he had wrapped the book. He then repeated that process with the two other books, said something I didn’t quite catch, and handed them over. Still fascinated by what he had just done, all I could do is offer a weak <em>Arigatou</em>.</p>
<p>I found a bench outside of Umeda station and took a closer look at those books. The assistant at Kinokuniya hadn’t wrapped the books up. At least, not in the way I thought he had. Those books weren’t encased in paper like a present. In an act of bibliographic origami, he’d wrapped a thin, though robust, paper cover around them.</p>
<p>Wrapping my books like that was more than just an example of the wonderful customer service I came to know during my three months in Japan, regardless of what I bought. That wrapping also served a practical purpose or two.</p>
<p>Have you noticed how the edges of a paperback’s cover, or the faces of the covers themselves, get scuffed and worn over time? That’s called <em>shelfwear</em>. The fastidious side of me, the side that likes to take care of his possessions and keep them immaculate, really hates that. Those covers offer some protection for books. We’re all familiar, I’m sure, with the mild battering that the edges of a book took takes while it jostles around in a bag or your pocket. Those covers, bless them. stop that scuffing as you pull the books off the shelf out out of that bag or pocket.</p>
<p>The wrap also makes your book anonymous. Want to read the latest trashy bestseller in public (or whatever their early 1990s analogues were) without attracting attention? No problem. The wrap hides the cover of the book you’re embarrassed to read from prying eyes. No one will ask you what you’re reading, either. Then again, it’s not as if that would happen in Japan anyway — that would go against various social norms.</p>
<p>Of course, the wrap also serves one more purpose: free advertising for the book seller. It’s a cheap way to promote a business!</p>
<p>It wasn’t just large chains like Kinokuniya that offered this simple, useful service. Smaller chains and even some independent booksellers took the time to wrap their wares. The pattern was often the same: if the cashiers didn’t pass books off to their colleagues, they did the job themselves. No matter where I was, everyone used the same sequences of folds and smooths. I can’t tell you how many times I tried to duplicate what they did. My 10 thumbs just wouldn’t allow it.</p>
<p>I bought several books while in Japan. Unless they were the size of a textbook or were a thick trade paperback, someone invariably wrapped those books up. While the paper was thin, it was surprisingly durable. All the books from Japan that I held on to still had those covers on them over a decade later. Sure, those wraps were a little more worn, a little more threadbare (or whatever the equivalent for paper is). But they were still doing their jobs.</p>
<p>Who says that the simple things in life don’t matter and don’t last?</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
