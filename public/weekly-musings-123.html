<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 123</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 123</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>After last week’s edition hit the inboxes, a few of the fives who subscribe to <em>Weekly Musings</em> got in touch asking if I was planning on pulling the plug on the letter. No such luck. You’re stuck with me for a while longer.</p>
<p>This time ’round, I’m going back to a topic that I looked at a some time ago. It’s a topic I’ve been mentioning in a few recent letters so I thought it was high time to give it another look.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-the-diy-web-redux">On the DIY Web, Redux</h2>
<p>Over the years, the web has gotten big. And it’s just getting bigger. Not just in number of sites (which is in the 1.8 billion range), but also in the sizes of many of those sites.</p>
<p>A lot of that bulk comes from people using database-backed content management systems and web frameworks to build their sites. And I’m not just talking about business or ecommerce sites, either. People construct their personal sites using platforms like WordPress. They’re building sites which, for the most part, really don’t need all that power and leverage.</p>
<p>There’s a lot of waste online. It’s waste that really doesn’t need to exist.</p>
<p>But it doesn’t need to be that way. An idea that’s been floating around for a while online is that the web can be smaller again. Maybe not all of it, but definitely the personal portion of the web. Some people call that the <em>smallnet</em> or the <em>small web</em>. I’ve taken to calling it the <a href="weekly-musings-069.html">DIY web</a>. It’s a web that’s handcrafted using <a href="https://scottnesbitt.gumroad.com/l/learnhtml">HTML</a> or with software or services that churn out simple, functional pages or sites.</p>
<p>No matter what tag you slap on that idea, I believe that it can make the web a somewhat better, a somewhat friendlier place, definitely a more compact place.</p>
<p>As a friend said:</p>
<blockquote>
<p>[T]he smallnet reminds me of what the web <em>felt</em> like in its earlier days, and I’m reminded of what <em>I</em> felt like during that time.</p>
</blockquote>
<p>If that’s not a reason to go small or DIY, what is? There are more than a few others …</p>
<p>How many people consider their impact on the environment when using the web? Probably not all that many. I have to admit I don’t always do that. Do you? Well, publishing and serving web pages uses energy. Accessing those page uses energy. Often, it’s energy that’s generated using carbon-belching power plants (though that’s starting to change).</p>
<p>The effects add up as more and more people head over to the various sites that cram the web. According to the <a href="https://www.websitecarbon.com/">Website Carbon Calculator</a>:</p>
<blockquote>
<p>The average web page tested produces 1.76 grams CO<sub>2</sub> per page view. For a website with 10,000 monthly page views, that’s 211 kg CO<sub>2</sub> per year.</p>
</blockquote>
<p>Multiply that by 1.8 billion and you get a big, scary amount of CO<sub>2</sub>.</p>
<p>The DIY web can have a smaller environmental footprint. It takes a lot less energy to serve, say, a 512 KB page or a simple, small website than it does for a server to pump out one built with <a href="https://en.wikipedia.org/wiki/PHP">PHP</a> (a scripting language used to develop websites) and a database, a site that contains several megabytes worth of graphics or multimedia. Stuff that really doesn’t need to be used. Which takes us to …</p>
<p>A core concept of the DIY web is minimalism. On all levels. The aesthetic of the DIY web isn’t about design (or what some people <em>think</em> is design). It’s not about looks. That’s not to say a DIY website can’t be attractive. It’s attractiveness that’s lean, that’s streamlined, that’s simple. So lean, streamlined, and simple that you don’t notice how attractive it is.</p>
<p>The DIY web isn’t (necessarily) about <a href="https://brutalist-web.design/">brutalist web design</a>. It can, and should, be clean, minimal, easy to engage with. It should be easy to read and to follow. It should be nicely laid out. That minimalist aesthetic gets to the point — kind of like reading an article at, say, the <a href="https://foreignpolicy.com/">Foreign Policy magazine</a> website using <a href="https://support.mozilla.org/en-US/kb/firefox-reader-view-clutter-free-web-pages">Firefox’s Reader View</a>. There’s no cruft, there’s no fluff. Just want you need to see.</p>
<p>That aesthetic eschews the idea that visuals and fancy layouts and complex design are a key element of a web page or website. It’s what’s <em>on</em> the page, not how the page looks, which is most important.</p>
<p>The DIY web’s minimalism goes beyond looks. Does a page or site need JavaScript (or any script) so visitors can interact with it? Does it need complex, convoluted <a href="https://en.wikipedia.org/wiki/CSS">CSS</a> (short for Cascading Style Sheets, a way of formatting web pages) for layout or will a <a href="https://plaintextproject.online/articles/2021/03/16/css.html">minimal stylesheet</a> do? For a personal site, the answers to those questions are <em>No</em>, <em>No</em>, and <em>Yes</em>.</p>
<p>That minimalism is reminiscent of the <a href="https://www.lotuscars.com/en-GB/lotus-philosophy/">design philosophy</a> that <a href="https://en.wikipedia.org/wiki/Colin_Chapman">Colin Chapman</a> laid down for Lotus’ racing and road cars: <em>Simplify, then add lightness</em>.</p>
<p>Like the <a href="https://thoughts.natedickson.com/web-primitivism">author of this post</a>, I’m not against something called <em>web primitivism</em>, but a dash of CSS can make a site or page more engaging. A smidgen of styling can make a site or page a bit more appealing. But you don’t need to go overboard by building a page or site around a convoluted <a href="https://en.wikipedia.org/wiki/CSS_framework">CSS framework</a> to do that.</p>
<p>An important (though not essential) feature of DIY web is the ability to self host. And that, in some ways, is holding the DIY web back. Anyone who wants to can, over the space of a few hours, learn enough HTML to build a simple website. Hosting a site using your own server, though, is something completely different and requires a different level of knowledge. Doing that is <a href="https://opensourcemusings.com/to-self-host-or-not.html">not exactly easy</a> for person without, or without the desire to develop, technical skills.</p>
<p>There are folks, like the people behind the <a href="https://small-tech.org/">Small Technology Foundation</a>, who are working to make self hosting less daunting. But until hosting your own site is as easy as installing a piece of software with a couple of clicks, self hosting will only be an option for the few. During the wait for that day to come, many of us will have to rely on other peoples’ computers to publish online.</p>
<p>Another important aspect of the DIY web is <em>simplicity</em>, of the sort promoted by <a href="https://jeffhuang.com/designed_to_last/">computer scientist Jeff Huang</a>. Among the ideas that he espouses, Huang advocates using a single page rather than several. This makes a site easier to maintain, embraces the minimalism that I mentioned earlier, and can be a great solution for a personal site.</p>
<p>Earlier this year, I consolidated the several pages of <a href="https://scottnesbitt.net">my personal site</a> into something resembling a traditional <a href="https://en.wikipedia.org/wiki/Home_page">home page</a>. Using plain <a href="https://en.wikipedia.org/wiki/HTML5">HTML5</a>, I was able to make the page visually compact even though there’s a lot information on it.</p>
<p>I admit going for a single page is workable for a personal website. It’s not the best option for a site that’s a bit more complex. There’s no way I can, for example, turn one of my online publishing channels, <a href="https://plaintextproject.online">The Plain Text Project</a>, into a single page site. Still, not everyone is building a large site like that (even if it is hand hewn).</p>
<p>As wrote back in <a href="weekly-musings-069.html">Musing 069</a>, the DIY web isn’t for everyone or for every purpose. It can be a simple, minimal, and effective way to stake your claim to a piece of the web. One which has a small footprint (in several ways), but one which also enables you to project your voice and to share your thoughts with a wider world.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
