<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 117</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 117</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Welcome back! It’s good to be here again, sharing thoughts and ideas with you via this letter. Thanks for joining me.</p>
<p>This week, the thoughts I’m sharing are about a technology that’s been close to my (digital) heart for quite a long time. A technology that, in many ways, shaped my ideas around and deepened my understanding of collaboration.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-wikis">On Wikis</h2>
<p>In around 2002, I started getting interested in a particular tool for publishing online. Before running into that tool, I’d been crafting web pages by hand in <a href="https://gum.co/learnhtml">HTML</a> (with a dash of fairly lame CSS added for good measure). Those pages were nothing fancy, nothing visually stunning. They worked, but they took a while to pull together.</p>
<p>The tool I just mentioned, though, made that a lot easier. For that reason, at least at first, it didn’t just catch my attention. It grabbed hold of it. I’d been aware of that kind of tool for few years, but never got around to seriously investigating it. Until I started using one at the then-Day Job<sup>TM</sup>, where my team used it to publish internal documentation and notes.</p>
<p>That tool was a <em><a href="https://en.wikipedia.org/wiki/Wiki">wiki</a></em>. To be entirely honest, that wiki was more than a bit of a revelation. Not only could I quickly publish something, I could publish without fiddling with HTML. Instead, I used <em>wikitext</em>, a <a href="https://scottnesbitt.gumroad.com/l/learnmarkdown">Markdown</a>-like way of using keyboard symbols to format a page. That wiki enabled me to collaborate with others without passing files around via email or by plonking them on network shares. That wiki enabled me, and others, to instantly publish, as well as to quickly update what we’d publish and to get almost immediate feedback.</p>
<p>That wiki changed how I thought about publishing on web and about collaboration. And I quickly began thinking about wikis and how technical writers (and other writers) like myself could take advantage of them. Over the next few years, I came under influence of others who knew more about wikis than I did at the time, learning so much at their (digital) feet. Their influence changed way in which I looked at how I did my work and how I looked at collaboration in general.</p>
<p>While I haven’t used wikis as intensively or as regularly as I once did, I still believe they can be useful and effective tools for working and for organizing thoughts and information.</p>
<p>So just what is a wiki? You can learn more about their history <a href="https://en.wikipedia.org/wiki/History_of_wikis">over at Wikipedia</a> (and, no, I’m not being ironic!). The easiest way to think of a wiki, though, is as a web page that you can quickly and easily edit in a web browser. There’s a lot more to a wiki than that.</p>
<p>Wikis are, at their heart, repositories of knowledge and information. Knowledge and information that’s ready and waiting to be shared, to be spread as widely (or not) as you see fit. I’ve seen wikis used to deliver documentation, as knowledge bases, personal websites, <a href="weekly-musings-092.html">digital gardens</a>, blogs, and more. A wiki can be just about anything you want or need it to be.</p>
<p>Here’s how <a href="https://en.wikipedia.org/wiki/Ward_Cunningham">Ward Cunningham</a>, who created the wiki, encapsulated the tool’s greatest strength:</p>
<blockquote>
<p>A wiki works best where you’re trying to answer a question that you can’t easily pose, where there’s not a natural structure that’s known in advance to what you need to know</p>
</blockquote>
<p>One of the beauties of a wiki is that it’s flexible. Not just in what you can do with them, but in <em>how</em> you’re using them. If you have the technical chops to install and maintain all the (web) plumbing, you can host your own. Many web hosting companies sport something called a <a href="https://en.wikipedia.org/wiki/CPanel">one-click installer</a>, which takes away some of the technical pain of installing wiki software. There are still a few hosted wikis out there — I started using wikis for my own work via a service called PBWiki (now PBWorks) in 2005. You can even install wiki software <a href="https://en.wikipedia.org/wiki/Personal_wiki#Single-user_wiki_software">on your desktop</a>.</p>
<p>That flexibility goes beyond the software itself. Wikis, as I mentioned, can be tools for collaboration. They can be for your use and yours alone. Or, you can have a personal wiki and open bits of it up to others. It’s not one or the other, as someone tried to convince me many a year ago. <em>You</em> choose how to use a wiki and who, if anyone, uses it with you.</p>
<p>Wikis give collaborators license to fiddle, modify, correct, expand upon, update, or edit a page or a document. All without the worries, and stigma attached to, violating the supposed sanctity of <em>someone else’s document</em>. Instead, wikis can encourage collaboration and consensus. Everything is done in public, in the open. The process is transparent. There’s no need to bounce files back and forth, with the requisite worry that you’ll spend time working on the wrong version of a file. There are no email or chat threads to wade back through, as if scrolling back in time, to find a comment or idea or someone’s rationale for doing something. Everything’s on the (wiki) page, staring you in the face.</p>
<p>When I first embraced wikis, they gave me writing tool that I could use anywhere — regardless of whether I was using a desktop PC, a laptop, or a <a href="weekly-musings-071.html">netbook</a>. This was in the days) before collaborative online tools like Google Docs or <a href="https://en.wikipedia.org/wiki/Etherpad">EtherPad</a> were a thing. Later, wikis freed me from the shackles of online writing tools run by large platform companies or smaller developers who might disappear. When I was working, my daughter would ask me <em>Are you writing in your wiki again?</em></p>
<p>At that time, I tried to fully explore the flexibility of a wiki. I used them not just for writing, but also for planning and scripting presentations, for task management, for time tracking, for collaborating with freelance/contracting clients, and more. At no time did a wiki that I was using let me down.</p>
<p>It didn’t take me long to realize that wikis work in way the same way in which my brain works. I could link ideas together easily and in one place. I could easily make connections between seemingly disparate pages which were, in fact, related or complementary.</p>
<p>In tech terms, wikis can seem more than a bit archaic. After all, they’re 26 or so years old — fossils when their age is measured in internet time. As a few people have recently mentioned to me, why use a wiki when you have more <em>modern</em>, shiny, sexy tools for organizing information like <a href="https://obsidian.md/">Obsidian</a> and <a href="https://roamresearch.com/">Roam Research</a>?</p>
<p>Tools like that are nothing new. They do pretty much what a wiki does. To me, Obsidian, Roam Research, and their ilk are merely the next step in evolution of wikis. They’re essentially wikis — they just have some interesting bits and pieces bolted on for good measure.</p>
<p>I’m not sure if tools like Obsidian and Roam Research are <em>better</em> or <em>more effective</em> than a wiki. While they have more bells and whistles than a wiki packs out of the box, that doesn’t necessarily make them better or more effective. To be honest, just about everything I need from tools like Obsidian and Roam Research I can get from a wiki.</p>
<p>That said, wikis are by no means perfect. If you aren’t careful or mindful, a wiki can grow out of control. The pages can become like weeds in garden. Information which hasn’t been updated or is obsolete can clog up a wiki before you realize it. Going back to Ward Cunningham:</p>
<blockquote>
<p>for every hour spent organizing, two more hours are spent adding new material. So the status quo for a wiki is always partially organized</p>
</blockquote>
<p>It takes effort to prevent that, to even maintain that stage of partial organization. You need one or more dedicated <a href="https://www.stewartmader.com/home/wikipatterns/gnome/">wiki gnomes</a> (also called <em>wiki gardeners</em>) to regularly do some trimming and weeding. Or you need to set aside the time to do the gardening yourself.</p>
<p>On top of that, wikis don’t always look all that attractive. That’s shallow, I admit, but for more than a handful of folks looks <em>do</em> matter when it comes to their tools. Out of box, most wikis are functional but plain. Sometimes, they’re downright cluttered or homely. You can make a wiki prettier, you can make one more user friendly — for both readers and for people who are publishing using a wiki. That can take a bit of time and effort, though, and you still might not get the results that you want.</p>
<p>Like any other tool, a wiki isn’t for every purpose. It’s not for everyone. Wikis work for some folks, but they don’t work for others.</p>
<p>For me, a wiki is a useful tool. Like <a href="weekly-musings-108.html">outliners</a>, wikis work in the way in which I think. That’s what makes them such a great fit for me and for what I do.</p>
<p>While I haven’t used wikis as much as I used to in recent years, they’re one of the few pieces of software that have held my attention continuously for … well, for a <em>long</em> time. I’m seriously thinking of getting back into using them more intensively. Computing like I’m back in the early 2000s and all that.</p>
<p>If (more likely when) I do start embracing wikis again, I’ll let you know what happens.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
