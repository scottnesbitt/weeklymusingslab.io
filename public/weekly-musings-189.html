<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 189</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 189</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>This time ’round, a slightly more personal edition of the letter. One that looks at a subject that’s close to my heart, and I hope to yours.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-engaging-with-what-we-read">On Engaging with What We Read</h2>
<p>If nothing else, 2022 was a good year for reading. I managed to work my way through a number of new or new-ish volumes, a couple of classic I’d never read before, and re-read a handful of books that I hadn’t set my eyes on in decades. I even managed to cap the (reading) year off with Thomas Piketty’s tome <em>Capital in the Twenty-First Century</em>. A book that left me drained and overwhelmed, but also excited.</p>
<p>What might surprise you, though, is the number of books I read in 2022: 21 of them. A couple of people I know pointed out, oh so helpfully, that there are others out there who read eight, nine, or even 10 times that number of books in the same 12 months. And, continuing their efforts to be helpful, those acquaintances pointed me to articles that purport to explain how anyone can wade through 100, 150, or even 200 books in the space of 365 days.</p>
<p>I’m not interested in any of those articles. I’m not interested in the conceit behind them. And I’m just not interested in taking in that volume of words.</p>
<p>Unlike my acquaintances, I’m not in awe of or impressed by folks who read books in triple digits every year. And I don’t believe that’s the best way to approach reading — as something to <em>maximize</em> or <em>optimize</em>. As a way to gain bragging rights. As something that borders on a competition. As something that’s infected by the virus of extreme productivity hacking.</p>
<p>While it’s a laudable goal to be well read (or, at least, widely read), it’s not laudable to read anything and everything. Especially the anything and everything others tell you that you <em>need</em> to read. Doing that dissipates your mental energies. It doesn’t provide you with depth. I’m not sure that it even provides you with true breadth. If you’re trying to read in bulk, you’re taking in what amounts to snippets, quotes, highlights, and key points. Not the deeper, more important ideas behind those snippets, quotes, highlights, and key points.</p>
<p>In his <em>Dialogues</em>, the Roman philosopher Seneca admonished:</p>
<blockquote>
<p>It is much better to devote yourself to a few writers than to skim through many.</p>
</blockquote>
<p>Admittedly, I don’t follow that advice as closely as I should. But I do try to limit the number of books that I read annually to between 20 and 25. Books of varying lengths, of varying genres, of varying subjects. I also try to re-read at least three or four other books each year.</p>
<p>Why do I choose to work my way through so few books per annum? Sometimes, to be honest, I think I try to read too many … But I do that so I can <em>engage</em> with what’s on the pages that pass before my eyes and my brain. And that engagement is a key element missing from the push to read as much as possible in as short a time as possible.</p>
<p>When engaging with books, I make an effort to savour every sentence, every word. To pause to ponder the ideas that jump out at my from the page. To get closer to both. I can do that because I’m taking my time. I’m not skimming. I’m not searching for keywords or points that I can bring up in conversation.</p>
<p>Instead, I’m reading line by line, word by word. I take the time to reflect upon a paragraph or a phrase or an idea that jumps out. I take a moment or two to jot down a note about what I’ve just read.</p>
<p>Being able to engage with a book requires focus, concentration, and a level of mental energy, none of which are easy to muster at the best of times. Engagement doesn’t come in short sprints, but at a prolonged and steady pace. It requires a level of concentration that waxes and wanes depending on the day and what happened during that day.</p>
<p>When I read, I don’t have a set time limit. Typically, I read until, for example, I reach the end of a chapter or a section. That might take me 20 minutes. It might take me an hour. But during that time, I’m engaged with what’s on those pages — whether physical or digital. Those pages are my sole focus.</p>
<p>That seemingly minuscule amount of time is actually a lot if a book engages me. I’m getting more out of that time, and the small number of pages that I’m reading, than someone who is ripping through dozens of pages at high speed.</p>
<p>Marshaling and sustaining that focus isn’t easy. There are days when I struggle to start reading or to continue to read. Every so often, I forgo reading that day because I know that the concentration required to do the deed just isn’t there. That I won’t be able to fully engage with what I’m reading, no matter how light or deep that book is.</p>
<p>To engage with what you read, you also need to <em>slow down</em>. To read deliberately. To go line by line, word by word. You need to resist the urge to rush ahead. You need to allow an author to develop their points, rather than wanting everything laid out for you <em>now</em>. You need to follow the path of the book, follow the journey of the author.</p>
<p>It’s easier to engage with a book if it resonates with you, if it’s in sync with your interest, your ideas, and the paths that you want to follow. That might not be a book that everyone in your circle, or who you look to for advice about what to read, is cracking open. That book might not be a so-called <em>must read</em> that will help you get ahead in business or your career. It might not even be on any best seller list.</p>
<p>The book that resonates with you could be an unlikely volume. In the last couple of years, a couple of those books for me have been <em>The City is Not a Computer</em> (you can read my <a href="https://opensource.com/article/22/6/2022-opensourcecom-summer-reading-list">short review here</a> if you’re interested) and <em>Weapons of Math Distraction</em>. One is more academic in tone and style, while the other covers a subject with which I have little interest. Yet both books drew me in thanks to the ideas on their pages and how those ideas were conveyed.</p>
<p>Knocking off a certain number of pages per day isn’t an accomplishment if you don’t get much out of those pages. Reading isn’t about quotas. It isn’t about making up the numbers or hitting goals. It’s not about bragging rights. And reading definitely isn’t a competition.</p>
<p>Reading is about feeding your mind. It’s about challenging yourself. It’s about shifting gears. It’s about losing yourself in ideas and stories. It’s a way of relaxing and enjoying yourself. None of that comes if you’re trying to blitz through several hundred words a minute. None of that comes if you’re not engaged with what you’re reading. None of that comes if you’re desperately racing through what’s in your hands to get to the next book that you’ve been told that you <em>must</em> read.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
