<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 104</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 104</h1>
       </header>
	   <p>This letter marks something of a milestone: two years worth of musings. One hundred and four essays. Over 80,000 words. A bunch of different topics. Admittedly, the real second anniversary of <em>Weekly Musings</em> came and went a while ago but thanks to some hiatuses (planned and forced), it’s taken this long to publish Musing 104.</p>
<p>I’d just like to thank you all for supporting this little experiment in email publishing of mine. If it wasn’t for you, this letter wouldn’t exist.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-algorithms">On Algorithms</h2>
<p>Ah, the algorithm. A word that really only recently entered the popular lexicon. A word that … well, if it doesn’t strike fear into our hearts then it’s one which makes us wary.</p>
<p>And with good reason. Tech giants across the board have for years been releasing algorithms in the name of advancing their positions in the market and to bolster their bottom lines. All with little or not thought about how those algorithms can or will affect us.</p>
<p>Algorithms <em>do</em> affect us, whether we realize it or not. They mediate. They make choices. They judge. They shape. Often without us knowing it, even when it’s too late.</p>
<p>It wasn’t always that way. Algorithms have been around since before computers existed. They are, according to <a href="https://en.wikipedia.org/wiki/Algorithm">Wikipedia</a>:</p>
<blockquote>
<p>a finite sequence of well-defined, computer-implementable instructions, typically to solve a class of problems or to perform a computation</p>
</blockquote>
<p>Those problems can be pretty innocuous — like prioritizing items on a list or how much an individual contributes to society. Or those problems can be ones that are less benign, ones that can do us harm.</p>
<p>Nowadays, the algorithms that we hear about are ones which capture, funnel, and shape data and sundry information. What’s worse is that those algorithms are a <a href="https://en.wikipedia.org/wiki/Black_box">black box</a> — we don’t know how those algorithms are manipulating that data, especially our data. The results, though, can be be helpful. Often, the results are surprising. They’re shocking. They’re dehumanizing.</p>
<p>But are algorithms inherently evil? Or does their evil manifest itself in the hands of humans with less-than-transparent agendas? I’ll leave that to technology’s philosophers and ethicists to ponder and debate and arm wrestle over. To me, algorithms can be many things. They can be a source of annoyance. They can, as I mentioned a few moments ago, be dehumanizing. They can also be instruments of misinformation and misdirection.</p>
<p>Think about the two algorithms that you probably encounter most often in daily life. One of them is the algorithm that a streaming service like Spotify or Netflix uses to suggest what to listen to or watch or read based on your past choices. Those recommendations can be a source of an unintended chuckle or two — like recommending a rom-com after you’ve watched series of action flicks. Nothing too harmful, but a bit of a waste of time.</p>
<p>The other is the algorithm deployed by a search engine. Mainly, the search engine that I’m talking about is Google. Thanks to people using and abusing SEO, they get some <a href="https://www.techopedia.com/definition/31474/google-juice">Google juice</a> and their dubious <em>content</em> (yes, I’m using that in the pejorative) rises to the top of search results like curdled cream. Go back and read <a href="hweekly-musings-101.html">Musing 101</a> to learn why I think that’s harmful.</p>
<p>Those kinds of algorithms are used by large platforms to bait and keep users. Turning those users into, for lack of a better word, junkies who keep coming back to one platform or another for a fix. As LM Sacasas wrote in a 2017 blog post, platforms like Facebook and Twitter:</p>
<blockquote>
<p>[D]eploy sophisticated algorithms to serve users information they’re likely to care about.</p>
</blockquote>
<p>That information, as I pointed out earlier in this musing, is based on your previous choices and what you previously viewed. In doing that, a platform constructs a walled garden of information for you, whether you want the walled garden or not. A garden that’s filled with much dubious information of equally dubious provenance.</p>
<p>The information that algorithms channel into that garden doesn’t challenge or nourish you. It instead reaffirms your ideas and beliefs, often not in a good way. It builds divides rather than bridging chasms. It tugs at the strings of emotions and prejudices. It reinforces opinions and biases. It can misinform and mislead. It can lie and spread lies.</p>
<p>Worse still are the algorithms that have invaded our everyday lives. Some of them are the algorithms that the industrial security complex deploys to monitor us in the name of keeping the peace. Algorithms which have been known, and not in isolated cases, to zoom in on people whose skin colour might be a few shades darker than, perhaps, the people who developed those algorithms. Algorithms that can, and do, perpetuate some of the worst stereotypes and racial prejudices. Algorithms that can, and do, stigmatize groups in our society who are already looked at with a negative eye by some in positions of authority.</p>
<p>Only slightly less dehumanizing are the kinds of algorithms that companies like Amazon apply to their warehouses. Algorithms that expect workers to keep up a steady pace and cadence of work, of keeping to strict time frames for completing tasks. Paces and time frames that, when workers are starting their shifts and are mentally and physically fresh, can at best be described as grueling. Some would argue those paces and time frames are inhumane.</p>
<p>The problem is that the algorithm doesn’t take into account that people aren’t machines. That as time passes, people tire in both body and mind. That fatigue makes even simple tasks a struggle, that it causes people to lose focus. I can’t think of anyone, no matter how strong and fit, who can keep up the pace that an algorithm expects for as long as the algorithm expects them to.</p>
<p>Algorithms are everywhere. With each passing week, they’re becoming more and more tightly woven into the fabric of our digital and physical lives. But how can we push back against them?</p>
<p>I wish I had the answer. Or even <em>an</em> answer. The only way I’ve been able to push back is to stop using invasive platforms like Twitter, Google, and Amazon. That’s not enough, I know. It’s not an option for everyone. And for each person proclaiming <em>Delete Facebook!</em> or <em>Delete Twitter!</em> there’s someone who counters that instead we should demand a better Facebook or a better Twitter.</p>
<p>The problem with that is tech giants don’t have much incentive to change their ways if they’re still profitable and still pulling in I don’t know how many new users each day. You can demand better all you want. You just won’t get it. The algorithm will continue to win, to dominate.</p>
<p>This is a sticky problem to which there’s no easy solution. I’m not even sure that there is a solution. Maybe someone, preferably someones, will devise a way out. Until then, it’s up to each of us to navigate through the waters being churned by invasive algorithms as best we can.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
