<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 174</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 174</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>As sometimes happens in this space, what you’re about to read wasn’t what I had planned for this week’s letter. While writing <a href="weekly-musings-173.html">Musing 173</a>, a portion of that edition of the letter got some gears grinding in my brain. Which, when mixed with some thoughts I’ve been having lately, sparked an idea that took over my attention and focus. Which finds us here.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-travelling-light">On Travelling Light</h2>
<p>Travel’s been on my mind for quite few months now. It’s been over a year since my last trip — that one was within New Zealand, and it was relatively short. On top of that, it’s been almost three years since I last ventured outside the borders of the island on which live. I probably won’t take my next trip until sometime in 2023, and it might be another year or three after that before I summon up the pluck to venture overseas.</p>
<p>Still, those thoughts of travel linger. Not just that, but travelling as lightly as possible. Travelling as lightly as possible is … well, not (quite) an obsession of mine, but something I’ve been trying to refine. I’ve been working on that for well over 10 years. While I’m not bad at it, I can still do better.</p>
<p>Why travel light? Here’s <a href="https://urbancommuter.net/theneed.html">a little wisdom</a> from a blog that has both further informed and affirmed my ideas about lightweight travel:</p>
<blockquote>
<p>Carrying all those items that you mostly don’t use, ever, will slow you down. Think simple: simplify what you carry, how you dress, and the types of things you need.</p>
</blockquote>
<p>I don’t know about you, but I want to spend as little time in an airport (and the queues at an airport) as possible. I don’t want to worry about checking a bag, and having to wait by a carousel to retrieve it once I land. Assuming that bag hasn’t been lost, which is a distinct possibility these days. I don’t want to struggle with an awkward, bulky carry-on that takes more space than needed in a plane’s overhead compartment.</p>
<p>I want to be able to quickly stow my bag, either overhead or under seat in front of me. I want to be able to quickly grab that bag, get off the plane, make my way through customs (if I’m travelling overseas), and head out of the airport post haste. I want to be (to paraphrase Robert Fripp) a mobile, self-contained unit when on the road.</p>
<p>Taking one or more large pieces of luggage with me eliminates that mobility. It limits my ability to adapt to whatever situation I encounter. It limits me from easily venturing off the beaten path should the need or desire arise.</p>
<hr />
<p>Most of my trips last anywhere from five to seven days. I know more than a few people who, when taking trips that short, lug medium-sized wheelies that they need to check in. What they have in those pieces of luggage I’m afraid to ask. But I’ve found that for personal travel, for shorter trips (and sometimes longer ones), all of that is wasted bulk.</p>
<p>How do you go about travelling light? Like anything else, it all starts with a plan. Focus on what you <em>need</em> to bring with you, rather than what you think <em>should</em> bring. Don’t fall for the trap of thinking <em>I might need this. Just in case.</em> If you do, you’ll wind up packing more than you’ll use. That will only add to your burden and to your stress.</p>
<p>What do you need for a shorter trip? I usually bring the following, which fits into a 20 litre or 25 litre knapsack (with a bit of room to spare):</p>
<ul>
<li>Three sets each of shirts, underwear, and socks,</li>
<li>A spare pair of pants,</li>
<li>A toiletry kit,</li>
<li>A notebook and couple of pens,</li>
<li>A water bottle,</li>
<li>A collapsible travel mug,</li>
<li>A small passport and ticket pouch,</li>
<li>My phone and either a tablet or an ereader,</li>
<li>A couple of USB charging cables, and</li>
<li>My glasses.</li>
</ul>
<p>Depending on the season, I might also have a light or medium-weight jacket or fleece. If I’m attending an event, I might also bring a small laptop or a portable keyboard for my tablet.</p>
<p>My clothing is designed for travel — it’s generally light and fast drying. And, in case you’re wondering, I don’t wind up wearing smelly clothes for the last few days of any journey. I do laundry in my hotel room — my toiletry kit usually includes a small bottle or castile soap and an inexpensive travel clothesline.</p>
<p>What about items you forget or that you find you actually <em>do</em> need but didn’t pack? Consider having what Tim Ferriss calls a <em>settlement fund</em>. That’s money — anywhere from $50 to $200 (or its equivalent in the local currency) — that you set aside to buy various extra items. If you do have a settlement fund, stick to the basics. Don’t go overboard or you’ll have yet more to cart home with you.</p>
<p>A few paragraphs back, I mentioned one of the two keys to travelling light: focusing on <em>what you need to bring</em> rather than trying to pack for every scenario. The other key is to make what you pack as small as possible. I like to use packing cubes, which hold a lot more than they appear to. If you don’t want to go to the additional expense, try this instead: roll your shirts and underwear tightly, and carefully arrange them in your bag — shirts forming the base at the bottom of the bag — then carefully stack everything else on top. Having lightweight travel clothing makes that a lot easier.</p>
<p>While this mode of travelling doesn’t work for every trip (skiing vacation, anyone?), it’s a great way to carry what you need without worrying about lugging a bunch of suitcases around. And what I’ve just discussed isn’t just for travelling by air. You can use this advice when travelling by train or bus, or even on your daily or regular commute to work.</p>
<p>I’ll end this musing with an excerpt from another of my <a href="https://47nil.com/lightness.html">favourite blogs</a>:</p>
<blockquote>
<p>Traveling light allowed me to remain small and out of the way. Controlling what I bring enabled me to remove the uncertainty of checking a bag, for example, and knowing that my bag would arrive with me, because it <em>was</em> with me at all times. Having only one bag allowed me to decide on the spot whether I was going to head right to the hotel when I arrived, or, if I saw something cool along the way, jump off the train or whatever and go explore. I can’t control the craziness during travel, but I can control how much stuff I bring, and therefore I have a bit more control over some parts of that chaos.</p>
</blockquote>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
