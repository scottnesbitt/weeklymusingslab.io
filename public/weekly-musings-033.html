<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 033</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 033</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each week I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Over the last couple or three weeks, the human resources or people experience or whatever they call themselves department at The Day Job<sup>TM</sup> have been pushing <em>mindfulness</em>. They recommend employees install an app on their phones, do mindfulness exercises, and generally be more mindful of mindfulness.</p>
<p>As you might have guessed, I haven’t done any of that. It’s not because I find mindfulness to be a crock. I don’t. I just don’t agree with some interpretations of it. And I definitely don’t agree with mindfulness being the panacea for everything that ails us in the workplace or in our lives.</p>
<p>Anyway, mindfulness isn’t something you can embrace by downloading an app or doing a handful of exercises. Mindfulness is something you slowly develop over the span of months and years. Mindfulness is wrapped up in time.</p>
<p>It’s that idea which informs this week’s letter. Let’s get to it, shall we?</p>
<h2 id="on-art-time-and-mindfulness">On Art, Time, and Mindfulness</h2>
<p>Time. It’s something we’re all aware of. Time, or lack of it, is a source of stress. We vainly and futilely try to save time. We try to control or master time, to no avail.</p>
<p>The only way I can describe time is as a modern obsession. We all realize that time passes, often far too quickly for our liking. We want to control time, to harness it, to manage it. But we rarely try to have a relationship with time.</p>
<p>That said, I do believe it <em>is</em> possible to forge that relationship. All it takes is paying attention to time.</p>
<p>But do we really pay attention to time, beyond watching clocks and glancing at our watches? Is our interaction with time merely one of timers and stopwatches, of alarms and buzzers?</p>
<p>There has to be more than that to our relationship with time. There <em>should</em> be more to that relationship. And I believe that there can be.</p>
<p>I started seriously thinking about the idea of becoming more connected with time just after Christmas in 2015. In the afternoon of December 31<sup>st</sup> of that year, I tuned into Al-Jazeera. I caught the tail end of the news and stuck around to view a promo for an upcoming programme. After that, a short feature began. That feature focused on a man named Masahiro Kikuno, a watchmaker from Japan.</p>
<p>I’m not one who fetishizes or obsesses over watches. I’m not really interested in them. In fact, it’s been decades since I last owned a watch (a Swiss Army Watch, bought at Hercules Sporting Goods in Toronto, in case you’re wondering). But I continued watching the feature.</p>
<p>Why? I was initially drawn in by the quality of Kikuno’s work. What kept my attention were the ideas and philosophy around time that he wraps into that work. It was those ideas and that philosophy that started the gears in my mind moving, that sparked thoughts about time and mindfulness and art.</p>
<p>When I first started pondering his work, I considered Kikuno to be a master craftsman. He’s that — you can see it in each timepiece of creates. Kikuno’s more that a master craftsman. Kikuno is an artist. Time is his canvas. He paints on that canvas with wheels and springs, with gaskets and rotors, with dials and bezels.</p>
<p>Artist is an often-overused term, but it fits Kikuno perfectly. Kikuno is meticulous in creating his wares. The design and assembly of each watch is a painstaking, methodical, and slow process. He only creates, for example, <a href="http://www.masahirokikuno.jp/watches/wadokei-revision/">only one of a particular watch each year</a> each year. His is the kind of craftsmanship and care, attention to detail and the embrace of a philosophy of life, that seems so passé in our age of mass-produced timepieces, in this age of connected smartwatches. Or anything mass produced or connected, for that matter.</p>
<p>More than that, Kikuno’s watches and his devotion to the idea of time intersect with the oft-utter, but not oft-understood, concept of <em>mindfulness</em>. Mindfulness, <a href="https://en.wikipedia.org/wiki/Mindfulness">according to Wikipedia</a>:</p>
<blockquote>
<p>[I]s the psychological process of bringing one’s attention to the internal and external experiences occurring in the present moment</p>
</blockquote>
<p>There’s nothing automatic about Kikuno’s timepieces. They’re the ultimate throwback to a long-gone age. They’re the ultimate example of analog technology, a technology that’s portable and wearable.</p>
<p>The owners of Kikuno’s timepieces need to use their senses to ensure that their watches stay accurate. This forces the owner of one of Kikuno’s watches to be more aware of the passage of time. They need to know and understand that signs indicating that a watch is running down.</p>
<p>The owners of Kikuno’s timepieces need to be more aware and more mindful of how the passage of time affects not only the timepiece but also the world around them. Without that mindfulness, a watch stops. The owner of that watch would need to do quite a bit of fiddling to get the timepiece back on track. And, by extension, to get themselves back on track.</p>
<p>It’s that kind of awareness, that kind of mindfulness that’s lacking in many of our lives. We turn our wrists to cast a glance at our watches. We look at the clock on the wall or on our phones or in the corner of a computer screen. We <em>see</em> a representation of time. We know that time is passing. But I think we fail to understand time. We fail to understand how to live with time.</p>
<p>With one of Kikuno’s watches, you’re forced to live with time. Not in the abstract, but in the concrete.</p>
<p>In Kikuno’s own words, his watches <em>go on a journey with the owner</em>. There’s a symbiotic relationship between the timepiece and the person wearing it. They rely on each other. Kikuno’s watches need to be wound every couple of days to remain accurate. His watches don’t emit alerts. They don’t send reminders to your phone. They stop without giving any indication that they need attention. You need to pay attention to the watch in order to get the most out of it</p>
<p>Kikuno’s watches, as I’ve stated before, are works of art. That elevates them from being mere devices for keeping time to something more. Those watches cause a shift in one’s relationship with time, forcing the owner to be more mindful. Those watches are also something that owners can pass down from generation to generation. They’re something that resists the constantly shifting tides and winds of style and fashion. In that respect, Kikuno’s timepieces are <em>of</em> time but they also <em>transcend</em> time.</p>
<p>Kikuno admits that you can buy cheaper, more accurate watches than the ones he crafts. His wares will set you back well over $100,000 (USD) — hardly the type of watch you’d wear to the gym.</p>
<p>While Kikuno’s watches are beyond the means of most of us, that doesn’t take away from their beauty and the craft that went into creating them. And it doesn’t move them from the intersection of mindfulness and art. It doesn’t change that those watches take mindfulness and morph it into a beautiful form.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
