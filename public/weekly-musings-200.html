<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 200</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 200</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>And welcome to the 200<sup>th</sup> edition of the letter! While that’s something of a milestone, it’s also business as usual. Thank you for sticking with me through a double century of these little emails.</p>
<p>As for what you’re about to read, it’s a bit longer than usual and a bit of a change of pace. The topic is one that has been becoming more relevant to me in recent years, and will for you before you expect it.</p>
<p>There won’t be an edition of the letter landing in your inbox next week. I’m having some minor surgery and will be taking a few days to rest and recover. We’ll be back to our regularly-scheduled schedule on April 5<sup>th</sup>.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-getting-older">On Getting Older</h2>
<p>In mid-2022, a friend and I were riffing about meeting up in <a href="weekly-musings-049.html">a city</a> to which he’d never been and which we both want to spend some time exploring. During our back and forth, I half jokingly mentioned that since my 60<sup>th</sup> birthday is only a few years away, we should meet up then — a journey to Tokyo would make a great birthday gift to myself.</p>
<p>Said friend was shocked when I mentioned my age. In fact, he dressed me down for pulling his leg. Sadly, that wasn’t the case. I definitely wasn’t joking about the amount of time I’ve ticked off. The truth is that I’m on downward side of my 50s. And, no, I don’t know how I got here either …</p>
<p>All those years, and the mileage they’ve accrued, have started to catch up with me. And I don’t mean that in a bad way. I vaguely recall a book published in the 1990s titled <em>Growing Old is Not For Sissies</em> and always chuckle when its title pops into my head. There is, though, some truth to it.</p>
<p>Getting older isn’t something we can avoid. Not even if you’re one of those tech billionaires who spend seven or eight figures annually on treatments, diets, regimens, and supplements in a vain, losing battle to stave off the inevitable.</p>
<p>Getting older isn’t something to fear. It’s not something we can put off until a later date. Getting older is something that happens to all of us. It’s something we all need to accept. To embrace, even.</p>
<p>It’s <em>how</em> we get older that defines us. It’s <em>how</em> we get older that shows our true characters and natures. Allow me to share a few dubious insights, filtered through the perspective of someone with more sand gathered at the bottom of their hourglass than at the top, about getting older.</p>
<p>Even if we take care of ourselves, even if we avoid situations that can suddenly knock us off this mortal coil, we all have a limited amount of time on this Earth. None of us know how long that could be — it could be 80 or 90 years or more. It might be less. Much less.</p>
<p>Over the decades, I’ve had a friend or three fly away in what some would call the primes of their lives. Others I know are older than me and have a youth the belies their age. But all of them, the ones still here and the ones who’ve gone, <em>lived</em> their lives. Some boldly, some in a more quiet manner.</p>
<p>Each of them made a difference, no matter how large or small, in the lives of those around them. One or two had wider influences. They all had adventures. They all had and have amusing stories to tell. They all had and have insights that you might not expect them to possess. I must admit that I admire them for living as they do. None of them are leading lives of quiet desperation.</p>
<p>Sometimes, I half jokingly refer to myself as an <a href="https://knowyourmeme.com/memes/old-man-yells-at-cloud">old man yelling at clouds</a> (though most of the clouds I yell at are of the <a href="https://en.wikipedia.org/wiki/Cloud_computing">computing variety</a>). I’ve mellowed quite a bit over the years, believe it or not.</p>
<p>Long-time friends will tell you that I was more irascible in my 20s and 30s than I am now. While there are things that raise my ire and my gorge still, those are fewer than they were in the past. I’m better (at least, I <em>hope</em> I’m better) at choosing targets of my ire and invective these days. But, aside from a few minor things here and there, I’m fairly happy.</p>
<p>Professionally, I burned a bridge or three over the years on principle (or what I <em>thought</em> was principle). Nowadays, I try to be a bit more diplomatic, a bit more flexible. While I might not always succeed, I’m at least I’m willing to try. As opposed to my younger days when that kind of flexibility wasn’t an option I’d consider.</p>
<p>While my body can’t take the bashing it did in my teens, 20s, and 30s I can do a few things now that I couldn’t do then. And I know better how to work with my body (rather than expecting it to do what I tell it) to stay healthy. Many of my physical ailments aren’t (only) due to age. The mild arthritis in my fingers is due to breaking or dislocating them several times over the decades. The knee pain I experience daily is from having not cartilage in one and very little in the other. The tendinitis in elbows, again mild, is due to a number of factors.</p>
<p>None of those ailments are debilitating. I’m not in constant pain or even constantly aching. All of that is more annoying than anything when the pain and stiffness does flare up. Which happens a little more often these days. I just work through it, and try my best to not let the pains and aches and stiffness have an effect on my quality of life.</p>
<p>Probably the most important lesson I’ve learned is patience and the value of slow. Once upon a time, I rushed to finish things, then moved on to the next thing that had caught my attention. I believed that there was so much to do, so much to experience. And that I had to do and experience it all <em>ASAP</em>. Turns out a good chunk of all that wasn’t as important or as urgent as I thought.</p>
<p>Those times were too quick, too compressed. I was always wanting just a bit more. I spent a lot of time trying to live at <a href="weekly-musings-004.html">1,000 km/h</a>. And, to be honest, mainly failing miserably at it. Now, I try to take things at a slower pace. I try to be more thoughtful, more mindful, more deliberate. I’m doing what I <em>want</em> to, not everything I <em>can</em> or <em>think</em> I should do.</p>
<p>As a result, I learned to enjoy simple experiences and to appreciate the value of simple. In everything. Bulk, complexity, packed schedules — it’s all a recipe for overwhelm. Keeping things simple gets rid of a lot of cognitive overhead and stress. I might not see or do more, but I <em>can</em> focus on depth rather than grazing. I can bask in quality rather than trying to cram as much in as I can. I’m able to better savour moments. I can appreciate the time I have with others or just with myself.</p>
<p>As we get older, we’re supposed to amass a store of wisdom. I don’t claim to possess that commodity in any significant amount. Or any amount, to be honest. Instead of wisdom, I think I have gained <em>perspective</em>, in range of areas. For me, that’s as good as wisdom. There’s store of history I’ve watched unfold, in (again) a range of areas. I see patterns in what happened then being replicated in what’s happening now. Not just in the wider, grander scheme of things but on a personal level. Sometimes, that perspective is scary and depressing. Sometimes, the insights from that perspective gave pause to think, give me an opportunity to grow.</p>
<p>Then again, I’m definitely not immune to once in a while falling into trap of <em>Back in the day …</em> or <em>I remember when …</em> It’s easy to get wrapped in the haze of faux nostalgia. I know that things weren’t better in my younger years. There were problems then, just as we now have problems. Like many others, I didn’t realize that some of those problems were actually problems. And more than a few of those problems either were ignored or nudged aside so someone else could solve them at some point in the future. We all know how well a lot of that’s turned out, don’t we?</p>
<p>Those weren’t simpler times. Those weren’t slower times, either. The internet might not have been on everyone’s computers, but sources of information had started to proliferate and the speed at which information spread had started to accelerate. The rate of change rapid, even if it didn’t seem that way. New technology, new ideas, new cultural mores, new ways of doing things were appearing almost weekly. Thought, culture were expanding at what seemed pace with which none of us couldn’t keep up. Misinformation existed, but the vectors fewer and acted slower. The algorithms boosting dubious or false ideas and opinions were more of a human variety. Slower, but still effective.</p>
<hr />
<p>Getting older isn’t something to fear. It isn’t something to fight against tooth and nail. It’s going to happen to you, no matter who you are and what measures you put in place to try to stave off the march and ravages time.</p>
<p>What really matters is <em>how</em> you grow old. Do you let time have its way with you? Do you let life pass you by? Or do you live your life as fully as you can, enjoying as much of it as you can? In every way that you can.</p>
<p>A lot of it really does depend on you. On your attitude and on your outlook.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
