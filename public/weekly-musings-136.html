<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 136</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 136</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>You might have noticed that there wasn’t a letter last week. The good, the bad, and the ugly (and there’s been a lot of each) of 2021 finally caught up with me last week. And, to be honest, I needed to take a break.</p>
<p>This delayed edition of the letter branches off from <a href="weekly-musings-134.html">Musing 134</a>. How? You’ll have to read on to find out. I can’t make it easy for you, can I?</p>
<p>Just so you know, what you’re about to read started life in <a href="https://scottnesbitt.online/thoughts-about-feature-parity.html">my personal notebook</a> and appears here via a <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC-BY-NC-SA 4.0</a> license.</p>
<p>With that out of the way, let’s get to this week’s musing.</p>
<h2 id="on-feature-parity">On Feature Parity</h2>
<p>Back in 2015, I briefly chatted with John O’Nolan, founder of the blogging platform <a href="http://ghost.org">Ghost</a>. You’re probably wondering why the web needs another blogging platform, when WordPress powers millions of blogs and runs about 25% of all websites.</p>
<p>Prior to creating Ghost, O’Nolan worked for Automattic (the company behind WordPress). But he <a href="http://john.onolan.org/project-ghost/">found that WordPress had</a>:</p>
<blockquote>
<p>too much stuff everywhere, too much clutter, too many (so many) options getting in the way of what I really want to do: publish content</p>
</blockquote>
<p>Instead, O’Nolan wanted to take blogging back to basics. And Ghost was born.</p>
<p>During our chat, O’Nolan mentioned that he didn’t intend for Ghost to reach feature parity with WordPress. Ever. That’s not the niche the Ghost inhabits.</p>
<p>I wholeheartedly agree with O’Nolan. Not everyone is a power user, regardless of what many software developers seem to think. Regardless of what far too many user think. Not everyone needs their software and tools to do the same things. Not everyone needs their software and tools to do everything.</p>
<p>But the feature parity argument persists. I generally hear it from people who are thinking of switching to something that doesn’t pack all functions that they know from what they currently use. It doesn’t matter if they use those additional features or not. And it’s usually not.</p>
<p>I find the feature parity argument comes from two distinct directions. First, from people who use it as a crutch. A crutch that gives them an excuse not to try something new or to make a change, even if that something new or that change will simplify and streamline their digital lives.</p>
<p>Second, from people who whine about missing features and only do so because they can. They aren’t the application’s the target audience. Even if the tool did have the features they want (or think they need), I doubt they’d use it. I doubt the tool would be suited to their needs.</p>
<p>If feel you can’t adopt a tool because it lacks <em>feature x</em> or <em>feature y</em>, or because it can’t perform a certain task, ask yourself these three questions:</p>
<ul>
<li>How often do I use feature x or feature y?</li>
<li>If I do use those features, will I actually miss them?</li>
<li>Is that task the tool can’t perform one that’s important to me?</li>
</ul>
<p>From my experience in a number of areas — technical communicator, writer, reviewer, and (former) technology coach — I’ve found that many people rarely (if ever) use the features that are supposedly deal breakers. They just need to take a little time to adapt and adjust to something new.</p>
<p>Feature parity isn’t something that’s important to everyone. More features, especially if you don’t use them, aren’t always better. It’s too easy to fall into the contingency mindset when it comes to features — thinking that maybe one day you’ll need <em>feature x</em>, even though that day rarely comes.</p>
<p>Don’t buy into the hype of feature parity. Instead of thinking in terms of features, think about what you need to do. Not at some point in the distant future, but <strong>right now</strong>. Then, think of what you need to do it. Chances are, you’ll discover that you don’t need something big and powerful to get your work done. You probably just need something simple, light, and streamlined.</p>
<p>I’m a strong believer in the <em><a href="http://en.wikipedia.org/wiki/Pareto_principle">80/20 rule</a></em>, that 80% of people really only need 20% of the features and functions of just about any tool they use. You can even argue that the ratio is 90/10.</p>
<p>Think about it for a moment. Unless you’re a specialist, you don’t need expert-level tools. You need tools that are small, fast, and lean. Tools that focus on the features you use regularly, if not daily. Tools that aren’t cluttered with functions and menus and toolbars and a lot of cruft.</p>
<p>Take, for example, image editing. Over the years, I used a powerful open source application called <a href="https://www.gimp.org">The GIMP</a>. It was installed on all of my laptops. But I rarely used it. My needs when it comes to editing images are simple: resizing them, straightening them, adjusting their colour, and cropping them. For those tasks, The GIMP was just <em>too much</em>.</p>
<p>Instead, I use a lighter application called <a href="https://www.pinta-project.com/">Pinta</a>. Why? It does what I need, and just a little bit more. Not too much more, though.</p>
<p>That goes for a majority of the tools that I use. They let me do my work, quickly and efficiently. I’m not burdened with what I don’t need.</p>
<p>As I was writing those last few paragraphs, I could hear the plaintive cries <em>But I need …</em>, <em>I can’t do without …, and </em>What if I need …*</p>
<p>Admittedly, there are people who actually <em>do</em> need more advanced and complex features. But they’re not the majority. Not even close. It’s easy for them to fall into what I call the <em><a href="weekly-musings-063">power user fallacy</a></em>: everyone uses a tool or technology in the way that you do. That’s not the case. Everyone has different needs, everyone has different goals. Everyone tackles tasks in slightly different ways.</p>
<p>As for not being able to do without feature x or function y, how do they know that they can’t? They’ll never know until they try. And those bells and whistles that some people proclaim, often in tones of outrage, that they can’t do without? More often than not, the missing bells and whistles are sound and fury that signify very little. Folks might <em>think</em> they miss them, but that bit of <a href="https://en.wikipedia.org/wiki/Fear_of_missing_out">FOMO</a> disappears when they realize that they never used those features or functions in the first place.</p>
<p>Finally, it’s easy to fall into the contingency mindset. Again, you need to ask yourself a question: <em>when was the last time I used that feature or tool?</em> If you can’t remember, or it was months ago, chance are you won’t miss it.</p>
<p>Let’s end this musing with <a href="https://netfoundry.io/why-we-switched-to-mattermost/">this thought</a>:</p>
<blockquote>
<p>Focus on what your tiny, tiny pond of early adopters wants and needs, rather than fighting a feature war for mass market opportunities. Delight your tiny pond of early adopters, and you can grow from there.</p>
</blockquote>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
