<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link rel="stylesheet" href="styles.css" type="text/css" />
    <link rel="icon" href="images/favicon.ico" />
    <title>Weekly Musings 157</title>
  </head>
  <body>
     <main>
      <article>
       <header>
        <h1>Weekly Musings 157</h1>
       </header>
	   <p>Welcome to this edition of <em>Weekly Musings</em>, where each Wednesday I share some thoughts about what’s caught my interest in the last seven days.</p>
<p>Welcome back. You might be pleased to know that I’ve published the <a href="https://scottnesbitt.gumroad.com/l/weeklymusings3">latest collection of essays</a> from this letter at Gumroad. It’s a free download, but if you’re so inclined you can pay what you want for the collection. You’re not expected or obliged to toss any money my way, though.</p>
<p>But back to our regularly-scheduled letter. This time ’round, some thoughts about a communication technology that’s been around for what seems like forever. One which we all use, and one which more than a few people complain about and rail against. A technology, though, that isn’t as bad as some people make it out to be.</p>
<p>With that out of the way, let’s kick off year four of the letter with this week’s musing.</p>
<h2 id="on-email">On Email</h2>
<p>Email’s dead. The communication technologies that have sent it to the digital glue factory? Those are many. Old school instant messaging. Skype. Social media. Modern group messaging apps like WhatsApp, Slack, Discord, and Teams. The list goes on.</p>
<p>The death of email is a narrative that’s been bouncing around the online world for a long, long time. But for something that was supposedly killed a while ago, email is still alive and kicking.</p>
<p>Each day, many millions of messages — long and short, important and trite — wend their way through the intertubes, landing in various inboxes waiting to be cracked open and read. So much for the rumours of email’s death.</p>
<p>Email, regardless of the digital firepower leveled against it, persists. It’s survived. I’d go so far to say that email has <em>thrived</em>. Not because it’s perfect; it’s definitely not perfect by any stretch of the imagination. Email has survived and thrived mail because it’s familiar. It’s widespread. It’s easy to use.</p>
<p>Email <em>works</em>. It. Just. Works. And that’s what has ensured it’s survival.</p>
<p>As I pointed out a couple of paragraphs back, email is <em>far</em> from perfect. It’s not always the best way to collaborate, especially when several people are involved. Email threads can quickly get long and convoluted. It’s difficult to keep track of where you are and what’s the latest response or thought or idea.</p>
<p>And then there’s the problem of email attachments. I’m not (only) talking about the potential for viruses or malware or spyware, but keeping track of those attachment. Of knowing where an attachment that you saw a few days or weeks ago, but which you need <em>now</em>, is hiding. Of those attachments eating into the storage limits of your email account. Of those attachments being out of date or there being multiple copies of the same file in your various folders.</p>
<p>Despite what Google started with <a href="https://en.wikipedia.org/wiki/Gmail">Gmail</a> and what 37Signals has been doing with <a href="https://en.wikipedia.org/wiki/Hey_(email_service)">HEY</a>, the problems I just outlined persist. And they will continue to persist, regardless of attempts to bring new us new ways of interacting with our emails.</p>
<p>That said, the messaging tools that have been touted as email killers run into the problem that email encounters. It doesn’t take long, either. At my current Day Job<sup>TM</sup>, for example, the groups I work with heavily use Microsoft Teams for communication and collaboration. Threads quickly go many, many messages deep. There’s a lot of commentary, discussion, argument going on. You need to wade through a lot to find a solution to a problem or the information that you need. Sometimes, that’s not possible. And, often, the same question (or a minor variation of it) is asked and debated over and answered several times over.</p>
<p>I know, from experience and anecdote, that happens with other messaging tools too. What’s new is old again, indeed.</p>
<p>Unlike messaging apps, email excels as medium of <em><a href="weekly-musings-055">slow communication</a></em>. Email’s so-called killers promote immediacy. They provoke knee jerk reactions. They carry an expectation, often a <em>demand</em>, of instant response. Sometimes that can be a good thing, especially if there’s a (digital) fire to put out.</p>
<p>But, more often than they should, there are people who get testy if they don’t receive a reply 4.781 seconds after sent their original message. Instant messaging, in any of its forms, doesn’t give you time to think a response through. Replying <em>now</em> is almost too late; a reflexive, shallow, pointed reply is the norm.</p>
<p>Email, too, has (to much lesser degree) those kinds of expectations. But it also offers you (if you decide to take that offer) space to consider, to pause, to ponder.</p>
<p>In that way, email also shines as medium of <em>slow information</em>. What comes into your inbox isn’t always a message or an offer or solicitation. It could what you’re reading right now. A longer-form piece of writing, either one that you subscribed to or one which a friend of family tapped or or sent your way.</p>
<p>Those can be letters, in the tradition of the 17<sup>TH</sup> and 18<sup>TH</sup> century <a href="https://en.wikipedia.org/wiki/Republic_of_Letters">Republic of Letters</a>. Longer meanderings about a topic. Maybe not on a deep intellectual or philosophical level, but letters which require you to take your time not just to read and absorb what’s in them, but to also carefully consider the ideas contained within.</p>
<p>I often think that instant messaging, in all its forms, can be like social media: a fire hose of information which doesn’t seem to stop, which doesn’t seem to slow down. Which expects the impossible of you: to take in each and every drop, regardless of its quality or veracity. Email is more like RSS. You can be selective about the information that you take in. You can be slow to absorb that information. With email, you don’t need to be constantly stressed about keeping up with everything. You only need to keep up with what you want to keep up with.</p>
<p>Email, to put it bluntly, is my social media. It’s how I keep in touch with people. I send them a message, not a DM or a post on whatever social media wall they maintain. Email is how, at least in part, I share ideas with a wider audience. A small group of people, many of whom I don’t know and who don’t know me. I get more out of email than I did when I was actively posting on Twitter and Mastodon.</p>
<p>The people with whom correspond don’t bog my email down with the excruciating minutiae of their daily lives — that artisan soda or beer they just drank, photos of a dessert that they’re about to tuck into, what they’re watching at this very moment on their streaming service of choice.</p>
<p>Best of all, my inbox isn’t clogged with misinformation or disinformation. My correspondents only share links to things that they know might pique my interest. Or, sometimes, things which might raise my ire or my gorge. They share important news about themselves or about mutual friends. Our digital correspondence can be light and breezy, but also can be serious and have some depth.</p>
<p>Email has its place, even in today’s ecosystem of quick, instant, reflexive communication. It can offers space for reflection, a chance to add detail and colour to replies. Responses to emails can be hair trigger, but they can also be well thought out in a way that’s rare with messaging tools.</p>
<p>Email <em>isn’t</em> for all communication. It never has been, and really shouldn’t be. And if you’re disappointed that email doesn’t do everything that you <em>think</em> it should, then I’d say the problem doesn’t lie with email.</p>
<p>Something to ponder.</p>
      <p class="author">&mdash; <a href="https://scottnesbitt.net" target="_blank">Scott Nesbitt</a></p>
      </article>
    </main>
    <footer><div class="left">
	<a href="index.html">Home</a> | <a href="about.html">About</a> | <a href="privacy.txt" target="_blank">Privacy Policy</a>
     </div>
     <div class="right">
	<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a> | &copy; Scott Nesbitt
</div>
</footer>
  </body>
</html>
